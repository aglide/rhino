var fs = require('fs');
var vars;

exports.vars = function() {
	if (vars === undefined) {
		vars = {};
		
		// Default values
		vars.test = true;
		vars.server_ip = '127.0.0.1';
		vars.server_port = '3000';
		vars.cookie_secret = '238839';
		vars.mongo = {
				user: "",
				pwd: "",
				host: "127.0.0.1",
				port: "27017",
				db: "animagens",
				url: '127.0.0.1:27017/animagens'
		};
		vars.nodemailer_secret = 'test';
		vars.nodemailer_refresh = 'test';
		vars.ips = ['127.0.0.1'];
		vars.prikey = 'a';
		vars.pubkey = 'b';
		vars.ssl = false;
		
		vars.load = function(file) {
			if (file !== undefined) {
				var jsonConfig = JSON.parse(fs.readFileSync(file));
				if (jsonConfig.test !== undefined) {
					vars.test = jsonConfig.test;
				}
				
				// Server config
				if (jsonConfig.server_ip !== undefined) {
					vars.server_ip = jsonConfig.server_ip;
				}
				if (jsonConfig.server_port !== undefined) {
					vars.server_port = jsonConfig.server_port;
				}
				if (jsonConfig.cookie_secret !== undefined) {
					vars.cookie_secret = jsonConfig.cookie_secret;
				}
				// Mongo config
				if (jsonConfig.mongodb_host !== undefined) {
					vars.mongo.host = jsonConfig.mongodb_host;
				}
				if (jsonConfig.mongodb_port !== undefined) {
					vars.mongo.port = jsonConfig.mongodb_port;
				}
				if (jsonConfig.mongodb_db !== undefined) {
					vars.mongo.db = jsonConfig.mongodb_db;
				}
				if (jsonConfig.mongodb_user !== undefined) {
					vars.mongo.user = jsonConfig.mongodb_user;
				}
				if (jsonConfig.mongodb_pwd !== undefined) {
					vars.mongo.pwd = jsonConfig.mongodb_pwd;
					vars.mongo.url = vars.mongo.user + ':' + vars.mongo.pwd + '@' + vars.mongo.host + ':'
										+ vars.mongo.port + '/' + vars.mongo.db;
				}
				if (jsonConfig.nodemailer_secret !== undefined) {
					vars.nodemailer_secret = jsonConfig.nodemailer_secret;
				}
				if (jsonConfig.nodemailer_refresh !== undefined) {
					vars.nodemailer_refresh = jsonConfig.nodemailer_refresh;
				}
				if (jsonConfig.ips !== undefined) {
					vars.ips = jsonConfig.ips;
				}
				if (jsonConfig.prikey !== undefined) {
					vars.prikey = jsonConfig.prikey;
				}
				if (jsonConfig.pubkey !== undefined) {
					vars.pubkey = jsonConfig.pubkey;
				}
				if (jsonConfig.ssl !== undefined) {
					vars.ssl = jsonConfig.ssl;
				}
			}
		}
	}
	return vars;
};