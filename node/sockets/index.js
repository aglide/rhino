var constants = require('../constants');
var db = require('../db');
var utils = require('../utils');

var sockets = {};
var socketChecker = setInterval(function(){utils.checkSockets(sockets)}, 30 * 1000);

exports.connectSocket = function(socket, user_id, callback) {
	db.findUserById(user_id, function(err, user) {
		if (err) {return callback(err);}
		
		user.password = undefined;
		user.salt = undefined;
		socket.userData = user;
		sockets[socket.id] = socket;
		
		exports.sendToAll('updateUsers', {users: exports.getConnectedUsers()});
		
		return callback(null, socket);
	});
};

exports.exitSocket = function(socket_id) {
	delete sockets[socket_id];
	exports.sendToAll('updateUsers', {users: exports.getConnectedUsers()});
};

exports.sendToAll = function(operation, data) {
	var keys = Object.keys(sockets);
	for (var i = 0; i < keys.length; i++) {
		var s = sockets[keys[i]];
		if (s != undefined && s.id != undefined) {
			s.emit(operation, data);
		}
	}
};

exports.sendToUser = function(user_id, operation, data) {
	var targetSockets = [];
	var keys = Object.keys(sockets);
	for (var i = 0; i < keys.length; i++) {
		var s = sockets[keys[i]];
		if (s != undefined && s.id != undefined && s.userData._id.toString() == user_id.toString()) {
			s.emit(operation, data);
		}
	}
};

exports.isConnected = function(user_id) {
	var keys = Object.keys(sockets);
	for (var i = 0; i < keys.length; i++) {
		var s = sockets[keys[i]];
		if (s != undefined && s.id != undefined && s.userData._id == user_id) {
			return true;
		}
	}
	return false;
};

exports.isInChat = function(user_id) {
	var keys = Object.keys(sockets)
	for (var i = 0; i < keys.length; i++) {
		var s = sockets[keys[i]];
		if (s != undefined && s.id != undefined && s.userData._id == user_id && s.inChat) {
			return true;
		}
	}
	return false;
};

exports.getConnectedUsers = function() {
	var users = [];
	var keys = Object.keys(sockets);
	for (var i = 0; i < keys.length; i++) {
		var s = sockets[keys[i]];
		if (s != undefined && s.id != undefined && utils.findUserInList(s.userData._id, users) == undefined) {
			users.push(s.userData);
		}
	}

	return users.sort(compareUsersByAlias);
};

function compareUsersByAlias(a,b) {
	if (a.alias < b.alias)
		return -1;
	else if (a.alias > b.alias)
		return 1;
	else
		return 0;
}

