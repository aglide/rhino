var db = require('../db');

exports.info = function(err) {
	protectPassword(err, function() {
		db.saveLog({
			status: 'INFO',
			content: err
		}, function(err) {
			if (err) console.error('DB LOGGING ERROR: ' + err);
		});
		console.log(err);
	});
};

exports.warn = function(err) {
	protectPassword(err, function() {
		db.saveLog({
			status: 'WARN',
			content: err
		}, function(err) {
			if (err) console.error('DB LOGGING ERROR: ' + err);
		});
		console.log(err);
	});
};

exports.error = function(err) {
	protectPassword(err, function() {
		db.saveLog({
			status: 'ERROR',
			content: err
		}, function(err) {
			if (err) console.error('DB LOGGING ERROR: ' + err);
		});
		console.error(err);
	});
};

function protectPassword(err, callback) {
	if (err.data !== undefined && err.data.password !== undefined) {
		err.data.password = '*********';
	}
	if (err.data !== undefined && err.data['rep-password'] !== undefined) {
		err.data['rep-password'] = '*********';
	}
	return callback(err);
}