var mongodb = require('../mongodb');
var utils = require('../utils');
var htmlToText = require('html-to-text');

// LOGS

exports.saveLog = function(log, callback) {
	var newLog = mongodb.Log(log);
	newLog.save(function(err) {
		  if (err) return callback(err);
		  return callback(null);
	});
};

// USERS

exports.saveUser = function(user, callback) {
	var newUser = mongodb.User(user);
	newUser.save(function(err) {
		  if (err) return callback(err);
		  return callback(null, newUser._id);
	});
};

exports.updateUser = function(user, callback) {
	var id = user._id;
	delete user['_id'];
	mongodb.User.update({_id:id}, user, function(err) {
		if (err) return callback(err);
		return callback(null);
	});
};

exports.findUsers = function(filter, one, callback) {
	if (one) {
		mongodb.User.findOne(filter, function(err, user) {
			return callback(err, user);
		});
	} else {
		mongodb.User.find(filter).sort({alias:1}).exec(function(err, user) {
			return callback(err, user);
		});
	}
};

exports.findUserById = function(id, callback) {
	mongodb.User.findById(id, function(err, u) {
		return callback(err, u);
	});
};

exports.findUserByEmailOrNick = function(inputUser, callback) {
	if (inputUser == undefined) {
		return callback(null, null);
	}
	
	var filter = {removed:false};
	if (inputUser.indexOf('@') > -1) {
		filter.email = inputUser;
	} else {
		filter.nickname = inputUser.toLowerCase();
	}
	exports.findUsers(filter, true, function(err, user) {
		return callback(err, user);
	});
};

exports.verifyUser = function(filter, callback) {
	mongodb.User.update(filter, {'$set': {verified: true}}, function(err) {
		return callback(err);
	});
};

exports.saveUbox = function(ubox, callback) {
	var newUbox = mongodb.Ubox(ubox);
	newUbox.save(function(err) {
		if (err) return callback(err);
		return callback(null, newUbox._id);
	});
};

exports.findUbox = function(filter, callback) {
	mongodb.Ubox.findOne(filter, function(err, ubox) {
		return callback(err, ubox);
	});
};

exports.updateUbox = function(ubox, callback) {
	var id = ubox._id;
	delete ubox['_id'];
	mongodb.Ubox.update({_id:id}, ubox, function(err) {
		if (err) return callback(err);
		return callback(null);
	});
};

// TOKENS

exports.findVerToken = function(filter, callback) {
	mongodb.VerificationToken.findOne(filter, function(err, token) {
		return callback(err, token);
	});
};

exports.removeVerToken = function(filter, callback) {
	mongodb.VerificationToken.remove(filter, function(err) {
		return callback(err);
	});
};

exports.saveVerificationToken = function(verToken, callback) {
	var newToken = mongodb.VerificationToken(verToken);
	newToken.save(function(err) {
		  if (err) return callback(err);
		  return callback(null);
	});
};

exports.updateVerificationToken = function(verToken, callback) {
	var id = verToken._id;
	delete verToken['_id'];
	mongodb.VerificationToken.update({_id:id}, verToken, function(err) {
		if (err) return callback(err);
		return callback(null);
	});
};

exports.findCategories = function(filter, one, callback) {
	if (one) {
		mongodb.Category.findOne(filter, function(err, c) {
			return callback(err, c);
		});
	} else {
		mongodb.Category.find(filter, function(err, c) {
			return callback(err, c);
		});
	}
};

exports.findCategoryById = function(id, callback) {
	mongodb.Category.findById(id, function(err, c) {
		return callback(err, c);
	});
};

// POSTS

exports.savePost = function(post, callback) {
	var newPost = mongodb.Post(post);
	newPost.save(function(err) {
		  if (err) return callback(err);
		  return callback(null, newPost._id);
	});
};

exports.updatePost = function(post, callback) {
	var id = post._id;
	delete post['_id'];
	mongodb.Post.update({_id:id}, post, function(err) {
		if (err) return callback(err);
		return callback(null);
	});
};

exports.findPostById = function(id, callback) {
	mongodb.Post.findById(id, function(err, p) {
		return callback(err, p);
	});
};

exports.findPosts = function(filter, one, callback) {
	if (one) {
		mongodb.Post.findOne(filter, function(err, p) {
			return callback(err, p);
		});
	} else {
		mongodb.Post.find(filter, function(err, p) {
			return callback(err, p);
		});
	}
};

exports.findResumedPosts = function(category, from, to, contextUser, callback, f) {
	var increment = to - from;
	
	mongodb.Category.findById(category, function(err, c) {
		if (err) return callback(err);
		
		if (c == undefined) {
			c = {
				title: 'Actividad reciente'
			}
		}
		
		var filter = f == undefined ? {} : f;
		filter.removed = false;
		if (c._id) {
			filter.category_id = c._id;
		}
		
		mongodb.Post.find(filter).sort({'fixed': -1, 'last_edit': -1}).skip(from).limit(increment).exec(function(err, posts) {
			if (err) return callback(err);
			
			if (posts.length > 0) {
				fillListWithUsers(0, posts, [], contextUser, function(err, userList) {
					if (err) return callback(err);
					
					mongodb.Post.count(filter, function(err, count) {
						if (err) return callback(err);
						return callback(null, {list:posts, userList: userList, category: c, totalCount: count});
					});
				});
			} else {
				return callback(null, {list:[], userList: [], category: c, totalCount: 0});
			}
		});
	});
};

function fillListWithUsers(index, postList, userList, contextUser, callback) {
	mongodb.User.findById(postList[index].owner_id, function(err, u) {
		if (err) return callback(err);

		mongodb.Comment.count({post_id: postList[index]._id}, function(err, commentsCount) {
			if (err) return callback(err);

			mongodb.VisitedPost.findOne({user_id: contextUser ? contextUser._id : undefined, post_id: postList[index]._id}, function(err, v) {
				if (err) return callback(err);

				postList[index].content = '<p>' + htmlToText.fromString(postList[index].content).replace(/(?:\r\n|\r|\n)/g, ' ').substring(0,100) + '...</p>';
				var d = new Date(postList[index].last_edit);
				var dateTime = utils.formatDate(d);

				var data = {
						owner_nickname: u.nickname,
						owner_alias: u.alias,
						dateTime: dateTime,
						visited: v == undefined ? true : v.visited,
						commentsCount: commentsCount
				};
				userList.push(data);
				index++;
				if (index == postList.length) {
					return callback(null, userList);
				}

				fillListWithUsers(index, postList, userList, contextUser, function(err, filledList) {
					return callback(err, filledList);
				});

			});
		});
	});
}

exports.savePostEdit = function(postEdit, callback) {
	var newPostEdit = mongodb.PostEdit(postEdit);
	newPostEdit.save(function(err) {
		  if (err) return callback(err);
		  return callback(null);
	});
};

exports.findVisitedPost = function(filter, callback) {
	mongodb.VisitedPost.findOne(filter, function(err, v) {
		return callback(err, v);
	});
};

exports.saveVisitedPost = function(visitedPost, callback) {
	var newVisitedPost = mongodb.VisitedPost(visitedPost);
	newVisitedPost.save(function(err) {
		  if (err) return callback(err);
		  return callback(null);
	});
};

exports.updateAllVisitedPost = function(owner_id, post_id, callback) {
	mongodb.VisitedPost.update({user_id: {'$ne': owner_id}, post_id: post_id}, {visited: false}, {multi:true}, function(err) {
		if (err) return callback(err);
		return callback(null);
	});
};

exports.updateVisitedPost = function(filter, data, callback) {
	mongodb.VisitedPost.update(filter, data, function(err) {
		if (err) return callback(err);
		return callback(null);
	});
};

exports.updateVisitedPosts = function(filter, data, callback) {
	mongodb.VisitedPost.update(filter, data, {multi:true}, function(err) {
		if (err) return callback(err);
		return callback(null);
	});
};

exports.saveComment = function(comm, callback) {
	var newComment = mongodb.Comment(comm);
	newComment.save(function(err) {
		  if (err) return callback(err);
		  return callback(null, newComment._id);
	});
};

exports.updateComment = function(comm, callback) {
	var id = comm._id;
	delete comm['_id'];
	mongodb.Comment.update({_id:id}, comm, function(err) {
		if (err) return callback(err);
		return callback(null);
	});
};

exports.findCommentById = function(id, callback) {
	mongodb.Comment.findById(id, function(err, c) {
		return callback(err, c);
	});
};

exports.findLastComment = function(post_id, callback) {
	mongodb.Comment.find({post_id: post_id}).sort({'created_at': -1}).limit(1).exec(function(err, c) {
		if (err) return callback(err);
		
		if (c.length == 0) {
			return callback(null, undefined);
		}
		
		return callback(err,c[0]);
	});
};

exports.findComments = function(filter, one, callback) {
	if (one) {
		mongodb.Comment.findOne(filter, function(err, c) {
			if (err) return callback(err);
			mongodb.User.findById(c.owner_id, function(err, u) {
				if (err) return callback(err);
				
				var d = new Date(c.created_at);
				var dateTime = utils.formatDate(d);
				
				return callback(null, {comment:c, userData: {dateTime: dateTime, alias: u.alias}});
			});
		});
	} else {
		mongodb.Comment.find(filter).sort({'created_at': 1}).exec(function(err, c) {
			if (err) return callback(err);
			
			if (c.length > 0) {
				fillWithUserData(0, c, [], function(err, userData) {
					return callback(null, {comments:c, userData: userData});
				});
			} else {
				return callback(null, {comments:c, userData: []});
			}
		});
	}
};

exports.findPagComments = function(filter, from, to, callback) {
	mongodb.Comment.find(filter).skip(from).limit(to - from).sort({'created_at': 1}).exec(function(err, c) {
		if (err) return callback(err);

		if (c.length > 0) {
			fillWithUserData(0, c, [], function(err, userData) {
				return callback(null, {comments:c, userData: userData});
			});
		} else {
			return callback(null, {comments:c, userData: []});
		}
	});
};

function fillWithUserData(index, commList, userData, callback) {
	mongodb.User.findById(commList[index].owner_id, function(err, u) {
		if (err) return callback(err);

		commList[index].content = utils.parseSpoilers(commList[index].content);
		var d = new Date(commList[index].created_at);
		var dateTime = utils.formatDate(d);
		
		var data = {
				alias: u.alias,
				nickname: u.nickname,
				profile_img: u.profile_img,
				signature: u.signature,
			    level: u.level,
				dateTime: dateTime
		};
		
		userData.push(data);
		index++;
		if (index == commList.length) {
			return callback(null, userData);
		}
		
		fillWithUserData(index, commList, userData, function(err, filledList) {
			return callback(err, filledList);
		});
	});
}

exports.findAllValorations = function(callback) {
	mongodb.Valoration.find({}, function(err, valorations) {
		return callback(err, valorations);
	});
};

exports.findValoration = function(filter, callback) {
	mongodb.Valoration.find(filter, function(err, valorations) {
		return callback(err, valorations);
	});
};

// NOTIFICATIONS

exports.saveNotification = function(notification, callback) {
	var newNotification = mongodb.Notification(notification);
	newNotification.save(function(err) {
		return callback(err, newNotification._id);
	});
};

exports.findNotifications = function(from, to, filter, callback) {
	mongodb.Notification.find(filter).sort({'created_at': -1}).skip(from).limit(to - from).exec(function(err, nots) {
		return callback(err, nots);
	});
};

exports.countNotReadNotifications = function(owner_id, callback) {
	mongodb.Notification.count({owner_id:owner_id, watched: false}, function(err, c) {
		return callback(err, c);
	});
};

exports.watchNotification = function(not_id, callback) {
	mongodb.Notification.findById(not_id, function(err, n) {
		if (err) return callback(err);
		
		var id = n._id;
		delete n['_id'];
		n.watched = true;
		mongodb.Notification.update({_id:id}, n, function(err) {
			return callback(err);
		});
	});
};

// CHAT
exports.findRooms = function(filter, one, callback) {
	if (one) {
		mongodb.Room.findOne(filter, function(err, r) {
			return callback(err, r);
		});
	} else {
		mongodb.Room.find(filter, function(err, r) {
			return callback(err, r);
		});
	}
};

exports.saveRoom = function(room, callback) {
	var newRoom = mongodb.Room(room);
	newRoom.save(function(err) {
		return callback(err, newRoom._id);
	});
};

exports.updateRoom = function(room, callback) {
	var id = room._id;
	delete room['_id'];
	mongodb.Room.update({_id:id}, room, function(err) {
		if (err) return callback(err);
		return callback(null);
	});
};

exports.deleteRoom = function(filter, callback) {
	mongodb.Room.update(filter, {
	    removed: true
	}, function(err) {
		callback(err);
	});
};

exports.findMessages = function(filter, callback, msgCount, l) {
	var limit = l ? l : 20;
	var skip = msgCount ? msgCount : 0;
	mongodb.Message.find(filter).sort({'created_at': -1}).skip(skip).limit(limit).exec(function(err, ms) {
		return callback(err, ms.reverse());
	});
};

exports.findPersonalMessages = function(filter, callback, msgCount, l) {
	var limit = l ? l : 20;
	var skip = msgCount ? msgCount : 0;
	mongodb.PersonalMessage.find(filter).skip(skip).sort({'created_at': -1}).limit(limit).exec(function(err, ms) {
		return callback(err, ms.reverse());
	});
};

exports.getMessageHistory = function(room_id, from, amount, callback) {
	mongodb.Message.find({room_id: room_id},
		null,
		{
		    skip:from,
		    limit:amount,
		    sort:{
		        _id: -1
		    }
		},
		function(err,records){
			return callback(err, records.reverse());
		}
	);
};

exports.saveMessage = function(msg, callback) {
	var newMessage = mongodb.Message(msg);
	newMessage.save(function(err) {
		return callback(err);
	});
};

exports.savePersonalMessage = function(msg, callback) {
	var newPersonalMessage = mongodb.PersonalMessage(msg);
	newPersonalMessage.save(function(err) {
		return callback(err);
	});
};

// VOTING

exports.saveVoting = function(voting, callback) {
	var newVoting = mongodb.Voting(voting);
	newVoting.save(function(err) {
		return callback(err);
	});
};

exports.findVotings = function(filter, one, callback) {
	if (one) {
		mongodb.Voting.findOne(filter, function(err, v) {
			return callback(err, v);
		});
	} else {
		mongodb.Voting.find(filter, function(err, v) {
			return callback(err, v);
		});
	}
};

exports.removeVoting = function(filter, callback) {
	mongodb.Voting.update(filter, {'$set': {active: false}}, function(err) {
		return callback(err);
	});
};

exports.findVotes = function(filter, callback) {
	mongodb.Vote.find(filter, function(err, v) {
		return callback(err, v);
	});
};

exports.saveVote = function(vote, callback) {
	var newVote = mongodb.Vote(vote);
	newVote.save(function(err) {
		return callback(err);
	});
};

exports.saveBiography = function(bio, callback) {
	var newBio = mongodb.Biography(bio);
	newBio.save(function(err) {
		if (err) return callback(err);
		return callback(null, newBio._id);
	});
};

exports.findBios = function(filter, callback) {
	mongodb.Biography.find(filter, function(err, bios) {
		return callback(err, bios);
	});
};

exports.findAchievements = function(filter, callback) {
	mongodb.Achievement.find(filter, function(err, achs) {
		return callback(err, achs);
	});
};

exports.countAchievements = function(filter, callback) {
	mongodb.Achievement.count(filter, function(err, count) {
		return callback(err, count);
	});
};