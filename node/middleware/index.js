var db = require('../db');
var logger = require('../logger');
var constants = require('../constants');
var sessionManager = require('../session-manager');
var utils = require('../utils');
var config = require('../config').vars();
var sockets = require('../sockets');

var version = '0.1.11.0b';

exports.work = function (req, res, next) {
	var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
	if (req.headers.host.match(/^www/) !== null ) {
		res.redirect('https://' + req.headers.host.replace(/^www\./, '') + req.url);
	} else {
		getContext(req, ip, function(err, context) {
			if (err) {
				utils.manageError(req, res, 'getContext', ip, err);
			} else {
				try {
					context.version = version;
					context.ip = ip;
					context.isTest = config.test;
					context.connectedUsers = sockets.getConnectedUsers();
					req.animagens = {
						context: context
					};
					next();
				} catch (ex) {
					var err = constants.getError('c108');
					err.ip = ip;
					err.time = new Date();
					err.operation = req.url;
					err.data = req.query;
					err.info = ex.stack;
					logger.error(err);
				}
			}
		});   
  }
};

function getContext(req, ip, callback) {
	var context = {
		title : 'Animagens',
		navhistory : []
	};
	var session_token = req.session.session_token === undefined ? req.body.session_token : req.session.session_token;
	sessionManager.checkSession(session_token, ip, function(err, user) {
		if (err && err.code != 103) {
			return callback(err);
		}
		
		context.user = user;
		
		var filter = {age_protected: false};
		if (context.user) {
			var under_age = utils.calculateAge(context.user.birth) < 18;
			filter = under_age ? {age_protected: false} : {};
		}
		db.findCategories(filter, false, function(err, categories) {
			if (err) {
				return callback(err);
			}
			
			context.categories = categories;
			db.findUsers({removed:false, verified: true}, false, function(err, users) {
				if (err) {
					return callback(err);
				}
				
				context.users = users;
				context.notifications = [];
				context.notReadNots = 0;
				if (context.user) {
					db.findUbox({owner_id: context.user._id.toString()}, function(err, ubox) {
						if (err) {
							return callback(err);
						}

						if (ubox != undefined) {
							context.ubox = ubox;
						} else {
							context.ubox = {
								owner_id: context.user._id.toString(),
								achievements: [],
								animas: 0,
								experience: 0
							};
							db.saveUbox(context.ubox, function(err) {
								if (err) console.error(err);
							});
						}

						db.countNotReadNotifications(context.user._id, function(err, c) {
							if (err) {
								return callback(err);
							}

							context.notReadNots = c;
							db.findNotifications(0, 50, {owner_id: context.user._id}, function(err, nots) {
								if (err) {
									return callback(err);
								}

								context.notifications = nots;
								return callback(null, context);
							});
						});
					});
				} else {
					return callback(null, context);
				}
			});
		});
	});
}