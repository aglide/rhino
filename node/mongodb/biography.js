var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var biographySchema = new Schema({
  owner_id: { type: String, required: true },
  msg: { type: String, required: true },
  rewards: [String],
  animas: Number,
  experience: Number,
  created_at: Date
});

//on every save, add the date
biographySchema.pre('save', function(next) {

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = new Date();

  next();
});

// we need to create a model using it
var Biography = mongoose.model('Biography', biographySchema);

// make this available to our users in our Node applications
module.exports = Biography;