var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var uboxSchema = new Schema({
  owner_id: { type: String, required: true, unique: true },
  achievements: [String],
  animas: Number,
  experience: Number
});

//on every save, add the date
uboxSchema.pre('save', function(next) {

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = new Date();

  next();
});

// we need to create a model using it
var Ubox = mongoose.model('Ubox', uboxSchema);

// make this available to our users in our Node applications
module.exports = Ubox;