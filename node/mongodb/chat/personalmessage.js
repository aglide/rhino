var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var parsonalMessageSchema = new Schema({
  sender_id: { type: String, required: true},
  receiver_id: { type: String, required: true},
  content: { type: String, required: true},
  created_at: Date
});

//on every save, add the date
parsonalMessageSchema.pre('save', function(next) {
	var currentDate = new Date();
	
    // if created_at doesn't exist, add to that field
    if (!this.created_at)
      this.created_at = new Date();

    next();
});

// we need to create a model using it
var PersonalMessage = mongoose.model('PersonalMessage', parsonalMessageSchema);

// make this available to our users in our Node applications
module.exports = PersonalMessage;