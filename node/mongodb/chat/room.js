var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var roomSchema = new Schema({
  name: { type: String, required: true },
  founder_id: { type: String, required: true },
  removed: Boolean,
  perms: [String],
  bans: [String],
  public: Boolean,
  created_at: Date
});

//on every save, add the date
roomSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

var Room = mongoose.model('Room', roomSchema);
module.exports = Room;