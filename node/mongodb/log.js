var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var logSchema = new Schema({
	created_at: Date,
	status: String,
	content: {}
});

//on every save, add the date
logSchema.pre('save', function(next) {

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = new Date();

  next();
});

// we need to create a model using it
var Log = mongoose.model('Log', logSchema);

// make this available to our users in our Node applications
module.exports = Log;