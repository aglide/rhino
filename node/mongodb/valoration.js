var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var valorationSchema = new Schema({
  img: { type: String, required: true},
  description: { type: String, required: true}
});

// we need to create a model using it
var Valoration = mongoose.model('Valoration', valorationSchema);

// make this available to our users in our Node applications
module.exports = Valoration;