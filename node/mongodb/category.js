var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var categorySchema = new Schema({
  title: { type: String, required: true, unique: true },
  description: { type: String, required: true},
  age_protected: { type: Boolean, required: true },
  moderators: [String],
  created_at: Date
});

//on every save, add the date
categorySchema.pre('save', function(next) {

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = new Date();

  next();
});

// we need to create a model using it
var Category = mongoose.model('Category', categorySchema);

// make this available to our users in our Node applications
module.exports = Category;