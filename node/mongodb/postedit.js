var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var postEditSchema = new Schema({
  post_id: { type: String, required: true },
  user_id: { type: String, required: true},
  previous_content: { type: String, required: true},
  created_at: Date
});

//on every save, add the date
postEditSchema.pre('save', function(next) {
	var currentDate = new Date();
	
    // if created_at doesn't exist, add to that field
    if (!this.created_at)
      this.created_at = new Date();

    next();
});

// we need to create a model using it
var PostEdit = mongoose.model('PostEdit', postEditSchema);

// make this available to our users in our Node applications
module.exports = PostEdit;