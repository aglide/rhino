var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var visitedPostSchema = new Schema({
  user_id : { type: String, required: true },
  post_id : { type: String, required: true},
  visited: Boolean
});

// we need to create a model using it
var VisitedPost = mongoose.model('VisitedPost', visitedPostSchema);

// make this available to our users in our Node applications
module.exports = VisitedPost;