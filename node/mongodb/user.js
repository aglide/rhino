var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var userSchema = new Schema({
  email: { type: String, required: true, unique: true },
  nickname: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  salt: { type: String, required: true },
  verified: Boolean,
  banned: Boolean,
  removed: Boolean,
  private: Boolean,
  favs_list: [String],
  created_at: Date,
  last_known_ip: String,
  last_login: Date,
  alias: String,
  birth: {type: Date, required: true},
  signature: String,
  profile_img: String,
  profile_color: String,
  sound_enabled: Boolean,
  level: Number,
  italics: Boolean,
  bold: Boolean,
  permissions: {
	  super_admin: Boolean,
	  admin: Boolean,
	  chat_admin: Boolean,
	  chat_moderator: Boolean
  }
});

//on every save, add the date
userSchema.pre('save', function(next) {

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = new Date();

  next();
});

// we need to create a model using it
var User = mongoose.model('User', userSchema);

// make this available to our users in our Node applications
module.exports = User;