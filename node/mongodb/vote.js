var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var voteSchema = new Schema({
  voting_id: { type: String, required: true },
  owner_id: { type: String, required: true },
  chosens: [String],
  values: [{
	  name: String,
	  value: Number
  }],
  created_at: Date
});

//on every save, add the date
voteSchema.pre('save', function(next) {

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = new Date();

  next();
});

// we need to create a model using it
var Vote = mongoose.model('Vote', voteSchema);

// make this available to our users in our Node applications
module.exports = Vote;