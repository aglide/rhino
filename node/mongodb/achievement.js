var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var achievementSchema = new Schema({
  tag: { type: String, required: true, unique: true},
  name: String,
  description: String,
  img: String,
  secret: Boolean,
  created_at: Date
});

//on every save, add the date
achievementSchema.pre('save', function(next) {

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = new Date();

  next();
});

// we need to create a model using it
var Achievement = mongoose.model('Achievement', achievementSchema);

// make this available to our users in our Node applications
module.exports = Achievement;