var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var verificationTokenSchema = new Schema({
	verification_token: String,
	user_id: String,
	created_at: Date
});

// we need to create a model using it
var VerificationToken = mongoose.model('VerificationToken', verificationTokenSchema);

// make this available to our users in our Node applications
module.exports = VerificationToken;