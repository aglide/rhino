var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var votingSchema = new Schema({
  post_id: { type: String, required: true},
  active: Boolean,
  options: [{
	  name: String,
	  img: String
  }],
  choice_type: String,
  created_at: Date
});

//on every save, add the date
votingSchema.pre('save', function(next) {

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = new Date();

  next();
});

// we need to create a model using it
var Voting = mongoose.model('Voting', votingSchema);

// make this available to our users in our Node applications
module.exports = Voting;