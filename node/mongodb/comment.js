var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var commentSchema = new Schema({
  post_id: { type: String, required: true},
  owner_id: { type: String, required: true},
  content: { type: String, required: true},
  removed: Boolean,
  likes: [{
	  user_id: String,
	  valoration_id: String
  }],
  created_at: Date
});

//on every save, add the date
commentSchema.pre('save', function(next) {
	var currentDate = new Date();
	
    // if created_at doesn't exist, add to that field
    if (!this.created_at)
      this.created_at = new Date();

    next();
});

// we need to create a model using it
var Comment = mongoose.model('Comment', commentSchema);

// make this available to our users in our Node applications
module.exports = Comment;