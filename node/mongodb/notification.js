var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var notificationSchema = new Schema({
	owner_id: { type: String, required: true},
	title: { type: String, required: true},
	content: { type: String, required: true},
	link: { type: String, required: true},
	img: String,
	watched: Boolean,
	created_at: Date
});

//on every save, add the date
notificationSchema.pre('save', function(next) {

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = new Date();

  next();
});

// we need to create a model using it
var Notification = mongoose.model('Notification', notificationSchema);

// make this available to our users in our Node applications
module.exports = Notification;