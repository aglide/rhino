var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var postSchema = new Schema({
  title: { type: String, required: true },
  category_id: { type: String, required: true},
  content: { type: String, required: true},
  owner_id: { type: String, required: true},
  removed: Boolean,
  visits_counter: Number,
  fixed: Boolean,
  display_img: String,
  likes: [{
	  user_id: String,
	  valoration_id: String
  }],
  updated_at: Date,
  last_edit: Date,
  created_at: Date
});

//on every save, add the date
postSchema.pre('save', function(next) {
	var currentDate = new Date();
	
	// change the updated_at field to current date
	this.updated_at = currentDate;
	
    // if created_at doesn't exist, add to that field
    if (!this.created_at)
      this.created_at = new Date();
    
    if (!this.display_img)
    	this.display_img = 'https://jamfnation.jamfsoftware.com/img/default-avatars/generic-user.png';

    next();
});

// we need to create a model using it
var Post = mongoose.model('Post', postSchema);

// make this available to our users in our Node applications
module.exports = Post;