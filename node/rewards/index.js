var db = require('../db');
var sockets = require('../sockets');
var achievements = require('../achievements');

var expReward = {
    comment: 2,
    receivedComment: 6,
    valorate: 1,
    receivedValoration: 3,
    post: 10,
    dailyLogin: 15,
    registered: 15
};

var animasReward = {
    comment: 0,
    receivedComment: 0,
    valorate: 0,
    receivedValoration: 0,
    post: 0,
    dailyLogin: 1,
    registered: 1
};

var levelUpMessages = [
    'Nunca hay nada como el primero.',
    'Vaya, parece que estás cayendo bien por aquí.',
    '¡Wow! Ya has contado hasta 3. Hay quienes no llegan a tanto.',
    'Tu nombre me suena, creo que empieza a estar por todas partes.',
    'Tus contribuciones ya serán recordadas por la mayoría.'
];

exports.giveCommentReward = function(user_id, comment, post) {
    applyStandardRewards(user_id, 'comment', function(err, data) {
        if (err) {
            console.error(err);
        } else {
            achievements.checkAchievements('comment', {data:data, comment: comment, post: post}, function(err, achIDs) {
               if (err) {
                   console.error(err);
               } else {
                   var bio = {
                       owner_id: user_id,
                       msg: 'Has comentado en el post "' + post.title + '"',
                       rewards: achIDs,
                       animas: animasReward['comment'],
                       experience: expReward['comment'],
                       created_at: new Date()
                   };
                   db.saveBiography(bio, function(err) {
                       if (err) {
                           console.error(err);
                       }
                   });
               }
            });
        }
    });
};

exports.giveReceivedCommentReward = function(user_id, sender, comment, post) {
    applyStandardRewards(user_id, 'receivedComment', function(err, data) {
        if (err) {
            console.error(err);
        } else {
            achievements.checkAchievements('receivedComment', {data:data, comment: comment, post: post, sender: sender}, function(err, achIDs) {
                if (err) {
                    console.error(err);
                } else {
                    var bio = {
                        owner_id: user_id,
                        msg: 'Has recibido un comentario en el post "' + post.title + '"',
                        rewards: achIDs,
                        animas: animasReward['receivedComment'],
                        experience: expReward['receivedComment']
                    };
                    db.saveBiography(bio, function(err) {
                        if (err) {
                            console.error(err);
                        }
                    });
                }
            });
        }
    });
};

exports.giveValorateReward = function(user_id, valoration, post, comment) {
    applyStandardRewards(user_id, 'valorate', function(err, data) {
        if (err) {
            console.error(err);
        } else {
            achievements.checkAchievements('valorate', {data:data, valoration: valoration, post: post, comment: comment}, function(err, achIDs) {
                if (err) {
                    console.error(err);
                } else {
                    var bio = {
                        owner_id: user_id,
                        msg: 'Has valorado un mensaje',
                        rewards: achIDs,
                        animas: animasReward['valorate'],
                        experience: expReward['valorate']
                    };
                    db.saveBiography(bio, function(err) {
                        if (err) {
                            console.error(err);
                        }
                    });
                }
            });
        }
    });
};

exports.giveReceivedValorationReward = function(user_id, sender, valoration, post, comment) {
    applyStandardRewards(user_id, 'receivedValoration', function(err, data) {
        if (err) {
            console.error(err);
        } else {
            achievements.checkAchievements('receivedValoration', {data:data, valoration: valoration, post: post, comment: comment, sender: sender}, function(err, achIDs) {
                if (err) {
                    console.error(err);
                } else {
                    if (user_id != sender._id) {
                        var bio = {
                            owner_id: user_id,
                            msg: 'Has recibido una valoración en un mensaje',
                            rewards: achIDs,
                            animas: animasReward['receivedValoration'],
                            experience: expReward['receivedValoration']
                        };
                        db.saveBiography(bio, function (err) {
                            if (err) {
                                console.error(err);
                            }
                        });
                    }
                }
            });
        }
    });
};

exports.givePostReward = function(user_id, post) {
    applyStandardRewards(user_id, 'post', function(err, data) {
        if (err) {
            console.error(err);
        } else {
            achievements.checkAchievements('post', {data:data, post: post}, function(err, achIDs) {
                if (err) {
                    console.error(err);
                } else {
                    var bio = {
                        owner_id: user_id,
                        msg: 'Has creado el post "' + post.title + '"',
                        rewards: achIDs,
                        animas: animasReward['post'],
                        experience: expReward['post']
                    };
                    db.saveBiography(bio, function(err) {
                        if (err) {
                            console.error(err);
                        }
                    });
                }
            });
        }
    });
};

exports.giveDailyReward = function(user_id) {
    applyStandardRewards(user_id, 'dailyLogin', function(err, data) {
        if (err) {
            console.error(err);
        } else {
            achievements.checkAchievements('dailyLogin', {data:data}, function(err, achIDs) {
                if (err) {
                    console.error(err);
                } else {
                    var bio = {
                        owner_id: user_id,
                        msg: 'Has iniciado sesión por primera vez hoy.',
                        rewards: achIDs,
                        animas: animasReward['dailyLogin'],
                        experience: expReward['dailyLogin']
                    };
                    db.saveBiography(bio, function(err) {
                        if (err) {
                            console.error(err);
                        }
                    });
                }
            });
        }
    });
};

exports.giveRegisteredReward = function(user_id) {
    applyStandardRewards(user_id, 'registered', function(err, data) {
        if (err) {
            console.error(err);
        } else {
            achievements.checkAchievements('registered', {data:data}, function(err, achIDs) {
                if (err) {
                    console.error(err);
                } else {
                    var bio = {
                        owner_id: user_id,
                        msg: '¡Te has registrado!',
                        rewards: achIDs,
                        animas: animasReward['registered'],
                        experience: expReward['registered']
                    };
                    db.saveBiography(bio, function(err) {
                        if (err) {
                            console.error(err);
                        }
                    });
                }
            });
        }
    });
};

function applyStandardRewards(user_id, rewardName, callback) {
    retrieveUserInfo(user_id, function (err, data) {
        if (err) {
            return callback(err);
        }

        var oldLevel = data.user.level;
        if (expReward[rewardName] > 0) {
            applyExpReward(data, expReward[rewardName], function (err, newData) {
                data = newData;
                if (oldLevel != data.user.level) {
                    // New level
                    var notification = {
                        owner_id: data.user._id,
                        title: '¡Has subido al nivel ' + data.user.level + '!',
                        content: data.user.level-1 >= levelUpMessages.length ? '¡Enhorabuena!' : levelUpMessages[data.user.level-1],
                        link: '/levels',
                        img: data.user.profile_img,
                        watched: false
                    };

                    db.saveNotification(notification, function(err) {
                        if (err) {
                            console.error(err);
                        } else {
                            if (sockets.isConnected(user_id)) {
                                sockets.sendToUser(user_id.toString(), 'notification', notification);
                            }
                        }
                    });

                }
            });
        }
        if (animasReward[rewardName] > 0) {
            applyAnimasReward(data, animasReward[rewardName], function(err, newData) {
                data = newData;
            });
        }

        return callback(null, data);
    });
}

function retrieveUserInfo(user_id, callback) {
    db.findUserById(user_id.toString(), function(err, user) {
        if (err) return callback(err);
        db.findUbox({owner_id: user_id.toString()}, function(err, ubox) {
            if (err) return callback(err);

            return callback(null, {
                user: user,
                ubox: ubox
            });
        });
    });
}

function applyExpReward(data, reward, callback) {
    var newUser = data.user;
    var newExp = data.ubox.experience + reward;

    if (newExp >= (data.user.level+1)*100) {
        newExp = newExp - (data.user.level+1)*100;
        newUser.level = data.user.level+1;
        db.updateUser(newUser, function(err) {
            if (err) {
                console.error(err);
            }
        });
    }

    var newUbox = data.ubox;
    newUbox.experience = newExp;

    db.updateUbox(newUbox, function(err) {
        if (err) {
            console.error(err);
        }
    });

    return callback(null, {
        user: newUser,
        ubox: newUbox
    });
}

function applyAnimasReward(data, reward, callback) {
    var newAnimas = data.ubox.animas + reward;

    var newUbox = data.ubox;
    newUbox.animas = newAnimas;

    db.updateUbox(newUbox, function(err) {
        if (err) {
            console.error(err);
        }
    });

    return callback(null, {
        user: data.user,
        ubox: newUbox
    });
}