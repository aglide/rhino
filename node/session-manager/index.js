var db = require('../db');
var utils = require('../utils');
var constants = require('../constants');
var sessionExpiryTime = 120 * 60 * 1000;
var sessionExpiryCheckRate = 30 * 60 * 1000;

/*
 * Valid sessions stored like this:
 * user_id : {
 * 	session_token: string - Random token.
 *  ip: string - Access IP.
 *  last_usage: long - Last time checked.
 * }
 */
var validSessions = {};

// Session Expiration
setInterval(function () {
	var sessionsToRemove = [];
	var keys = Object.keys(validSessions);
	keys.forEach(function (k) {
		if (Date.now() - validSessions[k].last_usage >= sessionExpiryTime) {
			sessionsToRemove.push(k);
		}
	});
	sessionsToRemove.forEach(function(s) {
		delete validSessions[s];
	});
}, sessionExpiryCheckRate);

exports.insertOrUpdateSession = function(user, ip, callback) {
	var savedSession = validSessions[user._id];
	if (savedSession !== undefined) {
		// TODO: If the IPs are different it means that the user accessed from another machine without logging out.
		// This could be an intrusion and should be managed.
		savedSession.last_usage = Date.now();
		return callback(null, savedSession.session_token);
	}
	
	var session_token = utils.genToken();
	validSessions[user._id] = {
		session_token: session_token,
		ip: ip,
		last_usage: Date.now()
	};
	
	return callback(null, session_token);
};

exports.deleteUserSession = function(user_id) {
	delete validSessions[user_id];
};

exports.checkSession = function(session_token, ip, callback, skipData) {
	if (session_token === undefined) {
		return callback(constants.getError('c103'));
	}
	
	var found = false;
	var userIDs = Object.keys(validSessions);
	for (var i = 0; i < userIDs.length && !found; i++) {
		var userID = userIDs[i].toString();
		var userSession = validSessions[userID];
		if (userSession.session_token == session_token) {
			found = true;
			// TODO: If the IPs are different it means that the user accessed from another machine without logging out.
			// This could be an intrusion and should be managed.
			userSession.last_usage = Date.now();
			
			if (!skipData) {
				db.findUserById(userID, function(err, user) {
					if (err) {
						return callback(err);
					}
					
					user.password = undefined;
					user.salt = undefined;
					return callback(null, user);
				});
			} else {
				return callback(null, {_id: userID});
			}
		}
	}
	
	if (!found) {
		return callback(constants.getError('c103'));
	}
};