var db = require('../db');

var alphaDate = new Date("2016-01-18");
var achs;

exports.checkAchievements = function(type, info, callback) {
    loadAchs(function(err, achievements) {
        if (err) return callback(err);

        var achIDs = [];
        if (achievements.length > 0) {
            var newUbox = info.data.ubox;

            if (type == 'dailyLogin') {
                // Builder
                if (achievements['builder'] != undefined && newUbox.achievements.indexOf(achievements['builder']._id) < 0 && info.data.user.created_at.getTime() <= alphaDate.getTime()) {
                    newUbox.achievements.push(achievements['builder']._id);
                    achIDs.push(achievements['builder']._id);
                    db.updateUbox(newUbox, function (err) {
                        if (err) console.error(err);
                    });
                }
                // Happy birthday
                if (achievements['birthday'] != undefined && newUbox.achievements.indexOf(achievements['birthday']._id) < 0) {
                    var now = new Date();
                    var birthDate = new Date(info.data.user.birth);
                    var monthAndDayBirth = birthDate.getDate() + '/' + birthDate.getMonth();
                    var monthAndDayNow = now.getDate() + '/' + now.getMonth();

                    if (monthAndDayBirth == monthAndDayNow) {
                        newUbox.achievements.push(achievements['birthday']._id);
                        achIDs.push(achievements['birthday']._id);
                        db.updateUbox(newUbox, function (err) {
                            if (err) console.error(err);
                        });
                    }
                }
            } else if (type == 'receivedValoration') {
                // Caos
                if (achievements['caotic'] != undefined && newUbox.achievements.indexOf(achievements['caotic']._id) < 0) {
                    var allRecievedLikes = info.post == undefined ? info.comment.likes : info.post.likes;
                    var allReceivedIDs = [];
                    db.findAllValorations(function(err, vals) {
                        if (err) {
                            console.error(err);
                        } else {
                            for (var i = 0; i < allRecievedLikes.length; i++) {
                                if (allReceivedIDs.indexOf(allRecievedLikes[i].valoration_id) < 0) {
                                    allReceivedIDs.push(allRecievedLikes[i].valoration_id);
                                }
                            }
                            // Achievement done
                            if (vals.length == allReceivedIDs.length) {
                                newUbox.achievements.push(achievements['caotic']._id);
                                achIDs.push(achievements['caotic']._id);
                                db.updateUbox(newUbox, function (err) {
                                    if (err) console.error(err);
                                });
                            }
                        }
                    });
                }
                // A friend
                if (achievements['afriend'] != undefined && newUbox.achievements.indexOf(achievements['afriend']._id) < 0) {
                    var allRecievedLikes = info.post == undefined ? info.comment.likes : info.post.likes;
                    var likesCount = 0;
                    var dislikesCount = 0;
                    db.findValoration({description:'¡Me Gusta!'}, function(err, likes) {
                       if (err) {
                           console.error(err);
                       } else {
                           var like_id = likes.length > 0 ? likes[0]._id : '';
                           db.findValoration({description:'No me gusta'}, function(err, dislikes) {
                               if (err) {
                                   console.error(err);
                               } else {
                                   var dislike_id = dislikes.length > 0 ? dislikes[0]._id : '';
                                   for (var i = 0; i < allRecievedLikes.length; i++) {
                                       if (allRecievedLikes[i].valoration_id.toString() == like_id) {
                                           likesCount++;
                                       } else if (allRecievedLikes[i].valoration_id.toString() == dislike_id) {
                                           dislikesCount++;
                                       }
                                   }
                                   // Achievement done
                                   if (likesCount >= 10 && dislikesCount == 1) {
                                       newUbox.achievements.push(achievements['afriend']._id);
                                       achIDs.push(achievements['afriend']._id);
                                       db.updateUbox(newUbox, function (err) {
                                           if (err) console.error(err);
                                       });
                                   }
                               }
                           });
                       }
                    });
                }
                // Quality
                if (achievements['quality'] != undefined && newUbox.achievements.indexOf(achievements['quality']._id) < 0) {
                    var allRecievedLikes = info.post == undefined ? info.comment.likes : info.post.likes;
                    if (allRecievedLikes.length >= 30) {
                        var positiveCount = 0;
                        db.findValoration({description: '¡Me Gusta!'}, function (err, likes) {
                            if (err) {
                                console.error(err);
                            } else {
                                var like_id = likes.length > 0 ? likes[0]._id : '';
                                db.findValoration({description: 'Me enamora'}, function (err, loves) {
                                    if (err) {
                                        console.error(err);
                                    } else {
                                        var love_id = loves.length > 0 ? loves[0]._id : '';
                                        db.findValoration({description: 'Me troncho'}, function (err, laughs) {
                                            if (err) {
                                                console.error(err);
                                            } else {
                                                var laugh_id = laughs.length > 0 ? laughs[0]._id : '';
                                                for (var i = 0; i < allRecievedLikes.length; i++) {
                                                    if (allRecievedLikes[i].valoration_id.toString() == like_id
                                                         || allRecievedLikes[i].valoration_id.toString() == love_id
                                                         || allRecievedLikes[i].valoration_id.toString() == laugh_id) {
                                                        positiveCount++;
                                                    }
                                                }
                                                // Achievement done
                                                if (positiveCount * 100.0 / allRecievedLikes.length >= 70) {
                                                    newUbox.achievements.push(achievements['quality']._id);
                                                    achIDs.push(achievements['quality']._id);
                                                    db.updateUbox(newUbox, function (err) {
                                                        if (err) console.error(err);
                                                    });
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            } else if (type == 'valorate') {
                // Own judgement
                if (achievements['judgement'] != undefined && newUbox.achievements.indexOf(achievements['judgement']._id) < 0) {
                    var owner_id = info.post == undefined ? info.comment.owner_id : info.post.owner_id;
                    db.findValoration({description:'No me gusta'}, function(err, dislikes) {
                        if (err) {
                            console.error(err);
                        } else {
                            var dislike_id = dislikes.length > 0 ? dislikes[0]._id : '';
                            if (info.valoration.valoration_id.toString() == dislike_id
                                && info.data.user._id.toString() == owner_id.toString()) {

                                newUbox.achievements.push(achievements['judgement']._id);
                                achIDs.push(achievements['judgement']._id);
                                db.updateUbox(newUbox, function (err) {
                                    if (err) console.error(err);
                                });
                            }
                        }
                    });
                }
            } else if (type == 'registered') {
                if (achievements['register'] != undefined && newUbox.achievements.indexOf(achievements['register']._id) < 0) {
                    newUbox.achievements.push(achievements['register']._id);
                    achIDs.push(achievements['register']._id);
                    db.updateUbox(newUbox, function (err) {
                        if (err) console.error(err);
                    });
                }
            }
        }

        return callback(null, achIDs);
    });
};

function loadAchs(callback) {
    if (achs == undefined) {
        achs = {};
        db.findAchievements({}, function(err, result) {
            for (var i = 0; i < result.length; i++) {
                achs[result[i].tag] = result[i];
            }
            return callback(err, achs);
        });
    } else {
        return callback(null, achs);
    }

}