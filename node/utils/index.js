var logger = require('../logger');
var linkify = require('linkifyjs');
var linkifyHtml = require('linkifyjs/html');

// Token utils
var rand = function() {
    return Math.random().toString(36).substr(2);
};

exports.genToken = function() {
    return rand() + rand();
};

// Crypt utils
var crypto = require('crypto');
exports.hashPass = function(pwd, salt, callback) {
  var hash = crypto.createHash('sha256').update(pwd + salt).digest('base64');
  callback(null, hash);
};

// Html utils
var entityMap = {
	    "&": "&amp;",
	    "<": "&lt;",
	    ">": "&gt;",
	    '"': '&quot;',
	    "'": '&#39;',
	    "/": '&#x2F;'
};

var imgExts = [
     '.jpg',
     '.png',
     '.jpeg',
     '.gif',
     '.bmp',
     '.tiff',
     '.bpg',
     '.webp',
     '.svg',
     '.hdr'
];

exports.escapeHtml = function (string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
      return entityMap[s];
    });
};

function replaceURLWithHTMLLinks(text) {
    var exp = /^(?:(?:ht|f)tp(?:s?)\:\/\/|~\/|\/)?(?:\w+:\w+@)?((?:(?:[-\w\d{1-3}]+\.)+(?:com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|edu|co\.uk|ac\.uk|it|fr|tv|museum|asia|local|travel|[a-z]{2}))|((\b25[0-5]\b|\b[2][0-4][0-9]\b|\b[0-1]?[0-9]?[0-9]\b)(\.(\b25[0-5]\b|\b[2][0-4][0-9]\b|\b[0-1]?[0-9]?[0-9]\b)){3}))(?::[\d]{1,5})?(?:(?:(?:\/(?:[-\w~!$+|.,=]|%[a-f\d]{2})+)+|\/)+|\?|#)?(?:(?:\?(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=?(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)(?:&(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=?(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)*)*(?:#(?:[-\w~!$ |\/.,*:;=]|%[a-f\d]{2})*)?$/i;
    return text.replace(exp,"<a href='$1'>$1</a>"); 
}

exports.parseMessage = function (string) {
	var urlList = linkify.find(string);
	var parsedMsg = exports.escapeHtml(string);
	for (var i = 0; i < urlList.length; i++) {
		var u = urlList[i];
		if (u.type == 'url') {
			var parsedURL = exports.escapeHtml(u.value);
			if (imgExts.indexOf(u.value.substring(u.value.length-4,u.length)) > -1) {
				parsedMsg = parsedMsg.replace(parsedURL, '<img src="' + u.href + '"/>');
			} else {
				parsedMsg = parsedMsg.replace(parsedURL, '<a href="' + u.href + '" target="_blank">' + u.value + '</a>');
			}
		}
	}
	
    return parsedMsg;
};

exports.calculateAge = function(birthday) { // birthday is a date
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

exports.manageError = function(req, res, operation, ip, err, avoidRedirection) {
	err.from = ip;
	err.time = new Date();
	err.operation = operation;
	err.data = req.query;
	logger.error(err);
	if (!avoidRedirection) {
		res.redirect('/oops');
	}
};

exports.findUserInList = function(user_id, list) {
	for (var i = 0; i < list.length; i++) {
		var u = list[i];
		if (u != undefined && u._id.toString() == user_id.toString()) {
			return u;
		}
	}
	return undefined;
};

exports.checkSockets = function(sockets, callback) {
	var keys = Object.keys(sockets);
	for (var i = 0; i < keys.length; i++) {
		if (sockets[keys[i]].id == undefined) {
			delete sockets[keys[i]];
			if (callback != undefined) {
				callback(keys[i]);
			}
		}
	}
};

exports.formatDate = function(d, skipHours) {
	var dateTime = toTwoDigits(d.getDate()) + '/' + toTwoDigits((d.getMonth()+1)) + '/' + d.getFullYear();
	if (!skipHours) {
		dateTime += ' - ' + toTwoDigits(d.getHours()) + ':' + toTwoDigits(d.getMinutes()) + ':' + toTwoDigits(d.getSeconds());
	}
	return dateTime;
}

function toTwoDigits(num) {
	if (num < 10) {
		return '0' + num;
	}
	return num;
}

exports.parseSpoilers = function(oldContent) {
	var newContent = oldContent;
	var index = newContent.indexOf('[spoiler]');
	while(index > -1) {
		var htmlRepl = '<div style="height: 28px; overflow: hidden; background-color: #eeeeee"><div><button onclick="showSpoiler(this)">Mostrar Spoiler</button></div><p>' + newContent.substring(index + 9, newContent.indexOf('[/spoiler]')) + '</p></div>';
		newContent = newContent.replace(newContent.substring(index, newContent.indexOf('[/spoiler]')+10),htmlRepl);
		index = newContent.indexOf('[spoiler]');
	}
	return newContent;
};

exports.parseMentions = function(content, context, post_id, callback) {
	var mentions = content.match(/@\S+/g);
	if (mentions != undefined) {
		parseMention(0, mentions, content, context, post_id, function (err, newContent) {
			return callback(err, newContent);
		});
	} else {
		return callback(undefined, content);
	}
};

function parseMention(index, mentions, content, context, post_id, callback) {
	if (index >= mentions.length) {
		return callback(null, content);
	}

	var mNickname = mentions[index].substring(1).toLowerCase();

	db.findUserByEmailOrNick(mNickname, function(err, user) {
		if (err) return callback(err);

		var newContent = content;

		if (user != undefined) {
			newContent = content.replace('@' + mNickname, '<a target="_blank" href="/viewuser?user=' + mNickname + '">@' + mNickname + '</a>');
			var notification = {
				owner_id: user._id.toString(),
				title: 'Nueva mención',
				content: context.user.alias + ' te ha mencionado en un mensaje.',
				link: '/post?pid=' + post_id,
				img: context.user.profile_img,
				watched: false
			};
			db.saveNotification(notification, function(err, not_id) {
				if (err) return callback(err);

				notification._id = not_id;
				sockets.sendToUser(user._id.toString(),'notification', notification);
			});
		}
		parseMention(index+1, mentions, newContent, context, post_id, function(err, nc) {
			return callback(err, nc);
		});
	});
};