/**
 * Register post request with following fields:
 * - email : string - User Email.
 * - nickname : string - User Nickname.
 * - password : string - User password.
 * - alias : string - User display name.
 * - birth : Date - User birth date.
 * - private : boolean - True if user's profile is private. False Otherwise. Optional.
 * - profile_img : string - User's profile image URL. Optional.
 * - profile_color : string - Hexadecimal (#XXXXXX) profile color. Optional.
 * - signature : string - User signature shown in every post and message. Optional.
 * 
 * Returns an error if something went wrong. Nothing otherwise.
 **/
var db = require('../db');
var utils = require('../utils');
var constants = require('../constants');
var enotifier = require('../enotifier');
var config = require('../config').vars();

exports.requiredFields = ['email', 'nickname', 'registerpassword', 'alias', 'birth'];

exports.execute = function(data, context, callback) {
	verifyNickname(data.nickname.toLowerCase(), function(err) {
		if (err) {
			return callback(err);
		} else {
			verifyEmail(data.email, function(err) {
				if (err) {
					return callback(err);
				} else {
					registerNewUser(data, context, function(err) {
						if (err) {
							return callback(err);
						} else {
							return callback(null);
						}
					});
				}
			});
		}
	});
};

function registerNewUser(data, context, callback) {
	var salt = utils.genToken();
	utils.hashPass(data.registerpassword, salt, function(err, hashedPassword) {
		if (err) {
			return callback(err);
		}
		var birthParts = data.birth.split('/');
		var birthDate = new Date(birthParts[2],birthParts[1]-1,birthParts[0]);
		var newUser = {
				nickname: data.nickname.toLowerCase(),
				email: data.email,
				password: hashedPassword,
				salt: salt,
				alias: data.alias,
				birth: birthDate,
				private: data.private === undefined ? constants.defaultUserPrivate : data.private,
				profile_img: data.profile_img === undefined ? constants.defaultUserProfileImage : data.profile_img,
				profile_color: data.profile_color === undefined ? constants.defaultUserTextColor : data.profile_color,
				signature: data.signature === undefined ? '' : data.signature,
			    last_known_ip: context.ip,
			    last_login: new Date(),
			    favs_list: [],
			    removed: false,
			    banned: false,
			    verified: config.test,
			    level: 0,
			    permissions: {
			  	  super_admin: false,
			  	  admin: false,
			  	  chat_admin: false,
			  	  chat_moderator: false
			    }
			};
		db.saveUser(newUser, function(err, newUserId) {
			if (err) return callback(err);

			db.saveUbox({
				owner_id: newUserId,
				achievements: [],
				animas: 0,
				experience: 0
			}, function(err) {if(err) console.error(err)});

			if (config.test) {
				return callback(null);
			}
			
			var filter = {nickname: data.nickname.toLowerCase()};
			db.findUsers(filter, true, function(err, result) {
				if (err) return callback(err);
				
				var token = utils.genToken();
				var newVerToken = {
					verification_token: token,
					user_id: result._id
				};
				db.saveVerificationToken(newVerToken, function(err) {
					if (err) return callback(err);
					 
					enotifier.sendMail({
						 mails: [data.email],
						 subject: '¡Bienvenido/a a Animagens!',
						 content: '<p>De parte del equipo de Animagens, ¡bienvenido/a a nuestra comunidad!</p>'
							 + '<p>Con tal de confirmar tu registro de forma definitiva, debes acceder al siguiente enlace:</p>'
							 + '<p><a href="http://www.animagens.es/authToken?token=' + token + '">Confirmar registro</a></p>'
							 + '<p>¡Esperamos verte pronto!</p>'
					}, function(err) {
						 if (err) {
							 return callback(err);
						 } else {
							 return callback(null);
						 }
					});
				});
			});
		});
	});
};

function verifyNickname(nickname, callback) {
	db.findUsers({nickname: nickname}, false, function(err, results) {
		if (err) {
			return callback(err);
		}
		
		if (results.length > 0) {
			// Nickname already taken
			return callback(constants.getError('c101'));
		}
		
		return callback(null);
	});
}

function verifyEmail(email, callback) {
	db.findUsers({email: email}, false, function(err, results) {
		if (err) {
			return callback(err);
		}
		
		if (results.length > 0) {
			// Email already taken
			return callback(constants.getError('c102'));
		}
		
		return callback(null);
	});
}