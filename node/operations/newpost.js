var constants = require('../constants');
var db = require('../db');
var rewards = require('../rewards');
var utils = require('../utils');

exports.requiredFields = ['category_id', 'title', 'display_img', 'content'];

exports.execute = function(data, context, callback) {
	var today00 = new Date();
	today00.setHours(0);
	today00.setMinutes(0);
	today00.setSeconds(0);
	db.findPosts({owner_id: context.user._id, created_at:{"$gte": today00}}, false, function(err, posts) {
		if (err) return callback(err);

		var maxDailyPosts = context.user.level < 2 ? 3 : context.user.level < 5 ? 5 : 999;
		if (posts.length >= maxDailyPosts) {
			return callback(constants.getError('c116'));
		} else {
			db.findCategories({_id: data.category_id}, true, function(err, c) {
				if (err) return callback(err);

				if (c == undefined) {
					return callback(constants.getError('c999'));
				}

				data.owner_id = context.user._id;
				data.removed = false;
				data.fixed = data.fixed ? true : false;
				data.last_edit = new Date();

				db.savePost(data, function(err, post_id) {
					if (err) return callback(err);

					utils.parseMentions(data.content, context, post_id, function (err, newContent) {
						if (err) return callback(err);
						data.content = newContent;

						db.savePost(data, function (err, post_id) {
							if (err) return callback(err);

							if (data.votingName != undefined && data.votingName != 'none' && context.user.level >= 2) {
								var voting = {
									post_id: post_id,
									active: true,
									options: [],
									choice_type: data.votingName
								};
								var index = 1;
								while (data['option_' + index] != undefined) {
									voting.options.push({
										name: data['option_' + index],
										img: data['option_' + index + '_img']
									});
									index++;
								}
								db.saveVoting(voting, function (err) {
									if (err) console.log(err);
								});
							}

							rewards.givePostReward(context.user._id, data);

							db.findUsers({}, false, function (err, users) {
								if (err) return callback(err);

								saveVisitedPosts(0, users, post_id, context.user._id, function (err) {
									if (err) {
										return callback(err);
									}
								});
							});

							return callback(null, {post_id: post_id.toString()});
						});
					});
				});
			});
		}
	});
};

function saveVisitedPosts(index, users, post_id, owner_id, callback) {
	if (index >= users.length) {
		return callback(null);
	}
	
	var u = users[index];
	if (owner_id != u._id) {
		db.saveVisitedPost({
			post_id: post_id,
			user_id: u._id,
			visited: false
		}, function(err) {
			if (err) return callback(err);
			return saveVisitedPosts(index+1, users, post_id, owner_id, function(err) {
				return callback(err);
			});
		});
	}
}