var constants = require('../constants');
var db = require('../db');
var utils = require('../utils');

exports.requiredFields = ['_id', 'category_id', 'title', 'display_img', 'content'];

exports.execute = function(data, context, callback) {
	db.findCategories({_id: data.category_id}, true, function(err, c) {
		if (err) {
			return callback(err);
		}
		if (c == undefined) {
			return callback(constants.getError('c999'));
		}
		
		db.findPostById(data._id, function(err, post) {
			if (err) return callback(err);

			if (post == undefined) {
				return callback(constants.getError('c999'));
			}

			utils.parseMentions(data.content, context, post._id, function (err, newContent) {
				if (err) return callback(err);
				data.content = newContent;

				if (data.votingName != undefined && data.votingName != 'none' && context.user.level >= 2) {
					var voting = {
						post_id: post._id.toString(),
						active: true,
						options: [],
						choice_type: data.votingName
					};
					var index = 1;
					while (data['option_' + index] != undefined) {
						voting.options.push({
							name: data['option_' + index],
							img: data['option_' + index + '_img']
						});
						index++;
					}
					db.removeVoting({post_id: post._id.toString(), active: true}, function (err) {
						console.log('Removed!' + post._id.toString());
						db.saveVoting(voting, function (err) {
							if (err) console.log(err);
						});
					});
				} else if (data.votingName == undefined || data.votingName == 'none') {
					db.removeVoting({post_id: post._id.toString(), active: true}, function (err) {
						if (err) console.log(err);
						console.log('Removed!' + post._id.toString());
					});
				}

				if (post.content != data.content) {
					db.savePostEdit({
						post_id: post._id,
						user_id: context.user._id,
						previous_content: post.content
					}, function (err) {
						if (err) return callback(err);
						post.category_id = data.category_id;
						post.title = data.title;
						post.display_img = data.display_img;
						post.content = data.content;
						post.fixed = data.fixed;

						db.updatePost(data, function (err) {
							if (err) {
								return callback(err);
							}

							return callback(null);
						})
					});
				} else {
					post.category_id = data.category_id;
					post.title = data.title;
					post.display_img = data.display_img;
					post.content = data.content;
					post.fixed = data.fixed;

					db.updatePost(data, function (err) {
						if (err) {
							return callback(err);
						}

						return callback(null);
					})
				}
			});
		});
	});
};