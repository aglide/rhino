var constants = require('../constants');
var db = require('../db');

exports.requiredFields = ['_id', 'removed'];

exports.execute = function(data, context, callback) {
	db.findPostById(data._id, function(err, post) {
		if (err) return callback(err);
		
		if (!context.user || !context.user.permissions.admin && context.user._id != post.owner_id) {
			return callback(constants.getError('c112'));
		}
		
		post.removed = data.removed;
		db.updatePost(post, function(err) {
			if (err) {
				return callback(err);
			}
			
			return callback(null);
		});
	});
};