var constants = require('../constants');
var db = require('../db');
var utils = require('../utils');

exports.requiredFields = ['_id', 'content'];

exports.execute = function(data, context, callback) {
    db.findCommentById(data._id, function(err, comment) {
        if (err) return callback(err);

        if (comment == undefined) {
            return callback(constants.getError('c999'));
        }

        db.findPostById(comment.post_id, function(err, post) {
            if (err) return callback(err);

            utils.parseMentions(data.content, context, comment.post_id, function(err, newContent) {
                if (err) return callback(err);
                data.content = newContent;

                db.findCategoryById(post.category_id, function (err, category) {
                    if (err) return callback(err);

                    if (category.moderators.indexOf(context.user._id) < 0 && !context.user.permissions.admin) {
                        return callback(constants.getError('c112'));
                    }

                    comment.content = data.content;

                    db.updateComment(comment, function (err) {
                        if (err) return callback(err);

                        return callback(null);
                    });
                });
            });
        });
    });
};