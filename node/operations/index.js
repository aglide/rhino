var logger = require('../logger');
var sessionManager = require('../session-manager');
var constants = require('../constants');

var operations = {
		'login' : require('./login'),
		'register' : require('./register'),
		'edituser' : require('./edituser'),
		'banuser' : require('./banuser'),
		'removeuser' : require('./removeuser'),
		'newpost' : require('./newpost'),
		'editpost' : require('./editpost'),
		'removepost' : require('./removepost'),
		'newcomment' : require('./newcomment'),
	    'editcomment' : require('./editcomment')
};

exports.execute = function(operation, req, callback) {
	var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
	var data = req.body;
	
	// Check if it is a valid operation
	var op = operations[operation];
	if (op === undefined) {
		var err = constants.getError('c106');
		err.from = ip;
		err.time = new Date();
		err.operation = operation;
		err.data = data;
		return callback(err);
	}
	
	var context = {ip: ip};
	
	// If must be loged, check session_token and retrieve user info.
	var session_token = req.session.session_token === undefined ? req.body.session_token : req.session.session_token;
	sessionManager.checkSession(session_token, ip, function(err, user) {
		if (err && err.code != 103 || err && operation !== 'login' && operation !== 'register') {
			err.from = ip;
			err.time = new Date();
			err.operation = operation;
			err.data = data;
			logger.error(err);
			return callback(err);
		}
		
		context.user = user;
		callOperation(operation, op, data, context, function(err, result) {
			return callback(err, result);
		});
	});
};

function callOperation(opName, op, data, context, callback) {
	checkMissingFields(data, op.requiredFields, function(err) {
		if (err) {
			err.from = context.ip;
			err.time = new Date();
			err.operation = opName;
			err.data = data;
			logger.error(err);
			return callback(err);
		}
		
		op.execute(data, context, function(err, result) {
			if (err) {
				err.from = context.ip;
				err.time = new Date();
				err.operation = opName;
				err.data = data;
				logger.error(err);
				return callback(err);
			}
			
			return callback(err, result);
		});
	});
}

function checkMissingFields(data, fields, callback) {
	var err;
	
	fields.every(function (field) {
		if (data[field] === undefined || data[field] === '') {
			err = constants.getError('c105');
			err.info = {};
			err.info.field = field;
			return false;
		}
		
		return true;
	});
	
	return callback(err);
}