/**
 * Login post request with following fields:
 * - user : string - It can be either the user nickname or his email.
 * - password : string - The user password.
 * 
 * Returns a session token in case the sender needs it.
 **/
var sessionManager = require('../session-manager');
var db = require('../db');
var utils = require('../utils');
var constants = require('../constants');
var rewards = require('../rewards');

exports.requiredFields = ['user', 'password'];

exports.execute = function(data, context, callback) {
	if (data.user.indexOf('@') > -1) {
		// Email
		db.findUsers({email:data.user, removed: false}, true, function(err, user) {
			if (err) {
				return callback(err);
			}
			
			checkUser(data, user, context, function(err, data) {
				return callback(err, data);
			});
		});
	} else {
		// Nickname
		db.findUsers({nickname:data.user.toLowerCase(), removed: false}, true, function(err, user) {
			if (err) {
				return callback(err);
			}
			
			checkUser(data, user, context, function(err, data) {
				return callback(err, data);
			});
		});
	}
};

function checkUser(data, user, context, callback) {
	if (user == undefined) {
		return callback(constants.getError('c109'));
	}
	
	utils.hashPass(data.password, user.salt, function(err, hashedPassword) {
		if (err) {
			return callback(err);
		}
		
		if (hashedPassword === user.password) {
			if (!user.verified) {
				return callback(constants.getError('c110'));
			}
			if (user.banned) {
				return callback(constants.getError('c114'));
			}
			
			sessionManager.insertOrUpdateSession(user, context.id, function(err, session_token) {
				if (err) return callback(err);

				if (user.last_login.getDate() != new Date().getDate()) {
					rewards.giveDailyReward(user._id);
				}
				user.last_login = new Date();
				db.updateUser(user, function(err) {
					return callback(err, {session_token:session_token, user:user});
				});
			});
		} else {
			return callback(constants.getError('c109'));
		}
	});
}