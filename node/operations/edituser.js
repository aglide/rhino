/**
 * Post request which saves modified user data.
 * In order to ban or change permissions an administrator session token is required.
 * 
 * session_token : string - Session token retrieved using /login operation.
 * alias : string - New user display name. Optional.
 * password : string - New user password. Optional.
 * private : boolean - True if the user's profile is private. False otherwise. Optional.
 * profile_img : string - Image URL. Optional.
 * profile_color : string - Hexadecimal (#XXXXXX) profile color. Optional.
 * signature : string = Signature presented below every message. Optional.
 * banned : boolean = True if the user is banned. False if not. Optional. Administrator session token is required.
 * super_admin : boolean = True if the user is a super-admin. False otherwise. Optional. Administrator session token is required.
 * admin : boolean = True if the user is admin. False otherwise. Optional. Administrator session token is required.
 * chat_admin : boolean = True if the user is a chat-admin. False otherwise. Optional. Administrator session token is required.
 * chat_moderator : boolean = True if the user is a chat-moderator. False otherwise. Optional. Administrator session token is required.
 * 
 * Returns an error if something went wrong. Nothing otherwise.
 **/

var sessionManager = require('../session-manager');
var utils = require('../utils');
var constants = require('../constants');
var db = require('../db');

exports.requiredFields = ['nickname'];

exports.execute = function(data, context, callback) {
	if (!context.user.permissions.admin && context.user.nickname != data.nickname) {
		return callback(constants.getError('c112'));
	}
	
	db.findUsers({nickname:data.nickname.toLowerCase()}, true, function(err, u) {
		if (err) {
			return callback(err);
		}
		if (u == undefined) {
			return callback(constants.getError('c999'));
		}
		
		if (data.oldpassword != undefined && data.oldpassword != '') {
			if (data.password == undefined || data.password.length < 7 || data.password.length > 40) {
				return callback(constants.getError('c113'));
			}
			utils.hashPass(data.oldpassword, u.salt, function(err, hashedPassword) {
				if (err) {
					return callback(constants.getError('c999'));
				}
				
				if (hashedPassword === u.password) {
					utils.hashPass(data.password, u.salt, function(err, newhashedPassword) {
						data.password = newhashedPassword;
						saveData(data, u, context, function(err) {
							return callback(err);
						});
					});
				} else {
					return callback(constants.getError('c109'));
				}
			});
		} else if (context.user.permissions.admin && data.password != undefined && data.password != '') {
			utils.hashPass(data.password, u.salt, function(err, newhashedPassword) {
				data.password = newhashedPassword;
				saveData(data, u, context, function(err) {
					return callback(err);
				});
			});
		} else {
			delete data['password'];
			saveData(data, u, context, function(err) {
				return callback(err);
			});
		}
	});
};

function saveData(data, old, context, callback) {
	data._id = old._id;
	if (context.user.permissions.super_admin) {
		data.permissions = {
			super_admin: data.super_admin,
			admin: data.admin,
			chat_admin: data.chat_admin,
			chat_moderator: data.chat_moderator
		};
		sessionManager.deleteUserSession(old._id);
	} else {
		delete data['permissions'];
	}
	if (old.level < 1) {
		data.profile_color = '#000000';
	}
	if (old.level < 2) {
		data.italics = false;
	}
	if (old.level < 3) {
		data.bold = false;
	}
	delete data['birth'];
	delete data['level'];
	delete data['nickname'];
	if (data.italics == undefined) {
		data.italics = false;
	}
	if (data.bold == undefined) {
		data.bold = false;
	}
	if (data.private == undefined) {
		data.private = false;
	}
	if (data.profile_img == undefined || data.profile_img.trim() == '') {
		delete data['profile_img'];
	}
	db.updateUser(data, function(err) {
		return callback(err);
	});
}