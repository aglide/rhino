var constants = require('../constants');
var db = require('../db');
var sockets = require('../sockets');
var rewards = require('../rewards');
var utils = require('../utils');

exports.requiredFields = ['post_id', 'content'];

exports.execute = function(data, context, callback) {
	data.owner_id = context.user._id;
	data.removed = false;

	var today00 = new Date();
	today00.setHours(0);
	today00.setMinutes(0);
	today00.setSeconds(0);
	db.findComments({owner_id: context.user._id, created_at:{"$gte": today00}}, false, function(err, comments) {
		if (err) return callback(err);

		var maxDailyComments = context.user.level < 1 ? 10 : context.user.level < 3 ? 20 : 999;
		if (comments.length >= maxDailyComments) {
			return callback(constants.getError('c116'));
		} else {
			db.findLastComment(data.post_id, function (err, lastComment) {
				if (err) return callback(err);

				if (lastComment != undefined && lastComment.owner_id == context.user._id) {
					return callback(constants.getError('c115'));
				}

				db.findPostById(data.post_id, function(err, post) {
					if (err) return callback(err);

					utils.parseMentions(data.content, context, data.post_id, function(err, newContent) {
						if (err) return callback(err);
						data.content = newContent;

						db.saveComment(data, function(err, comment_id) {
							if (err) return callback(err);

							post.last_edit = new Date();

							db.findVotings({post_id: post._id, active:true}, true, function(err, voting) {
								if (err) return callback(err);

								if (voting != undefined) {
									var chosens = [];
									var values = [];
									if (voting.choice_type == 'single') {
										chosens.push(data.choice);
									} else if (voting.choice_type == 'multiple') {
										var chosenKeys = arrayContainsPart(/choice_/g, Object.keys(data));
										for (var i = 0; i < chosenKeys.length; i++) {
											chosens.push(data[chosenKeys[i]]);
										}
									} else if (voting.choice_type == 'range') {
										for (var i = 0; i < voting.options.length; i++) {
											values.push({
												name: voting.options[i].name,
												value: data[voting.options[i].name]
											});
										}
									}
									db.saveVote({
										voting_id: voting._id,
										owner_id: data.owner_id,
										chosens: chosens,
										values: values
									}, function(err) {});
								}
							});
							db.updatePost(post, function(err) {
								if (err) return callback(err);

								if (post.owner_id != context.user._id) {
									var notification = {
										owner_id: post.owner_id,
										title: 'Nuevo comentario',
										content: context.user.alias + ' ha comentado en tu post.',
										link: '/post?pid=' + post._id,
										img: context.user.profile_img,
										watched: false
									};
									db.saveNotification(notification, function(err, not_id) {
										if (err) return callback(err);

										notification._id = not_id;
										sockets.sendToUser(post.owner_id,'notification', notification);

										db.updateAllVisitedPost(context.user._id, data.post_id, function(err) {
											if (err) return callback(err);
										});
									});
								}
							});

							rewards.giveCommentReward(context.user._id, data, post);
							rewards.giveReceivedCommentReward(post.owner_id, context.user, data, post);

							return callback(null);
						});
					});
				});
			});
		}
	});
};

function arrayContainsPart(part, array) {
	var result = [];
	for (var i = 0; i < array.length; i++) {
		if (array[i].match(part)) {
			result.push(array[i]);
		}
	}
	return result;
}