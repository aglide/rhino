var sessionManager = require('../session-manager');
var constants = require('../constants');
var db = require('../db');

exports.requiredFields = ['nickname', 'banned'];

exports.execute = function(data, context, callback) {
	if (!context.user.permissions.admin) {
		return callback(constants.getError('c112'));
	}
	
	db.findUsers({nickname:data.nickname}, true, function(err, u) {
		if (err) {
			return callback(err);
		}
		if (u == undefined) {
			return callback(constants.getError('c999'));
		}
		
		u.banned = data.banned;
		db.updateUser(u, function(err) {
			if (err) {
				return callback(err);
			}
			
			sessionManager.deleteUserSession(u._id);
			return callback(null);
		});
	});
};