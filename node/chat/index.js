var constants = require('../constants');
var db = require('../db');
var utils = require('../utils');
var sockets = require('../sockets');
var Room = require('./room');
var spamControl = require('./spamcontrol');

var rooms = {};

exports.init = function() {
	// Init rooms
	db.findRooms({removed: false}, false, function(err, result) {
		for (var i = 0; i < result.length; i++) {
			rooms[result[i]._id] = (Room.create(result[i]));
		}
	});
};

exports.getRooms = function(callback) {
	formatRooms(callback);
};

exports.enterInRoom = function(room_id, socket) {
	var r = rooms[room_id];
	if (socket.userData.permissions.admin || r.founder_id == socket.userData._id || (r.public && (!r.bans || r.bans.indexOf(socket.userData.nickname) < 0)) || (!r.public && r.perms && r.perms.indexOf(socket.userData.nickname) > -1)) {
		rooms[room_id].insertSocket(socket);
		exports.getRooms(function(err, list) {
			sockets.sendToAll('updateRooms', list);
		});
	} else {
		db.findUserById(r.founder_id, function(err, founder) {
			if (!err && founder != undefined) {
				socket.emit('noPermission', {founder_nickname: founder.nickname});
			}
		});
	}
	
};

exports.enterPersonalChat = function(target_id, socket) {
	db.findUserById(target_id, function(err, u) {
		if (!err && u != undefined) {
			var otherUser = {
					_id : u._id,
					alias: u.alias,
					nickname : u.nickname,
					profile_img : u.profile_img,
					profile_color: u.profile_color,
				    level: u.level
			}
			db.findPersonalMessages({'$or':[{'$and':[{receiver_id:target_id}, {sender_id:socket.userData._id}]},{'$and':[{receiver_id:socket.userData._id},{sender_id:target_id}]}]}, function(err, history) {
				processPersonalHistory(0, history, [], socket.userData, otherUser, function(err, processedHistory) {
					var sendData = {
						target: otherUser,
						history: processedHistory
					};
					socket.emit('enterPersonalChat', sendData);
				});
			});
		}
	});
};

exports.exitRoom = function(room_id, socket_id) {
	rooms[room_id].exitSocket(socket_id);
	exports.getRooms(function(err, list) {
		sockets.sendToAll('updateRooms', list);
	});
};

exports.sendMessage = function(room_id, socket, content) {
	if (spamControl.checkSpam(content.trim(), socket.userData._id.toString())) {
		if (content.trim().indexOf('/') == 0) {
			processRoomCommand(room_id, socket, content.trim());
		} else {
			rooms[room_id].sendMessage(socket.id, content.trim());
		}
	}
};

exports.sendPersonalMessage = function(target_id, socket, content) {
	if (content.trim().indexOf('/') == 0) {
		processPersonalCommand(target_id, socket, content.trim());
	} else {
		var formatedContent = utils.parseMessage(content.trim());
		if (socket.userData.italics) {
			formatedContent = '<em>' + formatedContent + '</em>';
		}
		if (socket.userData.bold) {
			formatedContent = '<strong>' + formatedContent + '</strong>';
		}
		var message = {
			receiver_id: target_id,
			sender_id: socket.userData._id,
			content: formatedContent
		};
		if (!sockets.isInChat(target_id)) {
			var notification = {
				owner_id: target_id,
				title: 'Nuevo mensaje',
				content: socket.userData.alias + ' te ha enviado un mensaje de chat.',
				link: '/chatbox?u=' + socket.userData._id,
				img: socket.userData.profile_img,
				watched: false
			};
			db.saveNotification(notification, function (err, not_id) {
				notification._id = not_id;
				sockets.sendToUser(target_id, 'notification', notification);
			});
		}
		db.findUserById(target_id, function (err, u) {
			var otherUser = {
				_id: u._id,
				alias: u.alias,
				nickname: u.nickname,
				profile_img: u.profile_img,
				profile_color: u.profile_color,
				level: u.level
			};
			sockets.sendToUser(target_id, 'personalMessage', {
				sender: socket.userData,
				receiver: otherUser,
				msg: {content: message.content, dateTime: utils.formatDate(new Date())}
			});
			sockets.sendToUser(socket.userData._id, 'personalMessage', {
				sender: socket.userData,
				receiver: otherUser,
				msg: {content: message.content, dateTime: utils.formatDate(new Date())}
			});
		});
		db.savePersonalMessage(message, function (err) {
			if (err) console.log(err);
		});
	}
};

exports.requestChatHistory = function(target_id, msgCount, socket) {
	db.findUserById(target_id, function(err, u) {
		var otherUser = {
				_id : u._id,
				alias: u.alias,
				nickname : u.nickname,
				profile_img : u.profile_img,
				profile_color: u.profile_color,
			    level: u.level
		}
		db.findPersonalMessages({'$or':[{'$and':[{receiver_id:target_id}, {sender_id:socket.userData._id}]},{'$and':[{receiver_id:socket.userData._id},{sender_id:target_id}]}]}, function(err, history) {
			processPersonalHistory(0, history, [], socket.userData, otherUser, function(err, processedHistory) {
				var sendData = {
					target_id: target_id,
					history: processedHistory
				};
				socket.emit('updateChatHistory', sendData);
			});
		}, msgCount);
	});
};

exports.requestRoomHistory = function(room_id, msgCount, socket) {
	rooms[room_id].requestHistory(msgCount, socket);
};

exports.createRoom = function(room, socket) {
	var maxRooms = socket.userData.level < 2 ? 0 : socket.userData.level < 3 ? 1 : socket.userData.level < 5 ? 2 : 3;
	var createdRooms = 0;
	var keys = Object.keys(rooms);
	for(var i = 0; i < keys.length; i++) {
		var r = rooms[keys[i]];
		if (r.founder_id.toString() == socket.userData._id.toString()) {
			createdRooms++;
		}
	}
	if (socket.userData.permissions.admin || createdRooms < maxRooms) {
		var newRoom = {
			name: utils.escapeHtml(room.room_name),
			founder_id: socket.userData._id,
			removed: false,
			perms: [],
			bans: [],
			public: !(room.room_private)
		};
		db.saveRoom(newRoom, function(err, newID) {
			newRoom._id = newID;
			rooms[newID] = (Room.create(newRoom));
			exports.getRooms(function(err, list) {
				sockets.sendToAll('updateRooms', list);
			});
		});
	}
};

exports.editRoom = function(room, socket) {
	if (socket.userData.permissions.admin || rooms[room._id].founder_id == socket.userData._id) {
		rooms[room._id].updateRoom(room);
		exports.getRooms(function(err, list) {
			sockets.sendToAll('updateRooms', list);
		});
	}
};

exports.deleteRoom = function(room_id, socket) {
	if (socket.userData.permissions.admin || socket.userData._id == rooms[room_id].founder_id) {
		db.deleteRoom({_id: rooms[room_id]._id}, function(err) {
			if (!err) {
				delete rooms[room_id];
				exports.getRooms(function(err, list) {
					sockets.sendToAll('updateRooms', list);
				});
			}
		});
	}
};

exports.roomTyping = function(room_id, socket) {
	rooms[room_id].sendToAllInRoom('roomTyping', {room_id: room_id, user: socket.userData});
};

exports.chatTyping = function(target_id, socket) {
	sockets.sendToUser(target_id, 'chatTyping', {user: socket.userData});
};

function formatRooms(callback) {
	var finalRoomList = [];
	var keys = Object.keys(rooms);
	for(var i = 0; i < keys.length; i++) {
		var r = rooms[keys[i]];
		finalRoomList.push({
			_id: r._id,
			name: r.name,
			founder_id: r.founder_id,
			public: r.public,
			bans: r.bans,
			perms: r.perms,
			uCount: r.getUsersIn().length
		});
	}
	return callback(null, finalRoomList);
}

function processPersonalHistory(index, history, processedHistory, user1, user2, callback) {
	if (history == undefined || index == history.length) {
		return callback(null, processedHistory);
	}
	
	var h = history[index];
	var sender = user1._id == h.sender_id ? user1 : user2;
	var receiver = user1._id == h.receiver_id ? user1 : user2;
	processedHistory.push({msg:{content:h.content, dateTime: utils.formatDate(h.created_at)}, sender: sender, receiver: receiver});
	processPersonalHistory(index+1, history, processedHistory, user1, user2, function(err, newList) {
		return callback(err, newList);
	});
}

function processRoomCommand(room_id, socket, content) {
	var parts = content.match(/\S+/g);
	var command = parts[0];
	var rest = parts.join(" ").substring(command.length);
	if (command == '/me') {
		rooms[room_id].sendUserSaying(socket.id, rest);
	} else if (command == '/worse') {
		rooms[room_id].sendWorse(socket.id);
	/*} else if (command == '/quote') {
		rooms[room_id].sendQuote(socket.id);*/
	} else {
		socket.emit('badRoomCommand', {room_id: room_id});
	}
}

function processPersonalCommand(target_id, socket, content) {
	var parts = content.match(/\S+/g);
	var command = parts[0];
	var rest = parts.join(" ").substring(command.length);
	if (command == '/me') {
		db.findUserById(target_id, function (err, u) {
			var otherUser = {
				_id: u._id,
				alias: u.alias,
				nickname: u.nickname,
				profile_img: u.profile_img,
				profile_color: u.profile_color,
				level: u.level
			};
			var processedContent = socket.userData.alias + utils.escapeHtml(rest);
			sockets.sendToUser(target_id, 'cmdUserSaying', {
				sender: socket.userData,
				target: otherUser,
				msg: {
					content: processedContent,
					dateTime: utils.formatDate(new Date())
				}
			});
			sockets.sendToUser(socket.userData._id, 'cmdUserSaying', {
				sender: socket.userData,
				target: otherUser,
				msg: {
					content: processedContent,
					dateTime: utils.formatDate(new Date())
				}
			});
			var message = {
				receiver_id: target_id,
				sender_id: socket.userData._id,
				content: processedContent
			};
			db.savePersonalMessage(message, function (err) {
				if (err) console.log(err);
			});
		});
	} else if (command == '/worse') {
		db.findUserById(target_id, function (err, u) {
			var otherUser = {
				_id: u._id,
				alias: u.alias,
				nickname: u.nickname,
				profile_img: u.profile_img,
				profile_color: u.profile_color,
				level: u.level
			};
			sockets.sendToUser(target_id, 'cmdWorse', {
				sender: socket.userData,
				target: otherUser
			});
			sockets.sendToUser(socket.userData._id, 'cmdWorse', {
				sender: socket.userData,
				target: otherUser
			});
		});
	} else if (command == '/quote') {
		db.findPersonalMessages({'$or':[{'$and':[{receiver_id:target_id}, {sender_id:socket.userData._id}]},{'$and':[{receiver_id:socket.userData._id},{sender_id:target_id}]}]}, function(err, messages) {
			if (messages.length > 0) {
				var index = Math.floor(Math.random() * messages.length);
				db.findUserById(target_id, function (err, u) {
					var otherUser = {
						_id: u._id,
						alias: u.alias,
						nickname: u.nickname,
						profile_img: u.profile_img,
						profile_color: u.profile_color,
						level: u.level
					};
					sockets.sendToUser(target_id, 'cmdUserSaying', {
						sender: socket.userData,
						target: otherUser,
						msg: {
							content: socket.userData.alias + ' recuerda: <strong>"' + messages[index].content + '"</strong> - ' + (target_id == messages[index].sender_id ? otherUser.alias : socket.userData.alias) + ' ' + messages[index].created_at.getFullYear(),
							dateTime: utils.formatDate(new Date())
						}
					});
					sockets.sendToUser(socket.userData._id, 'cmdUserSaying', {
						sender: socket.userData,
						target: otherUser,
						msg: {
							content: socket.userData.alias + ' recuerda: <strong>"' + messages[index].content + '"</strong> - ' + (target_id == messages[index].sender_id ? otherUser.alias : socket.userData.alias) + ' ' + messages[index].created_at.getFullYear(),
							dateTime: utils.formatDate(new Date())
						}
					});
				});
			}
		}, 0, 5000);
	} else {
		socket.emit('badCommand', {target_id: target_id});
	}
}