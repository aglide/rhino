var utils = require('../utils');
var db = require('../db');

exports.create = function(roomData) {
	var self = {};
	
	self._id = roomData._id;
	self.name = roomData.name;
	self.public = roomData.public;
	self.removed = roomData.removed;
	self.founder_id = roomData.founder_id;
	self.perms = roomData.perms;
	self.bans = roomData.bans;
	self.created_at = roomData.created_at;
	
	self.insertedSockets = {};
	
	self.insertSocket = function(socket) {
		self.insertedSockets[socket.id] = socket;
		var data = {
			_id: self._id,
			name: self.name,
			founder_id: self.founder_id,
			public: self.public,
			perms: self.perms,
			bans: self.bans,
			users: self.getUsersIn()
		};
		self.sendToAllInRoom('updateRoom', data);
		db.findUserById(self.founder_id, function(err, founder) {
			data.founder = founder;
			db.findMessages({room_id: self._id}, function(err, history) {
				processHistory(0, history, [], function(err, processedHistory) {
					data.history = processedHistory;
					socket.emit('enterRoom', data);
				});
			});
		});
	};
	
	self.exitSocket = function(socket_id) {
		delete self.insertedSockets[socket_id];
		var data = {
			_id: self._id,
			name: self.name,
			founder_id: self.founder_id,
			public: self.public,
			perms: self.perms,
			bans: self.bans,
			users: self.getUsersIn()
		};
		self.sendToAllInRoom('updateRoom', data);	
	};
	
	self.updateRoom = function(roomData) {
		self.name = roomData.name;
		self.public = roomData.public;
		self.perms = roomData.perms;
		self.bans = roomData.bans;
		db.updateRoom(roomData, function (err) {
			var data = {
				_id: self._id,
				name: self.name,
				founder_id: self.founder_id,
				public: self.public,
				perms: self.perms,
				bans: self.bans,
				users: self.getUsersIn()
			};
			self.sendToAllInRoom('updateRoom', data);
		});
	};
	
	self.getUsersIn = function() {
		var users = [];
		var keys = Object.keys(self.insertedSockets);
		for (var i = 0; i < keys.length; i++) {
			var s = self.insertedSockets[keys[i]];
			if (s.id != undefined && utils.findUserInList(s.userData._id, users) == undefined) {
				users.push(s.userData);
			}
		}
		return users;
	};
	
	self.sendToAllInRoom = function(operation, data) {
		var keys = Object.keys(self.insertedSockets);
		for (var i = 0; i < keys.length; i++) {
			var s = self.insertedSockets[keys[i]];
			if (s.id != undefined) {
				s.emit(operation, data);
			}
		}
	};
	
	self.sendMessage = function(socket_id, content) {
		if (self.insertedSockets[socket_id] != undefined) {
			var userData = self.insertedSockets[socket_id].userData;
			var formatedContent = utils.parseMessage(content.trim());
			if (userData.italics) {
				formatedContent = '<em>' + formatedContent + '</em>';
			}
			if (userData.bold) {
				formatedContent = '<strong>' + formatedContent + '</strong>';
			}
			var message = {
				room_id: self._id,
				owner_id: userData._id,
				content: formatedContent
			};
			db.saveMessage(message, function(err) {
				if (err) {
					console.log(err);
				}
			});
			self.sendToAllInRoom('message', {
				room_id: self._id,
				msg: {
					content: formatedContent,
					dateTime: utils.formatDate(new Date())
				},
				sender: userData
			});
		}
	};

	self.sendUserSaying = function(socket_id, content) {
		if (self.insertedSockets[socket_id] != undefined) {
			var userData = self.insertedSockets[socket_id].userData;
			self.sendToAllInRoom('cmdRoomUserSaying', {
				room_id: self._id,
				msg: {
					content: userData.alias + utils.escapeHtml(content),
					dateTime: utils.formatDate(new Date())
				}
			});
			var message = {
				room_id: self._id,
				owner_id: userData._id,
				content: userData.alias + utils.parseMessage(content)
			};
			db.saveMessage(message, function(err) {
				if (err) {
					console.log(err);
				}
			});
		}
	};

	self.sendWorse = function(socket_id) {
		if (self.insertedSockets[socket_id] != undefined) {
			var userData = self.insertedSockets[socket_id].userData;
			self.sendToAllInRoom('cmdRoomWorse', {
				room_id: self._id,
				sender: userData
			});
		}
	};

	self.sendQuote = function(socket_id) {
		if (self.insertedSockets[socket_id] != undefined) {
			db.findMessages({room_id: self._id}, function(err, messages) {
				if (messages.length > 0) {
					var userData = self.insertedSockets[socket_id].userData;
					var index = Math.floor(Math.random() * messages.length);
					db.findUserById(messages[index].owner_id, function (err, u) {
						var otherUser = {
							_id: u._id,
							alias: u.alias,
							nickname: u.nickname,
							profile_img: u.profile_img,
							profile_color: u.profile_color,
							level: u.level
						};
						self.sendToAllInRoom('cmdRoomUserSaying', {
							room_id: self._id,
							msg: {
								content: userData.alias + ' recuerda: <strong>"' + messages[index].content + '"</strong> - ' + otherUser.alias + ' ' + messages[index].created_at.getFullYear(),
								dateTime: utils.formatDate(new Date())
							}
						});
					});
				}
			}, 0, 5000);
		}
	};
	
	self.requestHistory = function(msgCount, socket) {
		db.findMessages({room_id: self._id}, function(err, history) {
			processHistory(0, history, [], function(err, processedHistory) {
				socket.emit('updateRoomHistory', {room_id: self._id, history: processedHistory});
			});
		}, msgCount);
	};
	
	self.socketChecker = setInterval(function(){utils.checkSockets(self.insertedSockets, self.exitSocket)}, 30 * 1000);
	
	return self;
};

function processHistory(index, history, processedHistory, callback) {
	if (history == undefined || index == history.length) {
		return callback(null, processedHistory ? processedHistory : []);
	}
	
	var h = history[index];
	var owner = {};
	db.findUserById(h.owner_id, function(err, u) {
		processedHistory.push({msg:{content:h.content, dateTime: utils.formatDate(h.created_at)}, sender: {
				_id: u._id,
				nickname: u.nickname,
				alias: u.alias,
				profile_img: u.profile_img,
				profile_color: u.profile_color,
			    level: u.level
			}, room_id: h.room_id});
		processHistory(index+1, history, processedHistory, function(err, newList) {
			return callback(err, newList);
		});
	});
}