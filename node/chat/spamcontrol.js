var sockets = require('../sockets');

var minimumDistance = 6;
var numberOfSimilarMessages = 4;
var distanceBanTime = 10000;

var speedMessages = 7;
var speedTime = 2000;
var speedBanTime = 30000;

var lastMsgs = {};
var banMsgs = {};

function addLastMsg(msg, user_id) {
    if (lastMsgs[user_id] === undefined) {
        lastMsgs[user_id] = [];
    }
    lastMsgs[user_id].push({'msg': msg, 'time': new Date().getTime()});
    var limit = speedMessages > numberOfSimilarMessages ? speedMessages : numberOfSimilarMessages;
    if (lastMsgs[user_id].length > limit) {
        lastMsgs[user_id].splice(0,1);
    }
};

exports.checkSpam = function(newMsg, user_id) {
    if (banMsgs[user_id] !== undefined) {
        showBanMsg(user_id, 'Espera unos segundos antes de volver a escribir.');
        return false;
    } else if (banDistance(newMsg, user_id)) {
        banMsgs[user_id] = 'S';
        setTimeout(function(){
            lastMsgs[user_id] = [];
            banMsgs[user_id] = undefined;
        }, distanceBanTime);
        showBanMsg(user_id, 'Has escrito demasiados mensajes iguales seguidos. Espera 10 segundos antes de voler a escribir.');
        return false;
    } else if (banSpeed(newMsg, user_id)) {
        banMsgs[user_id] = 'S';
        setTimeout(function(){
            lastMsgs[user_id] = [];
            banMsgs[user_id] = undefined;
        }, speedBanTime);
        showBanMsg(user_id, 'Has escrito muchos mensajes demasiado rápido. Relájate y espera 30 segundos antes de volver a escribir.');
        return false;
    } else {
        addLastMsg(newMsg, user_id);
        return true;
    }
};

function banDistance(newMsg, user_id) {
    if (lastMsgs[user_id] !== undefined && (newMsg.length > minimumDistance || newMsg.charAt(0) == '/') && lastMsgs[user_id].length >= numberOfSimilarMessages-1) {
        var ban = true;
        var startIndex = lastMsgs[user_id].length - (numberOfSimilarMessages-1);
        for (var i = startIndex; i < lastMsgs[user_id].length; i++) {
            var data = lastMsgs[user_id][i];
            ban = ban && (levenshteinDistance(newMsg,data.msg) < minimumDistance || newMsg.charAt(0) == '/' && data.msg == newMsg);
        }

        return ban;
    }

    return false;
}

function banSpeed(newMsg, user_id) {
    if (lastMsgs[user_id] !== undefined && lastMsgs[user_id].length >= speedMessages-1) {
        var currentTime = new Date().getTime();
        return currentTime - lastMsgs[user_id][lastMsgs[user_id].length - (speedMessages-1)].time <= speedTime;
    }

    return false;
}

function showBanMsg(user_id, msg) {
   sockets.sendToUser(user_id, 'banerror', {msg:msg})
}

function levenshteinDistance(a, b) {
    if(a.length == 0) return b.length;
    if(b.length == 0) return a.length;

    var matrix = [];

    // increment along the first column of each row
    var i;
    for(i = 0; i <= b.length; i++){
        matrix[i] = [i];
    }

    // increment each column in the first row
    var j;
    for(j = 0; j <= a.length; j++){
        matrix[0][j] = j;
    }

    // Fill in the rest of the matrix
    for(i = 1; i <= b.length; i++){
        for(j = 1; j <= a.length; j++){
            if(b.charAt(i-1) == a.charAt(j-1)){
                matrix[i][j] = matrix[i-1][j-1];
            } else {
                matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                    Math.min(matrix[i][j-1] + 1, // insertion
                        matrix[i-1][j] + 1)); // deletion
            }
        }
    }

    return matrix[b.length][a.length];
};