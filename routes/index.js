var db = require('../node/db');
var logger = require('../node/logger');
var constants = require('../node/constants');
var sessionManager = require('../node/session-manager');
var config = require('../node/config').vars();
var enotifier = require('../node/enotifier');
var utils = require('../node/utils');
var chat = require('../node/chat');
var rewards = require('../node/rewards');

var Recaptcha = require('recaptcha').Recaptcha;
var captcha_pubkey = config.pubkey;
var captcha_prikey = config.prikey;

exports.oops = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Oops';
	context.navhistory.push({
		title: 'Oops',
		link: '#'
	});
	res.render('oops', {
		context : context
	});
};

exports.index = function(req, res) {
	var context = req.animagens.context;
	res.render('index', {
		context : context
	});
};

exports.register = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Registro';
	context.navhistory.push({
		title: 'Registro',
		link: '/register'
	});
	if (context.user) {
		res.redirect('/');
	} else {	
		var recaptcha = new Recaptcha(captcha_pubkey, captcha_prikey);
		res.render('register', {
			context : context,
			recaptcha_form : recaptcha.toHTML().replace(/http/g,"https")
		});
	}
};

exports.terms = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Términos y condiciones';
	context.navhistory.push({
		title: 'Términos y condiciones',
		link: '/terms'
	});
	res.render('terms', {
		context : context
	});
};

exports.levels = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Niveles';
	context.navhistory.push({
		title: 'Niveles',
		link: '/levels'
	});
	res.render('levels', {
		context : context
	});
};

exports.shop = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Tienda';
	context.navhistory.push({
		title: 'Tienda',
		link: '/shop'
	});
	res.render('shop', {
		context : context
	});
};

exports.registerdone = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Registro completado';
	context.navhistory.push({
		title: 'Registro',
		link: '/register'
	},{
		title: 'Completado',
		link: '#'
	});
	res.render('registerdone', {
		context : context
	});
};

exports.logout = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Desconectar';
	context.navhistory.push({
		title: 'Desconectar',
		link: '#'
	});
	if (context.user) {
		sessionManager.deleteUserSession(context.user._id);
		delete context['user'];
		res.render('logout', {
			context : context
		});
	} else {
		res.redirect('/');
	}
};

exports.authToken = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Confirmación de registro';
	context.navhistory.push({
		title: 'Registro',
		link: '/register'
	},{
		title: 'Confirmación',
		link: '#'
	});
	db.findVerToken({
		verification_token : req.query.token
	}, function(err, result) {
		if (err) {
			utils.manageError(req, res, 'authToken', context.ip, err);
		} else if (result == undefined) {
			err = constants.getError('c107');
			utils.manageError(req, res, 'authToken', context.ip, err);
		} else {
			db.verifyUser({
				_id : result.user_id
			}, function(err) {
				if (err) {
					utils.manageError(req, res, 'authToken', context.ip, err);
				} else {
					db.removeVerToken({
						user_id : result.user_id
					}, function(err) {
						if (err) {
							utils.manageError(req, res, 'authToken', context.ip, err, true);
						} else {
							db.findPosts({}, false, function(err, posts) {
								if (err) {
									utils.manageError(req, res, 'authToken', context.ip, err, true);
								} else {
									saveVisitedPosts(0, posts, result.user_id, function(err) {});
								}
							});
						}
					});
					res.render('authconf', {
						context : context
					});
					rewards.giveRegisteredReward(result.user_id);
				}
			});
		}
	});
};

function saveVisitedPosts(index, posts, user_id, callback) {
	if (index >= posts.length) {
		return callback(null);
	}
	
	var p = posts[index];
	db.saveVisitedPost({
		post_id: p._id,
		user_id: user_id,
		visited: false
	}, function(err) {
		if (err) return callback(err);
		return saveVisitedPosts(index+1, posts, user_id, function(err) {
			return callback(err);
		});
	});
}

exports.requestresend = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Reenvio de confirmación';
	context.navhistory.push({
		title: 'Registro',
		link: '/register'
	},{
		title: 'Reenvio',
		link: '#'
	});
	var inputUser = req.query.user;
	db.findUserByEmailOrNick(inputUser, function(err, user) {
		if (err) {
			utils.manageError(req, res, 'requestresend', context.ip, err);
		} else if (user == undefined) {
			res.render('result', {
				context : context,
				fail: true,
				message : 'El usuario introducido no existe.'
			});
		} else if (user.verified) {
			res.render('result',{
				context : context,
				fail: true,
				message : 'El usuario introducido no está pendiente de verificación.'
			});
		} else {
			db.findVerToken({user_id : user._id},function(err, result) {
				if (err) {
					utils.manageError(req, res, 'requestresend', context.ip, err);
				} else if (result == undefined) {
					res.render('result',{
						context : context,
						fail: true,
						message : 'El registro de verificación se ha perdido. Contacte con la administración en soporte@animagens.es'
					});
				} else {
					if (result.created_at != undefined && new Date().getTime() - result.created_at.getTime() < 1000 * 60 * 60 * 24) {
						res.render('result',{
							context : context,
							fail: true,
							message : 'Ya se ha solicitado un reenvio hoy. Esperese 24 horas para intentarlo de nuevo o contacte con la administración en soporte@animagens.es'
						});
					} else {
						enotifier.sendMail({
							mails : [ user.email ],
							subject : 'Animagens - Reenvio de verificación',
							content : '<p>Se ha solicitado un reenvio del correo de verificación de tu cuenta.</p>'
									+ '<p>Con tal de confirmar tu registro de forma definitiva, debes acceder al siguiente enlace:</p>'
									+ '<p><a href="http://www.animagens.es/authToken?token='
									+ result.verification_token
									+ '">Confirmar registro</a></p>'
									+ '<p>¡Esperamos verte pronto!</p>'
									},
							function(err) {
								if (err) {
									utils.manageError(req, res, 'requestresend', context.ip, err);
								} else {
									res.render('result',{
										context : context,
										fail: false,
										message : 'Se ha reenviado el correo de confirmación al usuario introducido.'
									});
								}
						});
						result.created_at = new Date();
						db.updateVerificationToken(result, function(err) {
							if (err) {
								utils.manageError(req, res, 'requestresend', context.ip, err, true);
							}
						});
					}
				}
			});
		}
	});
};

exports.requestpass = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Recuperación de contraseña';
	context.navhistory.push({
		title: 'Registro',
		link: '/register'
	},{
		title: 'Recuperación de contraseña',
		link: '#'
	});
	var inputUser = req.query.user;
	db.findUserByEmailOrNick(inputUser, function(err, user) {
		if (err) {
			utils.manageError(req, res, 'requestpass', context.ip, err);
		} else if (user == undefined) {
			res.render('result', {
				context : context,
				fail: true,
				message : 'El usuario introducido no existe.'
			});
		} else if (!user.verified) {
			res.render('result',{
				context : context,
				fail: true,
				message : 'El usuario introducido está pendiente de verificación.'
			});
		} else {
			db.findVerToken({user_id : user._id},function(err, result) {
				if (err) {
					utils.manageError(req, res, 'requestpass', context.ip, err, true);
					res.render('result',{
						context : context,
						fail: true,
						message : 'No se ha podido enviar el email. Contacte con la administración.'
					});
				} else if (result == undefined) {
					var token = utils.genToken();
					var newVerToken = {
						verification_token : token,
						user_id : user._id,
						created_at: new Date()
					};
					db.saveVerificationToken(newVerToken,function(err) {
						if (err) {
							utils.manageError(req, res, 'requestpass', context.ip, err);
						} else {
							sendRequestEmail(req, res, user.email, token, context);
						}
					});
				} else {
					if (result.created_at != undefined && new Date().getTime() - result.created_at.getTime() < 1000 * 60 * 60 * 24) {
						res.render('result',{
							context : context,
							fail: true,
							message : 'Ya se ha solicitado un cambio de contraseña hoy. Esperese 24 horas para intentarlo de nuevo o contacte con la administración en soporte.animagens@gmail.com'
						});
					} else {
						sendRequestEmail(req, res, user.email, result.verification_token, context);
						result.created_at = new Date();
						db.updateVerificationToken(result, function(err) {
							if (err) {
								utils.manageError(req, res, 'requestpass', context.ip, err, true);
							}
						});
					}
				}
			});
		}
	});
};

function sendRequestEmail(req, res, email, token, context) {
	enotifier.sendMail({
		mails : [ email ],
		subject : 'Animagens - Cambio de contraseña',
		content : '<p>Se ha solicitado un cambio de contraseña debido a una posible pérdida de la contraseña anterior.</p>'
				+ '<p>Si no ha sido usted el que ha solicitado la recuperación de contraseña, por favor, ignore este email y contacte con la administración.</p>'
				+ '<p>En caso contrario, acceda al siguiente enlace para llevar a cabo el proceso de recuperación de la cuenta: <a href="https://www.animagens.es/getpass?token='
				+ token
				+ '">Recuperar contraseña</a></p>'
				+ '<p>¡Esperamos verte pronto!</p>'
		},
		function(err) {
			if (err) {
				utils.manageError(req, res, 'requestpass', context.ip, err, true);
				res.render('result',{
					context : context,
					fail: true,
					message : 'No se ha podido enviar el email. Contacte con la administración en soporte.animagens@gmail.com'
				});
			} else {
				res.render('result',{
					context : context,
					fail: false,
					message : 'Se ha enviado un correo al usuario indicado con la información sobre la recuperación de la cuenta.'
				});
			}
	});
}

exports.getpass = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Recuperación de contraseña';
	context.navhistory.push({
		title: 'Registro',
		link: '/register'
	},{
		title: 'Recuperación de contraseña',
		link: '#'
	});
	db.findVerToken({
		verification_token : req.query.token
	}, function(err, result) {
		if (err) {
			utils.manageError(req, res, 'getpass', context.ip, err);
		} else if (result == undefined) {
			err = constants.getError('c107');
			utils.manageError(req, res, 'getpass', context.ip, err);
		} else {
			db.findUsers({
				_id : result.user_id
			},true,function(err, user) {
				var newPassword = utils.genToken().substring(0, 12);
				var salt = user.salt;
				utils.hashPass(newPassword,salt,function(err,hashedPassword) {
					user.password = hashedPassword;
					db.updateUser(user,function(err) {
						if (err) {
							utils.manageError(req, res, 'getpass', context.ip, err);
						} else {
							db.removeVerToken({
								verification_token : req.query.token
							},function(err) {
								if (err) {
									utils.manageError(req, res, 'getpass', context.ip, err, true);
								}
							});
							enotifier.sendMail({
								mails : [ user.email ],
								subject : 'Animagens - Nueva contraseña',
								content : '<p>Se ha completado el proceso de recuperación de la cuenta.</p>'
										+ '<p>Se ha generado una nueva contraseña para que pueda acceder a la cuenta. Recuerde que puede cambiarla en cualquier momento en el paner de configuración de la cuenta<p>'
										+ '<h3>Nueva contraseña: '
										+ newPassword
										+ '</h3>'
										+ '<p>¡Esperamos verte pronto!</p>'
							},function(err) {
								if (err) {
									utils.manageError(req, res, 'getpass', context.ip, err, true);
									res.render('result',{
										context : context,
										fail: true,
										message : 'No se ha podido enviar el email. Contacte con la administración.'
									});
								} else {
									res.render('result',{
										context : context,
										fail: false,
										message : 'Se ha enviado un correo con la nueva contraseña.'
									});
								}
							});
						}
					});
				});
			});
		}
	});
};

exports.forum = function(req, res) {
	var context = req.animagens.context;
	var category = req.query.c;
	var from = req.query.from ? req.query.from : 0;
	var to = req.query.to ? req.query.to : parseInt(from) + 9;
	db.findResumedPosts(category, from, to, context.user, function(err, posts) {
		if (err) {
			utils.manageError(req, res, 'forum', context.ip, err);
		} else {
			context.title = 'Animagens';
			context.navhistory.push({
				title: 'Foro',
				link: '/forum'
			});
			if (posts.category != undefined) {
				context.navhistory.push({
					title: posts.category.title,
					link: '/forum?c=' + category
				});
			}
			res.render('forum', {
				context : context,
				posts: posts.list,
				userList: posts.userList,
				category: posts.category,
				pages: {
					current: from / 9,
					count: posts.totalCount / 9
				}
			});
		}
	});
};

exports.post = function(req, res) {
	var context = req.animagens.context;
	db.findPostById(req.query.pid, function(err, result) {
		if (err) {
			utils.manageError(req, res, 'getpost', context.ip, err);
		} else {
			result.content = utils.parseSpoilers(result.content);
			db.findCategoryById(result.category_id, function(err, category) {
				if (err) {
					utils.manageError(req, res, 'getpost', context.ip, err);
				} else {
					context.title = result.title;
					context.navhistory.push({
						title: 'Forum',
						link: '/forum?c=' + result.category_id
					}, {
						title: 'Post',
						link: '/post?pid=' + result._id
					});
					db.findUserById(result.owner_id, function(err, owner) {
						if (err) {
							utils.manageError(req, res, 'getpost', context.ip, err);
						} else {
							var d = new Date(result.created_at);
							var dateTime = utils.formatDate(d);
							db.findPagComments({post_id: result._id}, 0, 5, function(err, commData) {
								if (err) {
									utils.manageError(req, res, 'getpost', context.ip, err);
								} else {
									db.findAllValorations(function(err, valorations) {
										if (err) {
											utils.manageError(req, res, 'getpost', context.ip, err);
										} else {
											var processedValorations = porcessValorations(valorations);
											var processedLikes = processLikes(result.likes, valorations);
											var commProcessedLikes = commProcessLikes(commData.comments, valorations);
											if (context.user) {
												db.updateVisitedPost({post_id: result._id, user_id: context.user._id}, {visited: true}, function(err) {
													if (err) {
														utils.manageError(req, res, 'getpost', context.ip, err);
													}
												});
											}
											db.findResumedPosts(null, 0, 5, context.user, function (err, posts) {
												if (err) {
													utils.manageError(req, res, 'getpost', context.ip, err);
												} else {
													db.findVotings({post_id: result._id,active: true}, true, function (err, voting) {
														if (err) {
															utils.manageError(req, res, 'getpost', context.ip, err);
														} else if (voting != undefined) {
															db.findVotes({voting_id: voting._id}, function (err, votes) {
																if (err) {
																	utils.manageError(req, res, 'getpost', context.ip, err);
																} else {
																	var preparedVotes = {};
																	var canVote = true;
																	var votingPercentage = [];
																	for (var i = 0; i < voting.options.length; i++) {
																		votingPercentage.push(0);
																	}
																	if (voting.choice_type == 'range') {
																		var rangeSum = 0;
																		for (var l = 0; l < votes.length; l++) {
																			if (context.user != undefined && votes[l].owner_id == context.user._id) canVote = false;
																			preparedVotes[votes[l].owner_id] = votes[l];
																			for (var k = 0; k < votes[l].values.length; k++) {
																				rangeSum += votes[l].values[k].value;
																			}
																		}
																		for (var j = 0; j < votes.length; j++) {
																			var vote = votes[j];
																			for (var k = 0; k < vote.values.length; k++) {
																				var index = findIndexOfOption(vote.values[k].name,voting.options);
																				votingPercentage[index] = votingPercentage[index] + (vote.values[k].value * 100 / rangeSum);
																			}
																		}
																	} else {
																		var choseSum = 0;
																		for (var l = 0; l < votes.length; l++) {
																			if (context.user != undefined && votes[l].owner_id == context.user._id) canVote = false;
																			preparedVotes[votes[l].owner_id] = votes[l];
																			choseSum += votes[l].chosens.length;
																		}
																		for (var j = 0; j < votes.length; j++) {
																			var vote = votes[j];
																			for (var l = 0; l < vote.chosens.length; l++) {
																				var index = findIndexOfOption(vote.chosens[l],voting.options);
																				votingPercentage[index] = votingPercentage[index] + (100.0 / choseSum);
																			}
																		}
																	}
																	res.render('post', {
																		context: context,
																		post: result,
																		owner: owner,
																		dateTime: dateTime,
																		comments: commData.comments,
																		commUserData: commData.userData,
																		valorations: processedValorations,
																		likes: processedLikes,
																		commLikes: commProcessedLikes,
																		category: category,
																		lastPosts: posts.list,
																		userList: posts.userList,
																		voting: voting,
																		votes: votes,
																		votingPercentage: votingPercentage,
																		canVote: canVote,
																		preparedVotes: preparedVotes
																	});
																}
															});
														} else {
															res.render('post', {
																context: context,
																post: result,
																owner: owner,
																dateTime: dateTime,
																comments: commData.comments,
																commUserData: commData.userData,
																valorations: processedValorations,
																likes: processedLikes,
																commLikes: commProcessedLikes,
																category: category,
																lastPosts: posts.list,
																userList: posts.userList
															});
														}
													});
												}
											}, {fixed:false});
										}
									});
								}
							});
						}
					});
				}
			});
		}
	});
};

function porcessValorations(valorations) {
	var processedValorations = [];
	for (var i = 0; i < valorations.length; i++) {
		var v = valorations[i];
		processedValorations.push({
			description: v.description,
			value: v._id,
			selected: false,
			imageSrc: v.img
		});
	}
	return processedValorations;
}

function processLikes(likes, valorations) {
	var vals = {};
	for (var i = 0; i < likes.length; i++) {
		var l = likes[i];
		
		if (vals[l.valoration_id] === undefined) {
			var tmpVal = findVal(valorations, l.valoration_id);
			if (tmpVal != null) {
				vals[l.valoration_id] = {
						_ids: [],
						user_ids: [],
						img: tmpVal.img,
						description: tmpVal.description,
						amount: 0
				};
			}
		}
		vals[l.valoration_id].amount++;
		vals[l.valoration_id]._ids.push(l._id);
		vals[l.valoration_id].user_ids.push(l.user_id);
	}
	
	var result = [];
	var keys = Object.keys(vals);
	for (var j = 0; j < keys.length; j++) {
		var k = keys[j];
		var data = vals[k];
		data._id = k;
		result.push(data);
	}
	return result;
}

function commProcessLikes(comments, valorations) {
	var result = [];
	for (var i = 0; i < comments.length; i++) {
		var c = comments[i];
		var data = processLikes(c.likes, valorations);
		result.push(data);
	}
	return result;
}

function findVal(list, id) {
	for (var i = 0; i < list.length; i++) {
		var val = list[i];
		if (val._id == id) {
			return val;
		}
	}
	return null;
}

function findIndexOfOption(option, list) {
	for (var i = 0; i < list.length; i++) {
		var o = list[i];
		if (o.name == option) {
			return i;
		}
	}
	return -1;
}

exports.editpost = function(req, res) {
	var context = req.animagens.context;
	db.findPostById(req.query.pid, function(err, result) {
		if (err) {
			utils.manageError(req, res, 'editpost', context.ip, err);
		} else if (result == null) {
			utils.manageError(req, res, 'editpost', context.ip, {});
		} else {
			context.title = 'Editar Post';
			context.navhistory.push({
				title: 'Forum',
				link: '/forum?c=' + result.category_id
			}, {
				title: 'Post',
				link: '/post?pid=' + result._id
			}, {
				title: 'Editar',
				link: '#'
			});
			if (context.user && (context.user.permissions.admin || context.user._id == result.owner_id)) {
				db.findVotings({post_id: result._id, active: true}, true, function (err, voting) {
					if (err) {
						utils.manageError(req, res, 'getpost', context.ip, err);
					} else {
						var recaptcha = new Recaptcha(captcha_pubkey, captcha_prikey);
						res.render('editpost',{
							context : context,
							post : result,
							categories: context.categories,
							recaptcha_form : recaptcha.toHTML().replace(/http/g,"https"),
							voting: voting
						});
					}
				});
			} else {
				res.redirect('/');
			}
		}
	});
};

exports.newpost = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Nuevo post';
	context.navhistory.push({
		title: 'Forum',
		link: '/forum'
	}, {
		title: 'Nuevo Post',
		link: '#'
	});
	var category_id = req.query.c;
	if (context.user == undefined) {
		res.redirect('/');
	} else {
		var recaptcha = new Recaptcha(captcha_pubkey, captcha_prikey);
		res.render('newpost',{
			context : context,
			categories: context.categories,
			category_id: category_id,
			recaptcha_form : recaptcha.toHTML().replace(/http/g,"https")
		});
	}
};

exports.viewuser = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Perfil';
	var userName = req.query.user;
	db.findUsers({
		nickname : userName.toLowerCase(),
		removed : false,
		verified : true
	}, true, function(err, u) {
		if (err) {
			utils.manageError(req, res, 'editpost', context.ip, err);
		} else if (u == undefined) {
			res.render('result',{
				context : context,
				fail: true,
				message : 'El usuario introducido es incorrecto.'
			});
		} else {
			context.navhistory.push({
				title: u.alias,
				link: '#'
			});
			u.password = undefined;
			u.salt = undefined;
			u.age = utils.calculateAge(u.birth);
			u.formattedBirth = utils.formatDate(u.birth, true);
			db.findUbox({owner_id: u._id}, function(err, ubox) {
				if (err) {
					utils.manageError(req, res, 'editpost', context.ip, err);
				} else {
					buildBiosAndAchievements(u._id, ubox, function(err, achievements, bios) {
						if (err) {
							utils.manageError(req, res, 'editpost', context.ip, err);
						} else {
							db.countAchievements({}, function(err, count) {
								if (err) {
									utils.manageError(req, res, 'editpost', context.ip, err);
								} else {
									res.render('viewuser', {
										context: context,
										data: u,
										ubox: ubox,
										bios: bios,
										achievements: achievements,
										totalAchCount: count
									});
								}
							});
						}
					});
				}
			});
		}
	});
};

function buildBiosAndAchievements(user_id, ubox, callback) {
	var finalBios = [];
	var finalAchievements = [];
	db.findBios({owner_id: user_id}, function (err, bios) {
		if (err) {
			return callback(err);
		} else {
			db.findAchievements({}, function (err, achs) {
				if (err) {
					return callback(err);
				} else {
					for (var i = 0; i < achs.length; i++) {
						var a = achs[i];
						a.description = undefined;
						if (ubox.achievements.indexOf(a._id) > -1) {
							finalAchievements.push(a);
						}
					}
					for (var i = bios.length-1; i >= 0; i--) {
						var b = bios[i];
						var rewards = [];
						for (var j = 0; j < achs.length; j++) {
							var a = achs[j];
							if (b.rewards.indexOf(a._id) > -1) {
								rewards.push(a);
							}
						}
						finalBios.push({
							owner_id: b.owner_id,
							msg: b.msg,
							rewards: rewards,
							animas: b.animas,
							experience: b.experience,
							created_at: b.created_at
						});
					}

					return callback(null, finalAchievements, finalBios);
				}
			});
		}
	});
}

exports.search = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Buscar';
	context.navhistory.push({
		title: 'Buscar',
		link: '#'
	});
	var query = new RegExp(req.query.q, "i");
	var users = [];
	var posts = [];
	
	db.findUsers({nickname: {"$regex": query}}, false, function(err, us) {
		if (err) {
			utils.manageError(req, res, 'search', context.ip, err);
		} else {
			users.push.apply(users, us);
			db.findUsers({alias: {"$regex": query}}, false, function(err, aus) {
				if (err) {
					utils.manageError(req, res, 'search', context.ip, err);
				} else {
					users.push.apply(users, aus);
					db.findPosts({title: {"$regex": query}}, false, function(err, tposts) {
						if (err) {
							utils.manageError(req, res, 'search', context.ip, err);
						} else {
							posts.push.apply(posts, tposts);
							db.findPosts({content: {"$regex": query}}, false, function(err, cposts) {
								if (err) {
									utils.manageError(req, res, 'search', context.ip, err);
								} else {
									posts.push.apply(posts, cposts);
									res.render('search', {
										context: context,
										query: req.query.q,
										users: users,
										posts: posts
									});
								}
							});
						}
					});
				}
			});
		}
	})
};

exports.chat = function(req, res) {
	var context = req.animagens.context;
	if (!context.user) {
		res.redirect('/');
	} else {
		context.title = 'Chat';
		context.navhistory.push({
			title: 'Chat',
			link: '#'
		});
		chat.getRooms(function(err, rooms) {
			res.render('chat', {
				context : context,
				rooms: rooms,
				uConv: req.query.u != undefined ? req.query.u : 'none'
			});
		});
	}
};

exports.emojisguide = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Emojis';
	context.navhistory.push({
		title: 'Guía de emojis',
		link: '#'
	});
	res.render('emojisguide', {
		context : context
	});
};

exports.notifications = function(req, res) {
	var context = req.animagens.context;
	if (!context.user) {
		res.redirect('/');
	} else {
		context.title = 'Notificaciones';
		context.navhistory.push({
			title: 'Notificaciones',
			link: '#'
		});
		db.findNotifications(0,1000,{owner_id: context.user._id}, function(err, notifications) {
			res.render('notifications', {
				context : context,
				notifications : notifications
			});
		});		
	}
};

exports.achievements = function(req, res) {
	var context = req.animagens.context;
	context.title = 'Logros';
	context.navhistory.push({
		title: 'Logros',
		link: '#'
	});
	var showSecrets = context.user && context.user.permissions.admin ? true : false;
	db.findAchievements(showSecrets ? {} : {secret: false}, function(err, achs) {
		if (err) {
			utils.manageError(req, res, 'achievements', context.ip, err);
		} else {
			db.countAchievements({secret:true}, function(err, count) {
				if (err) {
					utils.manageError(req, res, 'achievements', context.ip, err);
				} else {
					res.render('achievements', {
						context: context,
						achievements: achs,
						hiddenCount: count
					});
				}
			});
		}
	});
};
