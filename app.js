var express = require('express')
  , ipfilter = require('express-ipfilter')
  , bodyParser = require('body-parser')
  , path = require('path')
  , Recaptcha = require('recaptcha').Recaptcha
  , logger = require('./node/logger')
  , config = require('./node/config').vars()
  , constants = require('./node/constants');

// Config from params
var lastValue;
process.argv.forEach(function (val, index, array) {
	  if (index > 1 && lastValue === '--config-file') {
		  config.load(val);
	  }
	  lastValue = val;
});

var ips = config.ips;
var operations = require('./node/operations');
var db = require('./node/db');
var app = express();
var routes = require('./routes');
var middleware = require('./node/middleware');
var sessionManager = require('./node/session-manager');
var captcha_pubkey = config.pubkey;
var captcha_prikey = config.prikey;
var rewards = require('./node/rewards');

// all environments
var cookieParser = require('cookie-parser')();
var session = require('cookie-session')({ secret: config.cookie_secret });
app.set('port', config.server_port);
app.set('ip_addr', config.server_ip);
app.set('views', __dirname + '/views');
app.locals.basedir = __dirname + '/public';
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});
app.use(cookieParser);
app.use(session);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(app.router);
app.use(ipfilter(ips, {mode: 'allow'}));

// development only
if (config.test) {
  app.use(express.errorHandler());
}

// gets
app.get('/', middleware.work, routes.index);
app.get('/register', middleware.work, routes.register);
app.get('/registerdone', middleware.work, routes.registerdone);
app.get('/logout', middleware.work, routes.logout);
app.get('/viewuser', middleware.work, routes.viewuser);
app.get('/post', middleware.work, routes.post);
app.get('/editpost', middleware.work, routes.editpost);
app.get('/newpost', middleware.work, routes.newpost);
app.get('/authToken', middleware.work, routes.authToken);
app.get('/requestResend', middleware.work, routes.requestresend);
app.get('/requestPass', middleware.work, routes.requestpass);
app.get('/getpass', middleware.work, routes.getpass);
app.get('/terms', middleware.work, routes.terms);
app.get('/oops', middleware.work, routes.oops);
app.get('/forum', middleware.work, routes.forum);
app.get('/search', middleware.work, routes.search);
app.get('/chatbox', middleware.work, routes.chat);
app.get('/emojisguide', middleware.work, routes.emojisguide);
app.get('/notifications', middleware.work, routes.notifications);
app.get('/achievements', middleware.work, routes.achievements);
app.get('/levels', middleware.work, routes.levels);
app.get('/shop', middleware.work, routes.shop);
app.get('/ABA965FB22AAF9709304258F2C2369D0.txt', function(req, res) {
	res.writeHead(404, {'Content-Type': 'text/plain'});
    res.write('8A622A4A111C76335D64C926CA26EE2BE743DCD1\n');
    res.write('comodoca.com');
    res.end();
});


// posts
app.post('/login', function(req,res) {
	try {
		operations.execute('login', req, function(err, result) {
			if (err) {
				res.json({err: {code: err.code, info: err.info}});
			} else {
				req.session.session_token = result.session_token;
				res.json({session_token: result.session_token, user:result.user});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'login';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/register', function(req,res) {
	try {
		if (config.test) {
			operations.execute('register', req, function(err, result) {
	    		if (err) {
	    			res.json({err: {code: err.code, info: err.info}});
	    		} else {
	    			res.json({});
	    		}
	    	});
		} else {
			var data = {
			    remoteip:  req.connection.remoteAddress,
			    challenge: req.body.recaptcha_challenge_field,
			    response:  req.body.recaptcha_response_field
			};
			var recaptcha = new Recaptcha(captcha_pubkey, captcha_prikey, data);
			recaptcha.verify(function(success, error_code) {
			    if (success) {
			    	operations.execute('register', req, function(err, result) {
			    		if (err) {
			    			res.json({err: {code: err.code, info: err.info}});
			    		} else {
			    			res.json({});
			    		}
			    	});
			    } else {
			    	res.json({err: {code: 104, info: {captcha: recaptcha.toHTML()}}});
			    }
			});
		}
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'register';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/viewuser', function(req,res) {
	try {
		operations.execute('edituser', req, function(err, result) {
			if (err) {
				res.json({err: {code: err.code, info: err.info}});
			} else {
				res.json({});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'viewuser';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/contact', function(req,res) {
	try {
		operations.execute('contact', req, function(err, result) {
			if (err) {
				res.json({err: {code: err.code, info: err.info}});
			} else {
				res.json({});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'contact';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/banUser', function(req,res) {
	try {
		operations.execute('banuser', req, function(err, result) {
			if (err) {
				res.json({err: {code: err.code, info: err.info}});
			} else {
				res.json({});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'banUser';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/removeUser', function(req,res) {
	try {
		operations.execute('removeuser', req, function(err, result) {
			if (err) {
				res.json({err: {code: err.code, info: err.info}});
			} else {
				res.json({});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'removeUser';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/newpost', function(req,res) {
	try {
		if (config.test) {
			operations.execute('newpost', req, function(err, result) {
	    		if (err) {
	    			res.json({err: {code: err.code, info: err.info}});
	    		} else {
	    			res.json({post_id: result.post_id});
	    		}
	    	});
		} else {
			var data = {
			    remoteip:  req.connection.remoteAddress,
			    challenge: req.body.recaptcha_challenge_field,
			    response:  req.body.recaptcha_response_field
			};
			var recaptcha = new Recaptcha(captcha_pubkey, captcha_prikey, data);
			recaptcha.verify(function(success, error_code) {
			    if (success) {
			    	operations.execute('newpost', req, function(err, result) {
			    		if (err) {
			    			res.json({err: {code: err.code, info: err.info}});
			    		} else {
			    			res.json({post_id: result.post_id});
			    		}
			    	});
			    } else {
			    	res.json({err: {code: 104, info: {captcha: recaptcha.toHTML()}}});
			    }
			});
		}
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'newpost';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/editpost', function(req,res) {
	try {
		if (config.test) {
			operations.execute('editpost', req, function(err, result) {
	    		if (err) {
	    			res.json({err: {code: err.code, info: err.info}});
	    		} else {
	    			res.json({});
	    		}
	    	});
		} else {
			var data = {
			    remoteip:  req.connection.remoteAddress,
			    challenge: req.body.recaptcha_challenge_field,
			    response:  req.body.recaptcha_response_field
			};
			var recaptcha = new Recaptcha(captcha_pubkey, captcha_prikey, data);
			recaptcha.verify(function(success, error_code) {
			    if (success) {
			    	operations.execute('editpost', req, function(err, result) {
			    		if (err) {
			    			res.json({err: {code: err.code, info: err.info}});
			    		} else {
			    			res.json({});
			    		}
			    	});
			    } else {
			    	res.json({err: {code: 104, info: {captcha: recaptcha.toHTML()}}});
			    }
			});
		}
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'editpost';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/removePost', function(req,res) {
	try {
		operations.execute('removepost', req, function(err, result) {
			if (err) {
				res.json({err: {code: err.code, info: err.info}});
			} else {
				res.json({});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'removePost';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/indexPag', function(req,res) {
	try {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var session_token = req.session.session_token === undefined ? req.body.session_token : req.session.session_token;
		sessionManager.checkSession(session_token, ip, function(err, user) {
			if (err && err.code != 103) {
				var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
				err.from = ip;
				err.time = new Date();
				err.operation = 'indexPag';
				err.data = req.body;
				logger.error(err);
				res.json({err: {code: err.code, info: err.info}});
			} else {
				var category = req.body.category == 'all' ? undefined : req.body.category;
				var from = req.body.from;
				var to = req.body.to;
				db.findResumedPosts(category, from, to, user, function(err, result) {
					if (err) {
						res.json({err: {code: err.code, info: err.info}});
					} else {
						res.json({
							posts : result.list,
							users : result.userList,
							hasLeft : result.hasLeft,
							hasRight : result.hasRight
						});
					}
				});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'indexPag';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/newcomment', function(req,res) {
	try {
		operations.execute('newcomment', req, function(err, result) {
			if (err) {
				res.json({err: {code: err.code, info: err.info}});
			} else {
				res.json({});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'newcomment';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/editcomment', function(req,res) {
	try {
		operations.execute('editcomment', req, function(err, result) {
			if (err) {
				res.json({err: {code: err.code, info: err.info}});
			} else {
				res.json({});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'editcomment';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/likePost', function(req,res) {
	try {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var session_token = req.session.session_token === undefined ? req.body.session_token : req.session.session_token;
		sessionManager.checkSession(session_token, ip, function(err, user) {
			if (err) {
				var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
				err.from = ip;
				err.time = new Date();
				err.operation = 'likePost';
				err.data = req.body;
				logger.error(err);
				res.json({err: {code: err.code, info: err.info}});
			} else {
				db.findPostById(req.body.post_id, function(err, post) {
					if (err) {
						res.json({err: {code: err.code, info: err.info}});
					} else {
						var isLikedAlready = false;
						for (var i = 0; i < post.likes.length && !isLikedAlready; i++) {
							var l = post.likes[i];
							if (l.user_id == user._id) {
								isLikedAlready = true;
							}
						}
						if (!isLikedAlready) {
							var valoration = {
								user_id: user._id,
								valoration_id: req.body.valoration_id
							};
							post.likes.push(valoration);
							db.updatePost(post, function(err) {
								if (err) {
									res.json({err: {code: err.code, info: err.info}});
								} else {
									res.json({});
									if (user._id != post.owner_id) {
										db.saveNotification({
											owner_id: post.owner_id,
											title: 'Nueva Valoración',
											content: 'Has recibido una nueva valoración en tu post.',
											link: '/post?pid=' + post._id,
											watched: false
										}, function(err) {
											if (err) {
												logger.error(err);
											}
										});
									}
									rewards.giveValorateReward(user._id, valoration, post);
									rewards.giveReceivedValorationReward(post.owner_id, user, valoration, post);
								}
							});
						}
					}
				});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'newcomment';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/likeComm', function(req,res) {
	try {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var session_token = req.session.session_token === undefined ? req.body.session_token : req.session.session_token;
		sessionManager.checkSession(session_token, ip, function(err, user) {
			if (err) {
				var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
				err.from = ip;
				err.time = new Date();
				err.operation = 'likeComm';
				err.data = req.body;
				logger.error(err);
				res.json({err: {code: err.code, info: err.info}});
			} else {
				db.findCommentById(req.body.comm_id, function(err, comm) {
					if (err) {
						logger.error(err);
						res.json({err: {code: err.code, info: err.info}});
					} else {
						var valoration = {
							user_id: user._id,
							valoration_id: req.body.valoration_id
						};
						comm.likes.push(valoration);
						db.updateComment(comm, function(err) {
							if (err) {
								logger.error(err);
								res.json({err: {code: err.code, info: err.info}});
							} else {
								res.json({});
								if (user._id != comm.owner_id) {
									var notification = {
										owner_id: comm.owner_id,
										title: 'Nueva Valoración',
										content: 'Has recibido una nueva valoración en tu comentario.',
										link: '/post?pid=' + comm.post_id + "#" + req.body.comm_id + '-show',
										watched: false
									};
									db.saveNotification(notification, function(err, not_id) {
										if (err) {
											logger.error(err);
										}
										notification._id = not_id;
										sockets.sendToUser(comm.owner_id,'notification', notification);
									});
								}
								rewards.giveValorateReward(user._id, valoration, undefined, comm);
								rewards.giveReceivedValorationReward(comm.owner_id, user, valoration, undefined, comm);
							}
						});
					}
				});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'newcomment';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/readall', function(req,res) {
	try {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var session_token = req.session.session_token === undefined ? req.body.session_token : req.session.session_token;
		sessionManager.checkSession(session_token, ip, function(err, user) {
			if (err) {
				var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
				err.from = ip;
				err.time = new Date();
				err.operation = 'readall';
				err.data = req.body;
				logger.error(err);
				res.json({err: {code: err.code, info: err.info}});
			} else {
				db.updateVisitedPosts({user_id: user._id}, {visited: true}, function(err) {
					if (err) {
						var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
						err.from = ip;
						err.time = new Date();
						err.operation = 'readall';
						err.data = req.body;
						logger.error(err);
						res.json({err: {code: err.code, info: err.info}});
					} else {
						res.json({});
					}
				});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'readall';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/watchNotification', function(req,res) {
	try {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var session_token = req.session.session_token === undefined ? req.body.session_token : req.session.session_token;
		sessionManager.checkSession(session_token, ip, function(err, user) {
			if (err) {
				var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
				err.from = ip;
				err.time = new Date();
				err.operation = 'watchNotification';
				err.data = req.body;
				logger.error(err);
				res.json({err: {code: err.code, info: err.info}});
			} else {
				db.watchNotification(req.body.not_id, function(err) {
					if (err) {
						var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
						err.from = ip;
						err.time = new Date();
						err.operation = 'watchNotification';
						err.data = req.body;
						logger.error(err);
						res.json({err: {code: err.code, info: err.info}});
					} else {
						res.json({});
					}
				});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'watchNotification';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

app.post('/requestComments', function(req,res) {
	try {
		db.findPagComments({post_id: req.body.post_id}, parseInt(req.body.from), parseInt(req.body.from) + 5, function (err, data) {
			if (err) {
				var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
				err.from = ip;
				err.time = new Date();
				err.operation = 'requestComments';
				err.data = req.body;
				logger.error(err);
				res.json({err: {code: err.code, info: err.info}});
			} else {
				db.findAllValorations(function(err, valorations) {
					if (err) {
						var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
						err.from = ip;
						err.time = new Date();
						err.operation = 'requestComments';
						err.data = req.body;
						logger.error(err);
						res.json({err: {code: err.code, info: err.info}});
					} else {
						db.findVotings({post_id: req.body.post_id,active: true}, true, function (err, voting) {
							if (err) {
								var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
								err.from = ip;
								err.time = new Date();
								err.operation = 'requestComments';
								err.data = req.body;
								logger.error(err);
								res.json({err: {code: err.code, info: err.info}});
							} else if (voting != undefined) {
								db.findVotes({voting_id: voting._id}, function (err, votes) {
									if (err) {
										var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
										err.from = ip;
										err.time = new Date();
										err.operation = 'requestComments';
										err.data = req.body;
										logger.error(err);
										res.json({err: {code: err.code, info: err.info}});
									} else {
										var commProcessedLikes = commProcessLikes(data.comments, valorations);
										var preparedVotes = {};
										for (var l = 0; l < votes.length; l++) {
											preparedVotes[votes[l].owner_id] = votes[l];
										}
										res.json({comments: data.comments,
											commUserData: data.userData,
											commLikes: commProcessedLikes,
											voting: voting,
											preparedVotes: preparedVotes
										});
									}
								});
							} else {
								var commProcessedLikes = commProcessLikes(data.comments, valorations);
								res.json({comments: data.comments, commUserData: data.userData, commLikes: commProcessedLikes});
							}
						});
					}
				});
			}
		});
	} catch (ex) {
		var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
		var err = constants.getError('c108');
		err.ip = ip;
		err.time = new Date();
		err.operation = 'requestComments';
		err.data = req.body;
		err.info = ex.stack;
		logger.error(err);
	}
});

function commProcessLikes(comments, valorations) {
	var result = [];
	for (var i = 0; i < comments.length; i++) {
		var c = comments[i];
		var data = processLikes(c.likes, valorations);
		result.push(data);
	}
	return result;
}

function processLikes(likes, valorations) {
	var vals = {};
	for (var i = 0; i < likes.length; i++) {
		var l = likes[i];

		if (vals[l.valoration_id] === undefined) {
			var tmpVal = findVal(valorations, l.valoration_id);
			if (tmpVal != null) {
				vals[l.valoration_id] = {
					_ids: [],
					user_ids: [],
					img: tmpVal.img,
					description: tmpVal.description,
					amount: 0
				};
			}
		}
		vals[l.valoration_id].amount++;
		vals[l.valoration_id]._ids.push(l._id);
		vals[l.valoration_id].user_ids.push(l.user_id);
	}

	var result = [];
	var keys = Object.keys(vals);
	for (var j = 0; j < keys.length; j++) {
		var k = keys[j];
		var data = vals[k];
		data._id = k;
		result.push(data);
	}
	return result;
}

function findVal(list, id) {
	for (var i = 0; i < list.length; i++) {
		var val = list[i];
		if (val._id == id) {
			return val;
		}
	}
	return null;
}

function findIndexOfOption(option, list) {
	for (var i = 0; i < list.length; i++) {
		var o = list[i];
		if (o.name == option) {
			return i;
		}
	}
	return -1;
}

// Server initialization
var server = undefined;
if (config.ssl) {
	var fs = require('fs');
	var options = {
		key  : fs.readFileSync('/root/server/ssl/server.key'),
	    cert : fs.readFileSync('/root/server/ssl/server.crt'),
	    ca : fs.readFileSync('/root/server/ssl/ca.crt')
	};
	server = require('https').createServer(options, app);
	// set up plain http server
	var http = express.createServer();
	// set up a route to redirect http to https
	http.get('*',function(req,res){  
	    res.redirect('https://animagens.es'+req.url);
	});
	// have it listen on 3000
	http.listen(3000);
} else {
	server = require('http').createServer(app);
}

var io = require('socket.io')(server);
io.use(function(socket, next) {
    var req = socket.handshake;
    var res = {};
    cookieParser(req, res, function(err) {
        if (err) {
        	return next(err);
        }
        session(req, res, next);
    });
});
server.listen(app.get('port'), app.get('ip_addr'), function(){
	logger.info({msg:'Express server listening on port ' + app.get('port')});
});

var sockets = require('./node/sockets');
var chat = require('./node/chat');
chat.init();

// Socket connection (chat)
io.sockets.on('connection', function(socket) {
	var session_token = socket.handshake.session.session_token;
	sessionManager.checkSession(session_token, '', function(err, user) {
		if (!err) {
			sockets.connectSocket(socket, user._id, function(err, s){
				if (err) {
					logger.error(err);
				}
			});
		}
	}, true);
	
	socket.on('enterChat', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				socket.inChat = true;
				socket.emit('updateUsers', {users: sockets.getConnectedUsers()});
			}
		}, true);
	});
	
	socket.on('enterPersonalChat', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				chat.enterPersonalChat(data.target_id, socket);
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('enterRoom', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				chat.enterInRoom(data.room_id, socket);
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('exitRoom', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				chat.exitRoom(data.room_id, socket.id);
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('message', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				sessionManager.insertOrUpdateSession(socket.userData, socket.handshake.address, function(err) {
					chat.sendMessage(data.room_id, socket, data.content);
				});
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('personalMessage', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				sessionManager.insertOrUpdateSession(socket.userData, socket.handshake.address, function(err) {
					chat.sendPersonalMessage(data.target_id, socket, data.content);
				});
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('requestChatHistory', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				chat.requestChatHistory(data.target_id, data.msgCount, socket);
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('requestRoomHistory', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				chat.requestRoomHistory(data.room_id, data.msgCount, socket);
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('createRoom', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				chat.createRoom(data, socket);
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('editRoom', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				chat.editRoom(data, socket);
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('deleteRoom', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				chat.deleteRoom(data.room_id, socket);
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('publishNotification', function(notification) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				db.saveNotification(notification, function(err, not_id) {
					notification._id = not_id;
					socket.emit('notification', notification);
				});
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('setSound', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				socket.userData.sound_enabled = data.enabled;
				db.updateUser(socket.userData, function(err) {if (err) logger.error(err)});
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('roomTyping', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				chat.roomTyping(data.room_id, socket);
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('chatTyping', function(data) {
		var session_token = socket.handshake.session.session_token;
		sessionManager.checkSession(session_token, '', function(err, user) {
			if (!err) {
				chat.chatTyping(data.target_id, socket);
			} else {
				socket.emit('force-disconnect');
			}
		}, true);
	});
	
	socket.on('disconnect', function(data) {
		sockets.exitSocket(socket.id);
		socket.id = undefined;
	});
});