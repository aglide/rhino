//Notifications
var previousTitle = document.title;
var windowHasFocus = true;
var messagesWithoutReading = 0;
function clearNotifications() {
	windowHasFocus = true;
	if (messagesWithoutReading > 0) {
		document.title = previousTitle;
		messagesWithoutReading = 0;
	}
}
function windowGone() {
	windowHasFocus = false;
}

var audios = {};
function initAudios() {
	audios.joined = new Audio('/sounds/joined.mp3');
	audios.notification = new Audio('/sounds/notification.mp3');
	audios.personal_msg = new Audio('/sounds/personal_msg.mp3');
	audios.room_msg = new Audio('/sounds/room_msg.mp3');
}

var socket;

$(document).ready(function() {
	
	initAudios();
	
	if (isTest) {
		socket = io.connect('http://localhost:3000', {'forceNew': true});
	} else {
		socket = io.connect('https://animagens.es');
	}
	
	socket.on('updateUsers', function(data) {
		updateUsers(data.users);
	});
	
	socket.on('notification', function(notification) {
		$('#notCount').css('display', 'inline');
		var currentCount = $('#notCount').html();
		$('#notCount').html(parseInt(currentCount)+1);
		var html = '<tr><td id="' + notification._id + '" class="product" onmouseover="watchNot(\'' + notification._id + '\')">';
		if (notification.img != undefined) {
			html += '<i class="pull-right"><img style="width:25px; height: 25px;" src="' + notification.img + '"/></i>';
		}
		html += '<span><a href="' + notification.link + '">' + notification.title + '</a></span>';
		html += '<span class="small">' + notification.content + '</span>';
		$('#notList').prepend(html);
		$('#notBell').addClass('not-visited');
		if (userData.sound_enabled) {
			audios.notification.play();
		}
	});

	socket.on('banerror', function(data) {
		showError(data.msg);
	});
	
	socket.on('force-disconnect', function(data) {
		location.reload();
	});
	
});

function updateUsers(newConnectedusers) {
	$('#usersLayoutList').empty();
	for (var j = 0; j < newConnectedusers.length; j++) {
		var u = newConnectedusers[j];
		$('#usersLayoutList').prepend('<tr><td id="' + u._id + '-layoutlist" class="product connected-user" title="' + u.nickname + '"><span><a href="/viewuser?user=' + u.nickname + '">' + u.alias + '</a></span></td></tr>');
	}
	$('#userCount').html(newConnectedusers.length);
}

function publishNotification(notification) {
	socket.emit('publishNotification', notification);
}

function soundSwitch() {
	if (userData.sound_enabled) {
		userData.sound_enabled = false;
		$('#soundButton').removeClass('fa-volume-up');
		$('#soundButton').addClass('fa-volume-off');
		socket.emit('setSound', {enabled: false});
	} else {
		userData.sound_enabled = true;
		$('#soundButton').removeClass('fa-volume-off');
		$('#soundButton').addClass('fa-volume-up');
		socket.emit('setSound', {enabled: true});
	}
}