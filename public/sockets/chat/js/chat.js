// AntiObsesivePeopleVars
var openingRoom = false;
var creatingRoom = false;

var rooms = {};
var personalChats = {};
var userNicknames = [];

$(document).ready(function() {
	
	for (var i = 0; i < currentUsers.length; i++) {
		userNicknames.push(currentUsers[i].nickname);
	}
	
	// Load socket
	var id2 = setInterval(function() {
		if(socket != undefined){
			socket.on('enterRoom', function(room) {
				if (rooms[room._id] == undefined) {
					rooms[room._id] = new Room(room);
					rooms[room._id].open();
					for(var i = 0; i < room.history.length; i++) {
						var h = room.history[i];
						rooms[room._id].postMessage(h.sender, h.msg);
					}
				}
				openingRoom = false;
			});
			
			socket.on('enterPersonalChat', function(data) {
				if (rooms[data.target._id] == undefined) {
					personalChats[data.target._id] = new PersonalChat(data.target);
					personalChats[data.target._id].open();
					for(var i = 0; i < data.history.length; i++) {
						var h = data.history[i];
						personalChats[data.target._id].postMessage(h.sender, h.receiver, h.msg);
					}
				}
				openingRoom = false;
			});
			
			socket.on('message', function(data) {
				if (rooms[data.room_id] != undefined) {
					rooms[data.room_id].postMessage(data.sender, data.msg);
					if (userData.sound_enabled && !windowHasFocus) {
						audios.room_msg.play();
					}
				}
			});

			socket.on('cmdRoomUserSaying', function(data) {
				if (rooms[data.room_id] != undefined) {
					rooms[data.room_id].userSaying(data.msg);
					if (userData.sound_enabled && !windowHasFocus) {
						audios.room_msg.play();
					}
				}
			});

			socket.on('cmdRoomWorse', function(data) {
				if (rooms[data.room_id] != undefined) {
					rooms[data.room_id].worse();
					if (userData.sound_enabled && !windowHasFocus) {
						audios.room_msg.play();
					}
				}
			});

			socket.on('badRoomCommand', function(data) {
				if (rooms[data.room_id] != undefined) {
					rooms[data.room_id].badCommand();
				}
			});
			
			socket.on('personalMessage', function(data) {
				var other = data.sender._id == userData._id ? data.receiver : data.sender;
				if (personalChats[other._id] == undefined) {
					personalChats[other._id] = new PersonalChat(other);
					personalChats[other._id].open();
				}
				personalChats[other._id].postMessage(data.sender, data.receiver, data.msg);
				if (userData.sound_enabled && !windowHasFocus) {
					audios.personal_msg.play();
				}
			});

			socket.on('cmdUserSaying', function(data) {
				var other = data.sender._id == userData._id ? data.target : data.sender;
				if (personalChats[other._id] == undefined) {
					personalChats[other._id] = new PersonalChat(other);
					personalChats[other._id].open();
				}
				personalChats[other._id].userSaying(data.msg);
				if (userData.sound_enabled && !windowHasFocus) {
					audios.personal_msg.play();
				}
			});

			socket.on('cmdWorse', function(data) {
				var other = data.sender._id == userData._id ? data.target : data.sender;
				if (personalChats[other._id] == undefined) {
					personalChats[other._id] = new PersonalChat(other);
					personalChats[other._id].open();
				}
				personalChats[other._id].worse();
				if (userData.sound_enabled && !windowHasFocus) {
					audios.personal_msg.play();
				}
			});

			socket.on('badCommand', function(data) {
				if (personalChats[data.target_id] != undefined) {
					personalChats[data.target_id].badCommand();
				}
			});
			
			socket.on('updateRoom', function(room) {
				if (rooms[room._id] != undefined) {
					if (!userData.permissions.admin && room.founder_id != userData._id && (room.public && room.bans.indexOf(userData.nickname) > -1
							|| !room.public && room.perms.indexOf(userData.nickname) < 0)) {
						rooms[room._id].close();
						delete rooms[room._id];
					} else {
						rooms[room._id].update(room);
					}
				}
			});
			
			socket.on('updateRooms', function(updatedRooms) {
				creatingRoom = false;
				$('#roomSelection').empty();
				var roomTmpList = [];
				for(var i = 0; i < updatedRooms.length; i++) {
					var r = updatedRooms[i];
					if (r.founder_id == userData._id && !userData.permissions.admin) {
						$('#createRoomButton').remove();
					}
					roomTmpList.push(r._id);
					$('#roomSelection').append('<option value="' + r._id + '" class="' + ((userData.permissions.admin || r.founder_id == userData._id || (r.public && (r.bans == undefined || r.bans.indexOf(userData.nickname) < 0)) || (!r.public && r.perms != undefined && r.perms.indexOf(userData.nickname) > -1)) ? "publicRoomItem" : "privateRoomItem") + ' ">' + r.name + ' (' + r.uCount + ')');
				}
				
				var keys = Object.keys(rooms);
				for(var i = 0; i < keys.length; i++) {
					var room_id = keys[i];
					if (roomTmpList.indexOf(room_id) < 0) {
						rooms[room_id].close();
						delete rooms[room_id];
					}
				}
			});
			
			socket.on('updateRoomHistory', function(data) {
				if (rooms[data.room_id] != undefined) {
					rooms[data.room_id].loadHistory(data.history);
				}
			});
			
			socket.on('updateChatHistory', function(data) {
				if (personalChats[data.target_id] != undefined) {
					personalChats[data.target_id].loadHistory(data.history);
				}
			});
			
			socket.on('roomTyping', function(data) {
				if (userData._id.toString() != data.user._id.toString() && rooms[data.room_id] != undefined) {
					rooms[data.room_id].userTyped(data.user);
				}
			});
			
			socket.on('chatTyping', function(data) {
				if (userData._id.toString() != data.user._id.toString() && personalChats[data.user._id] != undefined) {
					personalChats[data.user._id].userTyped(data.user);
				}
			});
			
			socket.on('noPermission', function(data) {
				openingRoom = false;
				showError('No tienes permiso para entrar en esa sala. Pregunta al creador de la sala (' + data.founder_nickname + ') para pedir acceso.');
			});

			if (uConv != undefined && uConv != 'none') {
				socket.emit('enterPersonalChat', {target_id: uConv});
			}

			socket.on('updateUsers', function(data) {
				updateChatUsers(currentUsers, data.users);
			});
			
			socket.emit('enterChat');
			clearInterval(id2);
		}
	}, 1000);
	
	emojify.setConfig({
	    emojify_tag_type : 'div',           // Only run emojify.js on this element
	    only_crawl_id    : null,            // Use to restrict where emojify.js applies
	    img_dir          : 'emojis/images/basic',  // Directory for emoji images
	    ignored_tags     : {                // Ignore the following tags
	        'SCRIPT'  : 1,
	        'TEXTAREA': 1,
	        'A'       : 1,
	        'PRE'     : 1,
	        'CODE'    : 1
	    }
	});
	emojify.run();
	
});

function sendMessage(room_id) {
	var content = $('#' + room_id + '-input').val();
	if (content.trim() != '') {
		socket.emit('message', {content: content, room_id: room_id});
		$('#' + room_id + '-input').val('');
	}
}

function tryEnterRoom() {
	if(!openingRoom) {
		closeAlert();
		openingRoom = true;
		var room_id = $('#roomSelection').val();
		if (rooms[room_id] == undefined) {
			socket.emit('enterRoom', {room_id: room_id});
			$('#roomEnter').modal('hide');
		}
	}
}

function openRoom(room_id) {
	if (rooms[room_id] != undefined) {
		rooms[room_id].open();
	}
}

function minimizeRoom(room_id) {
	if (rooms[room_id] != undefined) {
		rooms[room_id].minimize();
	}
}

function closeRoom(room_id) {
	if (rooms[room_id] != undefined) {
		rooms[room_id].close();
		delete rooms[room_id];
		socket.emit('exitRoom', {room_id: room_id});
	}
}

function loadRoomHistory(room_id) {
	if (rooms[room_id] != undefined) {
		rooms[room_id].requestLoadHistory();
	}
}

function deleteRoom() {
	var room_id = $('#editRoomId').val();
	if (rooms[room_id] != undefined) {
		socket.emit('deleteRoom', {room_id: room_id});
		rooms[room_id].close();
		delete rooms[room_id];
	}
	$('#editRoom').modal('hide');
}

function sendPersonalMessage(target_id) {
	var content = $('#' + target_id + '-input').val();
	if (content.trim() != '') {
		socket.emit('personalMessage', {content: content, target_id: target_id});
		$('#' + target_id + '-input').val('');
	}
}

function tryEnterPersonalChat(data) {
	if(!openingRoom) {
		openingRoom = true;
		var target_id = $('#userSelection').val();
		if (personalChats[target_id] == undefined) {
			socket.emit('enterPersonalChat', {target_id: target_id});
			$('#userChat').modal('hide');
		}
	}
}

function openPersonalChat(target_id) {
	if (personalChats[target_id] != undefined) {
		personalChats[target_id].open();
	}
}

function minimizePersonalChat(target_id) {
	if (personalChats[target_id] != undefined) {
		personalChats[target_id].minimize();
	}
}

function closePersonalChat(target_id) {
	if (personalChats[target_id] != undefined) {
		personalChats[target_id].close();
		delete personalChats[target_id];
	}
}

function loadChatHistory(target_id) {
	if (personalChats[target_id] != undefined) {
		personalChats[target_id].requestLoadHistory();
	}
}

function updateChatUsers(currentUsers, newConnectedusers) {
	var newAll = currentUsers;
	for (var j = 0; j < newConnectedusers.length; j++) {
		var newConnected = newConnectedusers[j];

		for (var i = 0; i < newAll.length; i++) {
			var currentUser = newAll[i];
		
			
			if (newConnected._id == currentUser._id) {
				newAll[i].connected = true;
				break;
			}
		}
	}
	for (var k = 0; k < newAll.length; k++) {
		var u = newAll[k];
		
		$('#userSelection option[value="' + u._id + '"]').remove();
		if (u._id != userData._id) {
			if (u.connected) {
				$('#userSelection').prepend('<option value="' + u._id + '" class="connected-user" title="' + u.nickname + '">' + u.alias + ' (Conectado)</option>');
			} else {
				$('#userSelection').append('<option value="' + u._id + '" class="not-connected-user" title="' + u.nickname + '">' + u.alias + ' (Desconectado)</option>');
			}
		}
	}
}

function createRoom() {
	if (!creatingRoom) {
		creatingRoom = true;
		var room_name = $('#roomName').val();
		var room_private = $('#roomPrivate').is(':checked');
		socket.emit('createRoom', {room_name: room_name, room_private: room_private});
		$('#createRoom').modal('hide');
		$('#roomName').val('');
	}
}

function pressEnterCreateRoom(e) {
	if(e.keyCode == 13) {
		createRoom();
	}
}

function typeInRoom(e, room_id) {
	if(e.keyCode == 13) {
		sendMessage(room_id);
	} else {
		socket.emit('roomTyping', {room_id: room_id});
	}
}

function typeInChat(e, target_id) {
	if(e.keyCode == 13) {
		sendPersonalMessage(target_id);
	} else {
		socket.emit('chatTyping', {target_id: target_id});
	}
}

function loadEditRoom(room_id) {
	if (rooms[room_id] != undefined) {
		$('#editRoomId').val(rooms[room_id]._id);
		$('#editRoomName').val(rooms[room_id].name);
		$('#editRoomPrivate').attr('checked', !rooms[room_id].public);
		$('.twitter-typeahead').remove();
		if (rooms[room_id].public) {
			$('#editUsersContent').prepend('<input type="text" id="editInputUsers" class="form-control typeahead" placeholder="Baneos" maxlength="23" />')
			$('#infoRoomAccess').html('Baneados:<br>' + rooms[room_id].bans.join('<br>'));
		} else {
			$('#editUsersContent').prepend('<input type="text" id="editInputUsers" class="form-control typeahead" placeholder="Permisiones" maxlength="23" />')
			$('#infoRoomAccess').html('Permitidos:<br>' + rooms[room_id].perms.join('<br>'));
		}
		$('.typeahead').typeahead({hint:true,highlight:true,minLength:1,autocomplete:true},{name: 'users',source: substringMatcher(userNicknames)});
	}
}

function pressEnterEditRoom(e) {
	if(e.keyCode == 13) {
		editRoom();
	}
}

function editRoom() {
	var room_id = $('#editRoomId').val();
	if (rooms[room_id] != undefined) {
		var room_name = $('#editRoomName').val();
		var room_private = $('#editRoomPrivate').is(':checked');
		socket.emit('editRoom', {_id: room_id, name: room_name, public: !room_private, bans: rooms[room_id].bans, perms: rooms[room_id].perms});
		$('#editRoom').modal('hide');
		$('#editRoomName').val('');
	}
}

function editAddUser() {
	var room_id = $('#editRoomId').val();
	if (rooms[room_id] != undefined) {
		var userNick = $('#editInputUsers').val();
		if (userNicknames.indexOf(userNick) > -1) {
			if (rooms[room_id].public) {
				rooms[room_id].bans.push(userNick);
				$('#infoRoomAccess').html('Baneados:<br>' + rooms[room_id].bans.join('<br>'));
			} else {
				rooms[room_id].perms.push(userNick);
				$('#infoRoomAccess').html('Permitidos:<br>' + rooms[room_id].perms.join('<br>'));
			}
		}
		$('#editInputUsers').val('');
	}
}

function editRemoveUser() {
	var room_id = $('#editRoomId').val();
	if (rooms[room_id] != undefined) {
		var userNick = $('#editInputUsers').val();
		if (userNicknames.indexOf(userNick) > -1) {
			if (rooms[room_id].public) {
				var index = rooms[room_id].bans.indexOf(userNick);
				if (index !== -1) {
					rooms[room_id].bans.splice(index, 1);
					$('#infoRoomAccess').html('Baneados:<br>' + rooms[room_id].bans.join('<br>'));
				}
			} else {
				var index = rooms[room_id].perms.indexOf(userNick);
				if (index !== -1) {
					rooms[room_id].perms.splice(index, 1);
					$('#infoRoomAccess').html('Permitidos:<br>' + rooms[room_id].perms.join('<br>'));
				}
			}
		}
		$('#editInputUsers').val('');
	}
}

function changePrivateRoom() {
	var room_id = $('#editRoomId').val();
	rooms[room_id].public = !$('#editRoomPrivate').is(':checked');
	$('.twitter-typeahead').remove();
	if (rooms[room_id].public) {
		$('#editUsersContent').prepend('<input type="text" id="editInputUsers" class="form-control typeahead" placeholder="Baneos" maxlength="23" />');
		$('#infoRoomAccess').html('Baneados:<br>' + rooms[room_id].bans.join('<br>'));
	} else {
		$('#editUsersContent').prepend('<input type="text" id="editInputUsers" class="form-control typeahead" placeholder="Permisiones" maxlength="23" />');
		$('#infoRoomAccess').html('Permitidos:<br>' + rooms[room_id].perms.join('<br>'));
	}
	$('.typeahead').typeahead({hint:true,highlight:true,minLength:1,autocomplete:true},{name: 'users',source: substringMatcher(userNicknames)});
}

jQuery.fn.insertAt = function(index, element) {
	  var lastIndex = this.children().size()
	  if (index < 0) {
	    index = Math.max(0, lastIndex + 1 + index)
	  }
	  this.append(element)
	  if (index < lastIndex) {
	    this.children().eq(index).before(this.children().last())
	  }
	  return this;
};