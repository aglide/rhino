function Room(roomData) {
	var self = this;
	
	self._id = roomData._id;
	self.name = roomData.name;
	self.founder = roomData.founder;
	self.public = roomData.public;
	self.bans = roomData.bans != undefined ? roomData.bans : [];
	self.perms = roomData.perms != undefined ? roomData.perms : [];
	self.users = roomData.users;
	self.messages = roomData.history;
	self.lastMessageSender_id = undefined;
	self.lastHistorySender_id = undefined;
	self.requestingHistory = false;
	self.msgCount = 0;
	
	self.myLastTimeTyped = 0;
	self.lastTimeTyped = {};
	
	self.typeChecker = setInterval(function() {
		if (Date.now() - self.myLastTimeTyped <= 300) {
			socket.emit('typing', {room_id: self._id});
		}
		
		var typingUsers = []
		var keys = Object.keys(self.lastTimeTyped);
		for (var i = 0; i < keys.length; i++) {
			var user_id = keys[i];
			var last_time = self.lastTimeTyped[user_id].lastTime;
			if (Date.now() - last_time >= 500) {
				delete self.lastTimeTyped[user_id];
			} else {
				typingUsers.push(self.lastTimeTyped[user_id].user);
			}
		}
		if (typingUsers.length == 0) {
			$('#isTypingLabel' + self._id).html('');
		} else if (typingUsers.length == 1) {
			$('#isTypingLabel' + self._id).html(typingUsers[0].alias + ' está escribiendo...');
		} else if (typingUsers.length == 2) {
			$('#isTypingLabel' + self._id).html(typingUsers[0].alias + ' y ' + typingUsers[1].alias + ' están escribiendo...');
		} else {
			$('#isTypingLabel' + self._id).html(typingUsers.length + ' personas están escribiendo...');
		}
	}, 500);
	
	self.update = function(roomData) {
		self.name = roomData.name;
		self.users = roomData.users;
		self.public = roomData.public;
		self.bans = roomData.bans;
		self.perms = roomData.perms;
		$('#' + self._id + ' .box-title').html(self.name);
		$('#' + self._id + '-uList').html(self.users.length);
		var content = '';
		if (self.users.length > 0) {
			content = self.users[0].alias;
			for (var i = 1; i < self.users.length; i++) {
				content += ' - ' + self.users[i].alias;
			}
		}
		$('#' + self._id + '-uList').tooltip({
		    show: {
		     effect: "slideDown",
		     delay: 50
		   }
		});
		$('#' + self._id + '-uList').attr('data-original-title', content);
	};
	
	self.postMessage = function(sender, msg) {
		delete self.lastTimeTyped[sender._id];
		var html = '';
		if (self.lastMessageSender_id == sender._id) {
			$('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last .direct-chat-text').append('<br/>' + msg.content);
			$('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last .direct-chat-timestamp').html(msg.dateTime);
		} else {
			if (sender._id == userData._id) {
				html += '<div class="direct-chat-msg right">';
				html += '<div class="direct-chat-info clearfix">';
				html += '<span class="direct-chat-name pull-right" title="' + sender.nickname + '">' + sender.alias + '</span>';
				html += '<span class="direct-chat-timestamp pull-left">' + msg.dateTime + '</span>';
				html += '</div>';
			} else {
				html += '<div class="direct-chat-msg">';
				html += '<div class="direct-chat-info clearfix">';
				html += '<span class="direct-chat-name pull-left" title="' + sender.nickname + '">' + sender.alias + '</span>';
				html += '<span class="direct-chat-timestamp pull-right">' + msg.dateTime + '</span>';
				html += '</div>';
			}
			html += '<a href="/viewuser?user=' + sender.nickname + '" target="_blank" title="' + sender.nickname + '"><img src="' + sender.profile_img + '" alt="..." class="direct-chat-img"/></a>';
			html += '<div class="direct-chat-text" style="word-wrap: break-word; color:' + sender.profile_color + ';">' + msg.content + '</div></div>';
			$('#' + self._id + ' .direct-chat-messages').append(html);
		}
		$('#' + self._id + '-collapsed').addClass('not-visited');
		self.lastMessageSender_id = sender._id;

		var sTop = $('#' + self._id + ' .direct-chat-messages').scrollTop() + 10;
		$('#' + self._id + ' .direct-chat-messages').scrollTop(sTop + $('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last').height());
		
		if (!windowHasFocus) {
			messagesWithoutReading++;
			document.title = '(' + messagesWithoutReading + ') ' + previousTitle;
	    }
		
		self.msgCount++;
		
		emojify.run();
	};
	
	self.open = function() {
		if ($('#' + self._id).length == 0) {
			var html = '<div id="' + self._id + '" class="col-md-3 chatwindow">';
			html += '<div class="box box-primary direct-chat direct-chat-primary">';
			html += '<div class="box-header with-border">';
			html += '<h3 class="box-title" title="Creada por ' + self.founder.alias + '">' + self.name + '</h3>';
			html += '<div class="box-tools pull-right"><span id="' + self._id + '-uList" class="badge bg-light-blue">' + self.users.length + '</span>';
			html += '<button type="button" class="btn btn-box-tool" onclick="minimizeRoom(\'' + self._id + '\')"><i class="fa fa-minus"></i></button>';
			html += '<button type="button" class="btn btn-box-tool" onclick="closeRoom(\'' + self._id + '\')"><i class="fa fa-times"></i></button>';
			html += '</div>';
			html += '</div>';
			html += '<div class="box-body row" style="margin: 0px; padding: 0px;">';
			if (userData._id.toString() == self.founder._id.toString() || userData.permissions.admin) {
				html += '<div class="col-xs-6"><a href="#" data-toggle="modal", data-target="#editRoom" onclick="loadEditRoom(\'' + self._id + '\')"><small>Administrar Sala</small></a></div>';
			}
			html += '<div class="col-xs-6"><a onclick="loadRoomHistory(\'' + self._id + '\')"><small>Leer anteriores</small></a></div>';
			html += '<div class="col-xs-12 direct-chat-messages">';
			html += '</div>';
			html += '</div>';
			html += '<div class="box-footer">';
			html += '<div style="height: 20px;"><span id="isTypingLabel'  + self._id + '"></span></div>';
			html += '<div class="input-group">';
			html += '<input id="' + self._id + '-input" type="text" name="message" placeholder="Escribir mensaje..." class="form-control" onkeypress="typeInRoom(event, \'' + self._id + '\')", maxlength="500"/><span class="input-group-btn">';
			html += '<button onclick="sendMessage(\'' + self._id + '\')" type="button" class="btn btn-primary btn-flat">Enviar</button></span>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			$('.chatbox').append(html);
			
			$('#' + self._id).resizable({
				maxHeight: 525,
			    maxWidth: 1200,
			    minHeight: 525,
			    minWidth: 200
			});
			
			var content = '';
			if (self.users.length > 0) {
				content = self.users[0].alias;
				for (var i = 1; i < self.users.length; i++) {
					content += '\n' + self.users[i].alias;
				}
			}
		
			$('#' + self._id + '-uList').tooltip({
			    show: {
			     effect: "slideDown",
			     delay: 50
			   }
			});
			$('#' + self._id + '-uList').attr('data-original-title', content);
		} else {
			$('#' + self._id + '-collapsed').fadeOut(600, function() { $(this).remove(); });
			$('#' + self._id).fadeOut(300).fadeIn(300);
		}
	};
	
	self.isOpen = function() {
		return $('#' + self._id + '-collapsed').length == 0;
	};
	
	self.minimize = function() {
		if (self.isOpen()) {
			var html = '<a id="' + self._id + '-collapsed" onclick="openRoom(\'' + self._id + '\')" class="ml10 nav-circle-li" style="display: inline-block;"><span>' + $('#' + self._id + ' .box-title').html().substring(0,1) + '</span></a> ';
			$('#' + self._id).fadeOut(300);
			$('.conversations').append($(html).hide().fadeIn(300));
		}
	};
	
	self.close = function() {
		$('#' + self._id).remove();
		clearInterval(self.typeChecker);
	};
	
	self.requestLoadHistory = function() {
		if (!self.requestingHistory) {
			self.requestingHistory = true;
			socket.emit('requestRoomHistory', {room_id: self._id, msgCount: self.msgCount});
		}
	};
	
	self.loadHistory = function(history) {
		self.requestingHistory = false;
		self.lastHistorySender_id = undefined;
		var blockCount = 0;
		for (var i = 0; i < history.length; i++) {
			var sender = history[i].sender;
			var msg = history[i].msg;
			var html = '';
			if (self.lastHistorySender_id == sender._id) {
				$('#' + self._id + ' .direct-chat-messages .direct-chat-msg:nth-child(' + blockCount + ') .direct-chat-text').append('<br/>' + msg.content);
				$('#' + self._id + ' .direct-chat-messages .direct-chat-msg:nth-child(' + blockCount + ') .direct-chat-timestamp').html(msg.dateTime);
			} else {
				if (sender._id == userData._id) {
					html += '<div class="direct-chat-msg right">';
					html += '<div class="direct-chat-info clearfix">';
					html += '<span class="direct-chat-name pull-right">' + sender.alias + '</span>';
					html += '<span class="direct-chat-timestamp pull-left">' + msg.dateTime + '</span>';
					html += '</div>';
				} else {
					html += '<div class="direct-chat-msg">';
					html += '<div class="direct-chat-info clearfix">';
					html += '<span class="direct-chat-name pull-left">' + sender.alias + '</span>';
					html += '<span class="direct-chat-timestamp pull-right">' + msg.dateTime + '</span>';
					html += '</div>';
				}
				html += '<a href="/viewuser?user=' + sender.nickname + '"><img src="' + sender.profile_img + '" alt="..." class="direct-chat-img"/></a>';
				html += '<div class="direct-chat-text" style="word-wrap: break-word; color:' + sender.profile_color + ';">' + msg.content + '</div></div>';
				$('#' + self._id + ' .direct-chat-messages').insertAt(blockCount, html);
				blockCount++;
			}
			$('#' + self._id + '-collapsed').addClass('not-visited');
			self.lastHistorySender_id = sender._id;
	
			var sTop = $('#' + self._id + ' .direct-chat-messages').scrollTop();
			$('#' + self._id + ' .direct-chat-messages').scrollTop(sTop + $('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last').height());
			
			if (!windowHasFocus) {
				messagesWithoutReading++;
				document.title = '(' + messagesWithoutReading + ') ' + previousTitle;
		    }
			
			self.msgCount++;
		}
		$('#' + self._id + ' .direct-chat-messages').scrollTop(0);
	};
	
	self.userTyped = function(user) {
		self.lastTimeTyped[user._id] = {user: user, lastTime: Date.now()};
	};
	
	self.type = function() {
		self.myLastTimeTyped = Date.now();
	};

	self.userSaying = function(msg) {
		var html = '<div style="text-align: center;"><em>' + msg.content + '</em></div>';
		self.lastMessageSender_id = undefined;
		$('#' + self._id + ' .direct-chat-messages').append(html);
		$('#' + self._id + '-collapsed').addClass('not-visited');

		var sTop = $('#' + self._id + ' .direct-chat-messages').scrollTop() + 10;
		$('#' + self._id + ' .direct-chat-messages').scrollTop(sTop + $('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last').height());

		if (!windowHasFocus) {
			messagesWithoutReading++;
			document.title = '(' + messagesWithoutReading + ') ' + previousTitle;
		}
	};

	self.badCommand = function() {
		var html = '<div style="text-align: center;"><strong>Comando no válido</strong></div>';
		self.lastMessageSender_id = undefined;
		$('#' + self._id + ' .direct-chat-messages').append(html);
		$('#' + self._id + '-collapsed').addClass('not-visited');

		var sTop = $('#' + self._id + ' .direct-chat-messages').scrollTop() + 10;
		$('#' + self._id + ' .direct-chat-messages').scrollTop(sTop + $('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last').height());
	};

	self.worse = function() {
		var html = '<div style="text-align: center;"><strong>Podría ser peor</strong><img style="width: 24px; height: 24px;" src="img/flower.jpg" /></div>';
		self.lastMessageSender_id = undefined;
		$('#' + self._id + ' .direct-chat-messages').append(html);
		$('#' + self._id + '-collapsed').addClass('not-visited');

		var sTop = $('#' + self._id + ' .direct-chat-messages').scrollTop() + 10;
		$('#' + self._id + ' .direct-chat-messages').scrollTop(sTop + $('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last').height());

		if (!windowHasFocus) {
			messagesWithoutReading++;
			document.title = '(' + messagesWithoutReading + ') ' + previousTitle;
		}
	};
	
	return self;
}