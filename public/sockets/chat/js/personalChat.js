function PersonalChat(data) {
	var self = this;
	
	self._id = data._id;
	self.nickname = data.nickname;
	self.alias = data.alias;
	self.profile_img = data.profile_img;
	self.profile_color = data.profile_color;
	self.lastMessageSender_id = undefined;
	self.lastHistorySender_id = undefined;
	self.requestingHistory = false;
	self.msgCount = 0;
	
	self.myLastTimeTyped = 0;
	self.lastTimeTyped = {};
	
	self.typeChecker = setInterval(function() {
		if (Date.now() - self.myLastTimeTyped <= 300) {
			socket.emit('typing', {room_id: self._id});
		}
		
		var typingUsers = []
		var keys = Object.keys(self.lastTimeTyped);
		for (var i = 0; i < keys.length; i++) {
			var user_id = keys[i];
			var last_time = self.lastTimeTyped[user_id].lastTime;
			if (Date.now() - last_time >= 500) {
				delete self.lastTimeTyped[user_id];
			} else {
				typingUsers.push(self.lastTimeTyped[user_id].user);
			}
		}
		if (typingUsers.length == 0) {
			$('#isTypingLabel' + self._id).html('');
		} else {
			$('#isTypingLabel' + self._id).html(typingUsers[0].alias + ' está escribiendo...');
		}
	}, 500);
	
	self.update = function(data) {
		self.alias = data.alias;
		self.profile_img = data.profile_img;
		self.profile_color = data.profile_color;
	};
	
	self.postMessage = function(sender, receiver, msg) {
		var html = '';
		var other = sender._id == userData._id ? receiver : sender;
		delete self.lastTimeTyped[other._id];
		if (self.lastMessageSender_id == sender._id) {
			$('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last .direct-chat-text').append('<br/>' + msg.content);
			$('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last .direct-chat-timestamp').html(msg.dateTime);
		} else {
			if (sender._id == userData._id) {
				html += '<div class="direct-chat-msg right">';
				html += '<div class="direct-chat-info clearfix">';
				html += '<span class="direct-chat-name pull-right" title="' + userData.nickname + '">' + userData.alias + '</span>';
				html += '<span class="direct-chat-timestamp pull-left">' + msg.dateTime + '</span>';
				html += '</div>';
				html += '<a href="/viewuser?user=' + userData.nickname + '" title="' + userData.nickname + '"><img src="' + userData.profile_img + '" alt="..." class="direct-chat-img"/></a>';
				html += '<div class="direct-chat-text" style="word-wrap: break-word; color:' + userData.profile_color + ';">' + msg.content + '</div></div>';
			} else {
				html += '<div class="direct-chat-msg">';
				html += '<div class="direct-chat-info clearfix">';
				html += '<span class="direct-chat-name pull-left" title="' + other.nickname + '">' + other.alias + '</span>';
				html += '<span class="direct-chat-timestamp pull-right">' + msg.dateTime + '</span>';
				html += '</div>';
				html += '<a href="/viewuser?user=' + other.nickname + '" target="_blank" title="' + other.nickname + '"><img src="' + other.profile_img + '" alt="..." class="direct-chat-img"/></a>';
				html += '<div class="direct-chat-text" style="word-wrap: break-word; color:' + sender.profile_color + ';">' + msg.content + '</div></div>';
			}
			$('#' + other._id + ' .direct-chat-messages').append(html);
		}
		self.lastMessageSender_id = sender._id;
		$('#' + other.id + '-collapsed').addClass('not-visited');
		
		var sTop = $('#' + self._id + ' .direct-chat-messages').scrollTop() + 10;
		$('#' + self._id + ' .direct-chat-messages').scrollTop(sTop + $('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last').height());
		
		if (!windowHasFocus) {
			messagesWithoutReading++;
			document.title = '(' + messagesWithoutReading + ') ' + previousTitle;
	    }
		
		self.msgCount++;
		
		emojify.run();
	};
	
	self.open = function() {
		if ($('#' + self._id).length == 0) {
			var html = '<div id="' + self._id + '" class="col-md-3 chatwindow">';
			html += '<div class="box box-success direct-chat direct-chat-success">';
			html += '<div class="box-header with-border">';
			html += '<h3 class="box-title" title="' + self.nickname + '">' + self.alias + '</h3>';
			html += '<div class="box-tools pull-right">';
			html += '<button type="button" class="btn btn-box-tool" onclick="minimizePersonalChat(\'' + self._id + '\')"><i class="fa fa-minus"></i></button>';
			html += '<button type="button" class="btn btn-box-tool" onclick="closePersonalChat(\'' + self._id + '\')"><i class="fa fa-times"></i></button>';
			html += '</div>';
			html += '</div>';
			html += '<div class="box-body">';
			html += '<div style="text-align: center;"><a onclick="loadChatHistory(\'' + self._id + '\')"><small>Leer anteriores</small></a></div>';
			html += '<div class="direct-chat-messages">';
			html += '</div>';
			html += '</div>';
			html += '<div class="box-footer">';
			html += '<div style="height: 20px;"><span id="isTypingLabel'  + self._id + '"></span></div>';
			html += '<div class="input-group">';
			html += '<input id="' + self._id + '-input" type="text" name="message" placeholder="Escribir mensaje..." class="form-control" onkeypress="typeInChat(event, \'' + self._id + '\')", maxlength="500"/><span class="input-group-btn">';
			html += '<button onclick="sendPersonalMessage(\'' + self._id + '\')" type="button" class="btn btn-success btn-flat">Enviar</button></span>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			$('.chatbox').append(html);
			
			$('#' + self._id).resizable({
				maxHeight: 525,
			    maxWidth: 1200,
			    minHeight: 525,
			    minWidth: 200
			});
		} else {
			$('#' + self._id + '-collapsed').fadeOut(600, function() { $(this).remove(); });
			$('#' + self._id).fadeOut(300).fadeIn(300);
		}
	};
	
	self.isOpen = function() {
		return $('#' + self._id + '-collapsed').length == 0;
	};
	
	self.minimize = function() {
		if (self.isOpen()) {
			var html = '<a id="' + self._id + '-collapsed" onclick="openPersonalChat(\'' + self._id + '\')" class="ml10 nav-circle-li" style="display: inline-block;"><span>' + $('#' + self._id + ' .box-title').html().substring(0,1) + '</span></a> ';
			$('#' + self._id).fadeOut(300);
			$('.conversations').append($(html).hide().fadeIn(300));
		}
	};
	
	self.close = function() {
		$('#' + self._id).remove();
		clearInterval(self.typeChecker);
	};
	
	self.requestLoadHistory = function() {
		if (!self.requestingHistory) {
			self.requestingHistory = true;
			socket.emit('requestChatHistory', {target_id: self._id, msgCount: self.msgCount});
		}
	};
	
	self.loadHistory = function(history) {
		self.requestingHistory = false;
		self.lastHistorySender_id = undefined;
		var blockCount = 0;
		for (var i = 0; i < history.length; i++) {
			var sender = history[i].sender;
			var receiver = history[i].receiver;
			var msg = history[i].msg;
			var html = '';
			var other = sender._id == userData._id ? receiver : sender;
			
			if (self.lastHistorySender_id == sender._id) {
				$('#' + self._id + ' .direct-chat-messages .direct-chat-msg:nth-child(' + blockCount + ') .direct-chat-text').append('<br/>' + msg.content);
				$('#' + self._id + ' .direct-chat-messages .direct-chat-msg:nth-child(' + blockCount + ') .direct-chat-timestamp').html(msg.dateTime);
			} else {
				if (sender._id == userData._id) {
					html += '<div class="direct-chat-msg right">';
					html += '<div class="direct-chat-info clearfix">';
					html += '<span class="direct-chat-name pull-right">' + userData.alias + '</span>';
					html += '<span class="direct-chat-timestamp pull-left">' + msg.dateTime + '</span>';
					html += '</div>';
					html += '<a href="/viewuser?user=' + userData.nickname + '"><img src="' + userData.profile_img + '" alt="..." class="direct-chat-img"/></a>';
					html += '<div class="direct-chat-text" style="word-wrap: break-word; color:' + userData.profile_color + ';">' + msg.content + '</div></div>';
				} else {
					html += '<div class="direct-chat-msg">';
					html += '<div class="direct-chat-info clearfix">';
					html += '<span class="direct-chat-name pull-left">' + other.alias + '</span>';
					html += '<span class="direct-chat-timestamp pull-right">' + msg.dateTime + '</span>';
					html += '</div>';
					html += '<a href="/viewuser?user=' + other.nickname + '"><img src="' + other.profile_img + '" alt="..." class="direct-chat-img"/></a>';
					html += '<div class="direct-chat-text" style="word-wrap: break-word; color:' + sender.profile_color + ';">' + msg.content + '</div></div>';
				}
				$('#' + other._id + ' .direct-chat-messages').insertAt(blockCount, html);
				blockCount++;
			}
			self.lastHistorySender_id = sender._id;
			$('#' + other.id + '-collapsed').addClass('not-visited');
			
			var sTop = $('#' + self._id + ' .direct-chat-messages').scrollTop();
			$('#' + self._id + ' .direct-chat-messages').scrollTop(sTop + $('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last').height());
			
			if (!windowHasFocus) {
				messagesWithoutReading++;
				document.title = '(' + messagesWithoutReading + ') ' + previousTitle;
		    }
			
			self.msgCount++;
		}
		$('#' + self._id + ' .direct-chat-messages').scrollTop(0);
	};
	

	self.userTyped = function(user) {
		self.lastTimeTyped[user._id] = {user: user, lastTime: Date.now()};
	};
	
	self.type = function() {
		self.myLastTimeTyped = Date.now();
	};

	self.userSaying = function(msg) {
		var html = '<div style="text-align: center;"><em>' + msg.content + '</em></div>';
		self.lastMessageSender_id = undefined;
		$('#' + self._id + ' .direct-chat-messages').append(html);
		$('#' + self._id + '-collapsed').addClass('not-visited');

		var sTop = $('#' + self._id + ' .direct-chat-messages').scrollTop() + 10;
		$('#' + self._id + ' .direct-chat-messages').scrollTop(sTop + $('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last').height());

		if (!windowHasFocus) {
			messagesWithoutReading++;
			document.title = '(' + messagesWithoutReading + ') ' + previousTitle;
		}
	};

	self.badCommand = function() {
		var html = '<div style="text-align: center;"><strong>Comando no válido</strong></div>';
		self.lastMessageSender_id = undefined;
		$('#' + self._id + ' .direct-chat-messages').append(html);
		$('#' + self._id + '-collapsed').addClass('not-visited');

		var sTop = $('#' + self._id + ' .direct-chat-messages').scrollTop() + 10;
		$('#' + self._id + ' .direct-chat-messages').scrollTop(sTop + $('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last').height());
	};

	self.worse = function() {
		var html = '<div style="text-align: center;"><strong>Podría ser peor</strong><img style="width: 24px; height: 24px;" src="img/flower.jpg" /></div>';
		self.lastMessageSender_id = undefined;
		$('#' + self._id + ' .direct-chat-messages').append(html);
		$('#' + self._id + '-collapsed').addClass('not-visited');

		var sTop = $('#' + self._id + ' .direct-chat-messages').scrollTop() + 10;
		$('#' + self._id + ' .direct-chat-messages').scrollTop(sTop + $('#' + self._id + ' .direct-chat-messages .direct-chat-msg:last').height());

		if (!windowHasFocus) {
			messagesWithoutReading++;
			document.title = '(' + messagesWithoutReading + ') ' + previousTitle;
		}
	};
	
	return self;
}