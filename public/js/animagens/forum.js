function readAll() {
	$.ajax({
		type: "POST",
		url : 'readall',
	    dataType : 'json',
	    data: {},
	    success : function (response) {
	    	if (response.err) {
		        showError('Error desconocido');
	        } else {
	        	location.reload();
	        }
	    }
	});
}