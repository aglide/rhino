$(document).ready(function() {

	tinymce.init({
		selector: '#editSignature',
		height: 220,
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code spoiler'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image spoiler',
		content_css: [
			'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
			'//www.tinymce.com/css/codepen.min.css'
		],
		language: 'es',
		setup: function (editor) {
			editor.on('change', function () {
				editor.save();
			});
		}
	});

	$('.ach').tooltip({placement: 'bottom',trigger: 'manual'}).tooltip('show');

	$('.textColorPicker').colorpicker();
	
	$("form").submit(function( event ) {
		$('#alert').slideUp();
		// Check fields
		var aliasRe = /^[a-zñçA-Z0-9_ ]+$/;
		if(!aliasRe.test($("input[name=alias]").val())) {
        	showError("El Alias solo puede contener letras, números o espacios.");
        	event.preventDefault();
        } else if ($("input[name=password]").val() !== $("input[name=rep-password]").val()) {
			$('#loadingModal').modal('hide');
			showError('Las contraseñas no coinciden.');
			event.preventDefault();
		}
	});
	
	$('form').ajaxForm({
	    url : 'viewuser',
	    dataType : 'json',
	    success : function (response) {
	    	if (response.err) {
	    		if (response.err.code == 109) {
	    			showError('Contraseña incorrecta');
	    		} else if (response.err.code = 103){
	    			showError('Sesión expirada. <a href="/register">Identifícate</a>');
	    		} else {
	    			showError('Error desconocido');
	    		}
	        } else {
	        	showMessage('Cambios guardados satisfactoriamente.');
	        }
	    }
	});
});

function ban(nickname, newStatus) {
	$('#alert').slideUp();
	$.ajax({
		  type: "POST",
		  url: 'banUser',
		  data: {
			  nickname: nickname,
			  banned: newStatus
		  },
		  success: function(response) {
		  	if (response.err) {
	    		if (response.err.code == 112) {
	    			showError('No tienes los permisos necesarios.');
	    		} else {
	    			showError('Error desconocido');
	    		}
	        } else {
	        	if (newStatus) {
	        		$('#unban_id').show();
	        		$('#ban_id').hide();
	        		showMessage('Usuario baneado satisfactoriamente.');
	        	} else {
	        		$('#ban_id').show();
	        		$('#unban_id').hide();
	        		showMessage('Usuario perdonado satisfactoriamente.');
	        	}
	        }
		  },
		  dataType: 'json'
	});
}

function removeUser(nickname) {
	$('#alert').slideUp();
	$.ajax({
		  type: "POST",
		  url: 'removeUser',
		  data: {
			  nickname: nickname,
			  removed: true
		  },
		  success: function(response) {
			  if (response.err) {
		    		if (response.err.code == 112) {
		    			showError('No tienes los permisos necesarios.');
		    		} else {
		    			showError('Error desconocido');
		    		}
		        } else {
		        	$('#remove_id').hide();
		        	showMessage('Usuario eliminado satisfactoriamente.');
		        }
		  },
		  dataType: 'json'
	});
}

function onImageUploaded(url) {
	$('#profileAvatar').css('background-image', 'url(' + url + ')');
	$('#profileAvatarImg').attr('src', url);
	$('#profile_img').val(url);
}