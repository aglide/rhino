var uploadingImgOption;

$(document).ready(function() {

	tinymce.init({
		selector: '#newPostContent',
		height: 500,
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code spoiler'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image spoiler',
		content_css: [
			'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
			'//www.tinymce.com/css/codepen.min.css'
		],
		language: 'es',
		setup: function (editor) {
			editor.on('change', function () {
				editor.save();
			});
		}
	});

	$('#votingContent').hide();
	
	$("#newPostForm").submit(function( event ) {
		$('#alert').slideUp();
		// Check fields
    	var titleRe = /^[a-zA-Z0-9:,-_áéíóúñçÁÉÍÓÚÑÇ¿?¡! ]+$/;
    	if(!titleRe.test($("input[name=title]").val())) {
        	showError("El Titulo solo puede contener letras, números, espacios, comas y/o guiones");
        	event.preventDefault();
        } else {
        	var imgURL = $('#display_img').val();
        	if (imgURL == undefined || imgURL.trim() == '') {
        		showError("La imagen para mostrar no puede estar vacía.");
            	event.preventDefault();
        	} else if ($('#choiceList .choiceListItem').length > 0) {
				var optionNames = [];
				for (var i = 1; i <= $('#choiceList .choiceListItem').length; i++) {
					var id = 'input[name=option_' + i + ']';
					if (optionNames.indexOf($(id).val()) > -1) {
						showError("No puede haber dos opciones de votación con el mismo nombre.");
						event.preventDefault();
						break;
					} else {
						optionNames.push($(id).val());
					}
				}
			}
        }
	});
	
	$('#newPostForm').ajaxForm({
	    url : 'newpost',
	    dataType : 'json',
	    success : function (response) {
	    	if (response.err) {
		        if (response.err.code == 104) {
	        		// If captcha failed
	        		showError('El captcha introducido es incorrecto.');
		        } else if (response.err.code == 105) {
	        		// If expected field is missing
	        		showError('El campo ' + response.err.info.field + ' no puede estar vacío.');
		        } else if (response.err.code = 103) {
	    			showError('Sesión expirada. <a href="/register">Identifícate</a>');
	    		} else if (response.err.code = 116) {
					showError('Ya has alcanzado el límite de posts diarios para tu nivel.');
				} else {
		        	showError('Error desconocido');
		        }
		        
		        // reload captcha
		        $('#recaptcha_reload_btn')[0].click();
	        } else {
	        	// Send to a page explaining that a mail has been sent to the introduced e-mail
	        	window.location.href = "/post?pid=" + response.post_id;
	        }
	    }
	});
});

function votingChanged() {
	var newValue = $('#votingType').val();
	if (newValue == 'none') {
		$('#votingContent').fadeOut();
	} else {
		$('#votingContent').fadeIn();
	}
	
}

function addOption() {
	if ($('#choiceList .choiceListItem').length < 10) {
		var nextId = 'option_' + ($('#choiceList .choiceListItem').length + 1);
		var html = '<div class="row choiceListItem" id="' + nextId + '" style="margin: 5px;">';
		html += '<div class="col-md-10">';
		html += '<input name="' + nextId + '" style="width:100%; margin: 5px;" type="text" placeholder="Opción a votar" required/>';
		html += '</div>';
		html += '<div class="col-md-2">';
		html += '<input id="' + nextId + '_img' + '" name="' + nextId + '_img' + '" type="hidden" />';
		html += '<a class="info" data-toggle="modal", data-target="#uploadImage" onclick="uploadOptionImg(\'' + nextId + '\')"><img src="img/upload.png" style="width:100%;" title="Adjuntar imagen"/></a>';
		html += '</div>';
		html += '</div>';
		$('#choiceList').append(html);
	}
}

function removeLastOption() {
	$('#choiceList .choiceListItem').last().remove();
}

function uploadOptionImg(optionId) {
	uploadingImgOption = optionId;
}

function onImageUploaded(url) {
	if (uploadingImgOption == undefined) {
		$('.big-center-cropped').css('background-image', 'url(' + url + ')');
		$('.big-center-cropped img').attr('src', url);
		$('#display_img').val(url);
	} else {
		$('#' + uploadingImgOption + ' img').attr('src', url);
		$('#' + uploadingImgOption + '_img').val(url);
		uploadingImgOption = undefined;
	}
}