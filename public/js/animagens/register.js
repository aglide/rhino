function registerLogin() {
	var user = $('input[name=loginuser]').val();
	var password = $('input[name=loginpassword]').val();
	
	$.ajax({
		type: "POST",
		url : 'login',
	    dataType : 'json',
	    data: {
	    	user: user,
	    	password: password
	    },
	    success : function (response) {
	    	$('#login').modal('hide');
	    	if (response.err) {
		        if (response.err.code == 109) {
	        		// Wrong credentials
	        		showError('El nombre de usuario o la contraseña no son correctos.');
		        } else if (response.err.code == 110) {
	        		// Not verified
		        	var u = $("input[name=user]").val();
	        		showError('El correo aun no ha sido verificado. <a href="/requestResend?user=' + u + '">Solicitar reenvio</a>');
		        } else if (response.err.code == 105) {
	        		// If expected field is missing
	        		showError('El campo ' + response.err.info.field + ' no puede estar vacío.');
		        } else if (response.err.code == 114) {
	        		// User banned
	        		showError('El usuario introducido está baneado.');
		        } else {
		        	showError('Error desconocido');
		        }
	        } else {
	        	if (window.location.href.indexOf('logout') > -1) {
	        		window.location.href = '/';
	        	} else {
	        		location.reload();
	        	}
	        }
	    }
	});
}

$(document).ready(function() {
	$('#birthpicker').datetimepicker({defaultDate: "09/04/1994"});
	$('#birthpicker').data("DateTimePicker").minDate(new Date(1916,0,1));
	$('#birthpicker').data("DateTimePicker").maxDate(new Date());
	$('#birthpicker').data("DateTimePicker").format('DD/MM/YYYY');
    	
	$('#loginpassword').keypress(function(event) {
		if (event.which == 13 && $('#loginpassword').is(':focus')) {
			login();
		}
	});
    
    $('#registerform').submit(function( event ) {
    	$('#alert').slideUp();
		// Check fields
    	var nicknameRe = /^[a-zñçÑÇA-Z0-9_áéíóúÁÉÍÓÚ]+$/;
    	var aliasRe = /^[a-zñçÑÇA-Z0-9_áéíóúÁÉÍÓÚ ]+$/;
        if(!nicknameRe.test($("input[name=nickname]").val())) {
        	showError("El Nombre de usuario solo puede contener letras números y guiones bajos \"_\"");
        	event.preventDefault();
        } else if(!aliasRe.test($("input[name=alias]").val())) {
        	showError("El Alias solo puede contener letras números, guiones bajos \"_\" y espacios");
        	event.preventDefault();
        } else if ($("input[name=registerpassword]").val() !== $("input[name=reppassword]").val()) {
			showError('Las contraseñas no coinciden.');
			event.preventDefault();
		} else if ($("input[name=email]").val() !== $("input[name=repemail]").val()) {
			showError('Los emails no coinciden.');
			event.preventDefault();
		}
	});
    
    $("#registerform").ajaxForm({
		type: "POST",
		url : 'register',
	    dataType : 'json',
	    success : function (response) {
	    	if (response.err) {
		        if (response.err.code == 104) {
	        		// If captcha failed
	        		showError('El captcha introducido es incorrecto.');
		        } else if (response.err.code == 105) {
	        		// If expected field is missing
	        		showError('El campo ' + response.err.info.field + ' no puede estar vacío.');
		        } else if (response.err.code == 101){
		        	// If Nickname already exists
	        		showError('El nombre de usuario introducido ya existe. Por favor, escoge uno diferente.');
		        } else if (response.err.code == 102){
		        	// If email is already registered
	        		showError('El correo electrónico introducido ya está registrado.');
		        } else {
		        	showError('Error desconocido');
		        }
		        
		        // reload captcha
		        $('#recaptcha_reload_btn')[0].click();
	        } else {
	        	// Send to a page explaining that a mail has been sent to the introduced e-mail
	        	window.location.href = "/registerdone";
	        }
	    }
	});
});

function sendRequestPass() {
	var u = $("input[name=loginuser]").val();
	window.location.href = '/requestPass?user=' + u;
}
