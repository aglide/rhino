function isValidImageUrl(url, callback) {
    var img = new Image();
    img.onerror = function() { callback(false); }
    img.onload =  function() { callback(true); }
    img.src = url
}

function showError(msg) {
	$('#alert').attr('class', 'alert alert-danger-rw alert-danger alert-dismisable');
	$('#alertContent').html(msg);
	$('#alert').slideDown();
	$('html,body').animate({
        scrollTop: 0
    }, 800, 'swing');
} 

function showMessage(msg) {
	$('#alert').attr('class', 'alert alert-success-rw alert-success alert-dismisable alert-success');
	$('#alertContent').html(msg);
	$('#alert').slideDown();
	$('html,body').animate({
        scrollTop: 0
    }, 800, 'swing');
} 

function closeAlert() {
	$('#alert').slideUp();
}

function formatDate(d) {
	var dateTime = toTwoDigits(d.getDate()) + '/' + toTwoDigits((d.getMonth()+1)) + '/' + d.getFullYear() + ' - ' + toTwoDigits(d.getHours()) + ':' + toTwoDigits(d.getMinutes()) + ':' + toTwoDigits(d.getSeconds());
	return dateTime;
}

function toTwoDigits(num) {
	if (num < 10) {
		return '0' + num;
	}
	return num;
}

function search(query) {
	window.location.href = "/search?q=" + query;
}

function uploadImage(callback) {
	$('#alert').slideUp();
	$('#uploadImage').modal('hide');
	isValidImageUrl($('#imageupload').val(), function(valid) {
		if (valid) {
			callback($('#imageupload').val());
		} else {
			showError('La URL de la imagen no es válida.');
		}
		$('#imageupload').val('');
	});
}

function watchNot(not_id) {
	if (!$('#' + not_id).hasClass('watched')) {
		$('#' + not_id).addClass('watched');
		var current = $('#notCount').html();
		if (current == 1) {
			$('#notCount').hide();
			$('#notBell').removeClass('not-visited');
		}
		$('#notCount').html(parseInt(current) - 1);
		$.ajax({
			type: "POST",
			url : 'watchNotification',
		    dataType : 'json',
		    data: {not_id: not_id},
		    success : function (response) {}
		});
	}
}

function showSpoiler(button) {
	$(button).parent().parent().css('height','auto');
	$(button).html('Ocultar Spoiler');
	button.onclick = function() {
		hideSpoiler(button);
	};
}

function hideSpoiler(button) {
	$(button).parent().parent().css('height','28px');
	$(button).html('Mostrar Spoiler');
	button.onclick = function() {
		showSpoiler(button);
	};
}