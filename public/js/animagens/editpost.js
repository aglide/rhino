var uploadingImgOption;

$(document).ready(function() {

	tinymce.init({
		selector: '#content',
		height: 500,
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code spoiler'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image spoiler',
		content_css: [
			'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
			'//www.tinymce.com/css/codepen.min.css'
		],
		language: 'es',
		setup: function (editor) {
			editor.on('change', function () {
				editor.save();
			});
		}
	});

	$('#votingContent').hide();
});

function savePost(post_id) {
	$('#alert').slideUp();

	var post = {
		_id: post_id,
		title: $('#title').val(),
		content: $('#content').val(),
		fixed: document.getElementById("fixed") && document.getElementById("fixed").checked ? true : false,
		category_id: $('#category').val(),
		display_img: $('#display_img').val(),
		recaptcha_response_field: $('#recaptcha_response_field').val(),
		recaptcha_challenge_field: $('#recaptcha_challenge_field').val()
	};

	if ($('#votingType').length > 0) {
		post.votingName = $('#votingType').val();
		var index = 1;
		while($('input[name=option_' + index).length > 0) {
			post['option_' + index] = $('input[name=option_' + index).val();
			post['option_' + index + '_img'] = $('input[name=option_' + index + '_img').val();
			index++;
		}
	}
	
	// Check fields
	var titleRe = /^[a-zA-Z0-9:,-_áéíóúñçÁÉÍÓÚÑÇ¿?¡! ]+$/;
	if(!titleRe.test($("input[name=title]").val())) {
    	showError("El Titulo solo puede contener letras, números, espacios, comas y/o guiones");
    	return;
    } else {
    	var imgURL = $('#display_img').val();
    	if (imgURL == undefined || imgURL.trim() == '') {
    		showError("La imagen para mostrar no puede estar vacía.");
        	event.preventDefault();
    	} else if ($('#choiceList .choiceListItem').length > 0) {
			var optionNames = [];
			for (var i = 1; i <= $('#choiceList .choiceListItem').length; i++) {
				var id = 'input[name=option_' + i + ']';
				if (optionNames.indexOf($(id).val()) > -1) {
					showError("No puede haber dos opciones de votación con el mismo nombre.");
					event.preventDefault();
					break;
				} else {
					optionNames.push($(id).val());
				}
			}
		}
    }
	
	$.ajax({
		  type: "POST",
		  url: 'editpost',
		  data: post,
		  success: function(response) {
		  	if (response.err) {
		  		if (response.err.code == 104) {
	        		// If captcha failed
	        		showError('El captcha introducido es incorrecto.');
		        } else if (response.err.code == 105) {
	        		// If expected field is missing
	        		showError('El campo ' + response.err.info.field + ' no puede estar vacío.');
		        } else if (response.err.code = 103) {
	    			showError('Sesión expirada. <a href="/register">Identifícate</a>');
	    		} else if (response.err.code == 112) {
	    			showError('No tienes los permisos necesarios.');
	    		} else {
	    			showError('Error desconocido');
	    		}
	        } else {
	        	window.location.href = "/post?pid=" + post_id;
	        }
		  },
		  dataType: 'json'
	});
}

function removePost(post_id) {
	$('#alert').slideUp();
	
	$.ajax({
		  type: "POST",
		  url: 'removepost',
		  data: {
			  _id: post_id,
			  removed: true
		  },
		  success: function(response) {
		  	if (response.err) {
		  		if (response.err.code == 105) {
	        		// If expected field is missing
	        		showError('El campo ' + response.err.info.field + ' no puede estar vacío.');
		        } else if (response.err.code == 112) {
	    			showError('No tienes los permisos necesarios.');
	    		} else {
	    			showError('Error desconocido');
	    		}
	        } else {
	        	window.location.href = "/forum";
	        }
		  },
		  dataType: 'json'
	});
}

function votingChanged() {
	var newValue = $('#votingType').val();
	if (newValue == 'none') {
		$('#votingContent').fadeOut();
	} else {
		$('#votingContent').fadeIn();
	}

}

function addOption() {
	if ($('#choiceList .choiceListItem').length < 10) {
		var nextId = 'option_' + ($('#choiceList .choiceListItem').length + 1);
		var html = '<div class="row choiceListItem" id="' + nextId + '" style="margin: 5px;">';
		html += '<div class="col-md-10">';
		html += '<input name="' + nextId + '" style="width:100%; margin: 5px;" type="text" placeholder="Opción a votar" required/>';
		html += '</div>';
		html += '<div class="col-md-2">';
		html += '<input id="' + nextId + '_img' + '" name="' + nextId + '_img' + '" type="hidden" />';
		html += '<a class="info" data-toggle="modal", data-target="#uploadImage" onclick="uploadOptionImg(\'' + nextId + '\')"><img src="img/upload.png" style="width:100%;" title="Adjuntar imagen"/></a>';
		html += '</div>';
		html += '</div>';
		$('#choiceList').append(html);
	}
}

function removeLastOption() {
	$('#choiceList .choiceListItem').last().remove();
}

function uploadOptionImg(optionId) {
	uploadingImgOption = optionId;
}

function onImageUploaded(url) {
	if (uploadingImgOption == undefined) {
		$('.big-center-cropped').css('background-image', 'url(' + url + ')');
		$('.big-center-cropped img').attr('src', url);
		$('#display_img').val(url);
	} else {
		$('#' + uploadingImgOption + ' img').attr('src', url);
		$('#' + uploadingImgOption + '_img').val(url);
		uploadingImgOption = undefined;
	}
}

function deleteVoting() {
	var html = '<div class="row" style="margin-top: 10px; margin-bottom: 10px;">';
	html += '<label for="votingType">Tipo de votación</label>';
	html += '<select id="votingType" name="votingName" onchange="votingChanged()">';
	html += '<option value="none">Ninguna</option>';
	html += '<option value="single">Elección única</option>';
	html += '<option value="multiple">Elección múltiple</option>';
	html += '<option value="range">Valoración por nota</option>';
	html += '</select></div>';
	$('#votingView').before(html);
	var newHtml = '<div id="votingContent" class="row" style="margin-top: 10px; margin-bottom: 10px;">';
	newHtml += '<label for="votingType"> Opciones</label>';
	newHtml += '<div id="choiceList"></div>';
    newHtml += '<div class="form-group">';
	newHtml += '<span class="input-group-btn">';
	newHtml += '<button type="button" class="btn btn-info" onclick="addOption()" title="Añadir a la lista.">';
	newHtml += '<span class="glyphicon glyphicon-plus" /></button>';
	newHtml += '<button type="button" class="btn btn-danger" onclick="removeLastOption()" title="Eliminar de la lista.">';
	newHtml += '<span class="glyphicon glyphicon-minus"></span></button>';
	newHtml += '</div></div>';
	$('#votingView').before(newHtml);
	$('#votingView').remove();
}