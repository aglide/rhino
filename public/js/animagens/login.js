function login() {
	var user = $('#layoutloginuser').val();
	var password = $('#layoutloginpassword').val();
	
	$.ajax({
		type: "POST",
		url : 'login',
	    dataType : 'json',
	    data: {
	    	user: user,
	    	password: password
	    },
	    success : function (response) {
	    	$('#login').modal('hide');
	    	if (response.err) {
		        if (response.err.code == 109) {
	        		// Wrong credentials
	        		showError('El nombre de usuario o la contraseña no son correctos.');
		        } else if (response.err.code == 110) {
	        		// Not verified
		        	var u = $("input[name=user]").val();
	        		showError('El correo aun no ha sido verificado. <a href="/requestResend?user=' + u + '">Solicitar reenvio</a>');
		        } else if (response.err.code == 105) {
	        		// If expected field is missing
	        		showError('El campo ' + response.err.info.field + ' no puede estar vacío.');
		        } else if (response.err.code == 114) {
	        		// User banned
	        		showError('El usuario introducido está baneado.');
		        } else {
		        	showError('Error desconocido');
		        }
	        } else {
	        	if (window.location.href.indexOf('logout') > -1 || window.location.href.indexOf('authToken') > -1) {
	        		window.location.href = '/';
	        	} else {
	        		location.reload();
	        	}
	        }
	    }
	});
}

function passwordPressed(event) {
	if (event.which == 13) {
		login();
	}
}

function sendLayoutRequestPass() {
	var u = $("#layoutloginuser").val();
	window.location.href = '/requestPass?user=' + u;
}