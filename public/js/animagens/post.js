var loadingComments = false;
var currentComments = 5;

$(document).ready(function() {

	tinymce.init({
		selector: '#newComment',
		height: 500,
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code spoiler'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image spoiler',
		content_css: [
			'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
			'//www.tinymce.com/css/codepen.min.css'
		],
		language: 'es',
		setup: function (editor) {
			editor.on('change', function () {
				editor.save();
			});
		}
	});

	$('.optionSlider').slider({
		formatter: function(value) {
			return 'Current value: ' + value;
		}
	});

	$('.optionSlider').on("slide", function(slideEvt) {
		$("#" + slideEvt.target.id.replace(/ /g,'_') + '_label').text(slideEvt.value);
	});
	
	$("#commentform").submit(function( event ) {
		$('#alert').slideUp();
	});
	
	$('#commentform').ajaxForm({
	    url : 'newcomment',
	    dataType : 'json',
	    success : function (response) {
	    	if (response.err) {
	    		if (response.err.code == 115) {
	    			showError('El último comentario es tuyo. Espera a que alguien responda antes de volver a comentar.');
	    		} else if (response.err.code == 103) {
	    			showError('Sesión expirada. <a href="/register">Identifícate</a>');
	    		} else if (response.err.code == 116) {
					showError('Ya has alcanzado el límite de comentarios diarios para tu nivel.');
				} else {
	    			showError('Error desconocido');
	    		}
	        } else {
	        	window.location.href = "/post?pid=" + $('#post_id').val();
	        }
	    }
	});

	prepareComments();
});

function prepareComments() {
	$('.avatar').each(function(index) {
		var elem = $(this);
		isValidImageUrl(elem.attr('src'), function(isValid) {
			if (!isValid) {
				elem.attr('src','https://jamfnation.jamfsoftware.com/img/default-avatars/generic-user.png');
			}
		});
	});

	var ddData = [];
	var index = 0;
	var element = $('#ddData_' + index);
	while(element.length > 0) {
		ddData.push(JSON.parse(element.val()));
		index++;
		element = $('#ddData_' + index);
	}

	$('.validation').each(function(index, elem){
		$(this).ddslick({
			data: ddData,
			width:170,
			selectText: "Valorar",
			imagePosition:"right",
			onSelected: function(selectedData){
				var id = elem.id;
				$('#' + id).fadeOut();
				var url = null;
				var data = null;
				if ($('#post_id').val() == id) {
					url = 'likePost';
					data = {
						post_id: id,
						valoration_id: selectedData.selectedData.value
					}
				} else {
					url = 'likeComm';
					data = {
						comm_id: id,
						valoration_id: selectedData.selectedData.value
					}
				}
				$.ajax({
					type: "POST",
					url: url,
					data: data,
					success: function(response) {
						if (response.err) {
							if (response.err.code == 112) {
								showError('No tienes los permisos necesarios.');
							} else {
								showError('Error desconocido');
							}
						} else {
							var element = $('#' + id + '-show .' + selectedData.selectedData.value);
							if (element.length == 0) {
								$('#' + id + '-show').append('<div style="display: inline;"><img src="' + selectedData.selectedData.imageSrc + '" title="' +
								selectedData.selectedData.description + '"/><span class="' + selectedData.selectedData.value + '">' +
								1 + '</span></div>');
							} else {
								var currentValue = element.html();
								element.html(parseInt(currentValue) + 1);
							}
						}
					},
					dataType: 'json'
				});
			}
		});
	});
}

function beginEditComment(commentID, index) {
	tinymce.init({
		selector: '#message-content-' + index,
		height: 125,
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code spoiler'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image spoiler',
		content_css: [
			'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
			'//www.tinymce.com/css/codepen.min.css'
		],
		language: 'es',
		setup: function (editor) {
			editor.on('change', function () {
				editor.save();
			});
		}
	});

	$("#message-edit-save-" + index).show();
}

function saveComment(commentID, index) {
	var newContent = $('#message-content-' + index).html();

	$.ajax({
		type: "POST",
		url : 'editcomment',
		dataType : 'json',
		data: {
			_id: commentID,
			content: newContent
		},
		success : function (response) {
			if (response.err) {
				if (response.err.code == 105) {
					// If expected field is missing
					showError('El campo ' + response.err.info.field + ' no puede estar vacío.');
				} else if (response.err.code = 103) {
					showError('Sesión expirada. <a href="/register">Identifícate</a>');
				} else if (respones.err.code = c112) {
					showError('No tienes suficientes permisos.');
				} else {
					showError('Error desconocido');
				}
			} else {
				location.reload();
			}
		}
	});
}

function loadComments() {
	if (!loadingComments) {
		loadingComments = true;

		console.log($('#post_id').val());

		$.ajax({
			type: "POST",
			url : 'requestComments',
			dataType : 'json',
			data: {
				post_id: $('#post_id').val(),
				from: currentComments
			},
			success : function (response) {
				if (response.err) {
					showError('Error desconocido');
				} else {
					currentComments += response.comments.length;
					loadingComments = false;
					if (response.comments.length < 5) {
						$('#commentsLoader').fadeOut();
					}
					for (var index = 0; index < response.comments.length; index++) {
						var c = response.comments[index];
						var html = '<li class="clearfix">';
						html += '<img class="avatar" src="' + response.commUserData[index].profile_img + '"/>';
						html += '<strong style="display: block; color: gold; margin-left: 70px;">' + response.commUserData[index].level + '</strong>';
						html += '<div class="post-comments">';
						html += '<div class="row"><div id="' + c._id + '-show" class="likes col-md-9">';
						var userInLikes = typeof userData === 'undefined';
						for (var i = 0; i < response.commLikes[index].length; i++) {
							var l = response.commLikes[index][i];
							if (!userInLikes) {
								userInLikes = l.user_ids.indexOf(userData._id.toString()) > -1;
							}
							html += '<div style="display: inline;">';
							html += '<img src="' + l.img + '", title="' + l.description + '"/>';
							html += '<span class="' + l._id + '">' + l.amount + '</span>';
							html += '</div>';
						}
						html += '</div>';
						if (!userInLikes) {
							html += '<div class="col-md-3"><i class="pull-right"><div id="' + c._id + '" class="validation"/></i></div>';
						}
						html += '</div>';
						html += '<p class="meta">' + response.commUserData[index].dateTime;
						html += '<a href="/viewuser?user=' + response.commUserData[index].nickname + '"> ' + response.commUserData[index].alias + '</a>';
						html += ' dijo : ';
						if (typeof userData !== 'undefined' && (c.owner_id == userData._id || userData.permissions.admin)) {
							html += '<i class="pull-right"><a href="#' + c._id + '" onclick="beginEditComment("' + c._id + '", ' + index + ')"><small>Editar</small></a></i>';
						}
						html += '</p>';
						html += '<div id="message-content-' + index +'" class="message-content">' + c.content + '</div>';
						html += '<button class="btn btn-rw btn-info" id="message-edit-save-' + index + '" style="display: none;" type="button" onclick="saveComment("' + c._id + '", ' + index + ')">Guardar</button>';
						if (response.voting != undefined && response.preparedVotes[c.owner_id] != undefined) {
							html += '<hr/>';
							html += '<p> Votos:';
							if (response.voting.choice_type == 'range') {
								for (var i = 0; i < response.preparedVotes[c.owner_id].values.length; i++) {
									var v = response-preparedVotes[c.owner_id].values[i];
									html += '<span>' + v.name + ': ' + v.value + '</span>';
									html += '<br/>';
								}
							} else {
								for (var i = 0; i < response.preparedVotes[c.owner_id].choices.length; i++) {
									var v = response-preparedVotes[c.owner_id].choices[i];
									html += '<span>' + v + '</span>';
								}
							}
							html += '</p>';
						}
						if (response.commUserData[index].signature && response.commUserData[index].signature != '') {
							html += '<hr/>';
							html += '<div id="message-signature-' + index + '" class="message-signature" style="padding-left: 5%; padding-right: 5%;">' + response.commUserData[index].signature + '</div>';
						}

						html += '</div>';
						html += '</li>';
						$('.comments').append(html);
					}

					prepareComments();
				}
			}
		});
	}
}